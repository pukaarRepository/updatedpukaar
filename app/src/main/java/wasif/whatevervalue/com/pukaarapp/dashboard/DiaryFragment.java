package wasif.whatevervalue.com.pukaarapp.dashboard;


import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Objects;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.database.MyDatabase;
import wasif.whatevervalue.com.pukaarapp.intefaces.GetTAG;
import wasif.whatevervalue.com.pukaarapp.models.CompleteMood;
import wasif.whatevervalue.com.pukaarapp.therapistEnd.DiaryList;
import wasif.whatevervalue.com.pukaarapp.therapistEnd.NotesList;
import wasif.whatevervalue.com.pukaarapp.utils.DateConverter;
import wasif.whatevervalue.com.pukaarapp.views.FaceSelectionView;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiaryFragment extends Fragment implements GetTAG {


	public static final String TAG = "DiaryFragment";
	private static String UID = "";

	public static DiaryFragment getInstance(String UID, boolean pTherapist, String pUID)
	{
		DiaryFragment.UID = UID;
		mUID = pUID;
		mTherapist = pTherapist;
		return new DiaryFragment();
	}

	public DiaryFragment() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View lView = inflater.inflate(R.layout.fragment_diary, container, false);
		RecyclerView diary = lView.findViewById(R.id.diary);
		ImageView lButton = lView.findViewById(R.id.back);
		ImageView historyBtn = lView.findViewById(R.id.history);
		historyBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				if(getActivity() instanceof DashboardActivity)
					((DashboardActivity) Objects.requireNonNull(getActivity())).changeFragment(new EmotionHistory(), EmotionHistory.TAG);
				else if(getActivity() instanceof NotesList)
					((NotesList) Objects.requireNonNull(getActivity())).changeFragment(EmotionHistory.getInstance(false, mUID), EmotionHistory.TAG);
				else if(getActivity() instanceof DiaryList)
					((DiaryList) Objects.requireNonNull(getActivity())).changeFragment(EmotionHistory.getInstance(true, mUID), EmotionHistory.TAG);
			}
		});

		lButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				Objects.requireNonNull(getActivity()).onBackPressed();
			}
		});

		ArrayList<CompleteMood> lCompleteMoods = new ArrayList<>();
		DiaryAdapter da = new DiaryAdapter(lCompleteMoods);
//		if(!userID.equals(""))
//		{
		fetchMoodsFromFirebase(UID, da, lCompleteMoods);

//
//		}
//		else {
//			ArrayList<CompleteMood> lCompleteMoods1 = MyDatabase.getInstance(getContext()).getAllCompleteMoods();
//			lCompleteMoods.addAll(lCompleteMoods1);
//
//			if(lCompleteMoods.size() == 0)
//			{
//				fetchMoodsFromFirebase(FirebaseAuth.getInstance().getUid(), da, lCompleteMoods);
//			}
//
//		}


//		if(lCompleteMoods.size() != 0)
//		{
//			lView.findViewById(R.id.nothig_here).setVisibility(View.GONE);
//		}

		diary.setLayoutManager(new LinearLayoutManager(getContext()));

		diary.setAdapter(da);

		return lView;
	}

	@Override
	public void onDestroy() {
		mTherapist = false;
		mUID = "";
		super.onDestroy();
	}

	private static boolean mTherapist = false;
	private static String mUID = "";

	private void fetchMoodsFromFirebase(String pUID, final DiaryAdapter pDiaryAdapter, final ArrayList<CompleteMood> lCompleteMoods) {
		DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
//		final ArrayList<CompleteMood> lCompleteMoods = new ArrayList<>();
		String notesNode = "notes";
		if(mTherapist)
			notesNode = "therapistNotes";
		String uid = pUID;
		if(!mTherapist && !mUID.equals(""))
			uid = mUID;
		ref = ref.child(notesNode).child(uid);

		if(mTherapist)
			ref = ref.child(mUID);

		ref.orderByKey().limitToLast(50).addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {
				Iterable<DataSnapshot> ds =  pDataSnapshot.getChildren();
				for(DataSnapshot dss : ds)
				{
					String mood = (String) Objects.requireNonNull(dss.child("mood").getValue());
//					CompleteMood cm = dss.getValue(CompleteMood.class);
					CompleteMood cm = new CompleteMood((Long) dss.child("timestamp").getValue()
							, mood
//							, Integer.valueOf((String) Objects.requireNonNull(dss.child("Medicine Taken").getValue()))
							, ((Long)Objects.requireNonNull(dss.child("anxiety").getValue())).intValue()
							, ((Long) Objects.requireNonNull(dss.child("selfConfidence").getValue())).intValue()
							, ((Long) Objects.requireNonNull(dss.child("energyLevel").getValue())).intValue()
							, (String) dss.child("note").getValue());

					lCompleteMoods.add(cm);

				}
				pDiaryAdapter.notifyDataSetChanged();
			}

			@Override
			public void onCancelled(@NonNull DatabaseError pDatabaseError) {

			}
		});
	}

	@Override
	public String getTAG() {
		return TAG;
	}


	private class DiaryAdapter extends RecyclerView.Adapter<DiaryHolder>
	{
		ArrayList<CompleteMood> mCompleteMoods;
		DiaryAdapter(@NonNull ArrayList<CompleteMood> pCompleteMoods) {
			mCompleteMoods = pCompleteMoods;
		}

		@NonNull
		@Override
		public DiaryHolder onCreateViewHolder(@NonNull ViewGroup pViewGroup, int pI) {
			View v = LayoutInflater.from(getContext()).inflate(R.layout.diary_item_new,
					pViewGroup, false);
			return new DiaryHolder(v);
		}

		@SuppressLint("ClickableViewAccessibility")
		@Override
		public void onBindViewHolder(@NonNull final DiaryHolder pDiaryHolder, int pI) {

			CompleteMood cm = getItem(pI);
			pDiaryHolder.moodImage.setImageDrawable(getResources().
					getDrawable(FaceSelectionView
							.getfullFaceImageID(FaceSelectionView.getMoodIndex(cm.mood))));

//			pDiaryHolder.mood.setText("Mood: " + FaceSelectionView.moods[cm.mood]);
//			pDiaryHolder.anxiety.setText("Anxiety: "+ cm.anxiety);
//			pDiaryHolder.energyLevel.setText("Energy Level: "+ cm.energyLevel);
//			pDiaryHolder.selfConfidence.setText("Self Confidence: "+ cm.selfConfidence);
			pDiaryHolder.date.setText(DateConverter.convertToString(new Date(cm.timestamp)));
//			pDiaryHolder.diaryItem.setRadius(15);
//			String yes = "Yes";
//			if(cm.medicationTaken == 0)
//			{
//				yes = "NO";
//			}
//			else if(cm.medicationTaken != 1)
//				yes = "-";
//			pDiaryHolder.medication.setText("Medication: " + yes);

			pDiaryHolder.note.setText(cm.note);
			pDiaryHolder.note.setMovementMethod(new ScrollingMovementMethod());

			pDiaryHolder.note.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View view, MotionEvent event) {
					view.getParent().requestDisallowInterceptTouchEvent(true);
					switch (event.getAction()&MotionEvent.ACTION_MASK){
						case MotionEvent.ACTION_UP:
							view.getParent().requestDisallowInterceptTouchEvent(false);
							break;
					}
					return false;
				}
			});
//			pDiaryHolder.show_note.setOnClickListener(new View.OnClickListener() {
//				@Override
//				public void onClick(View pView) {
//
//					TextView lTextView = pView.findViewById(R.id.show_note_text);
//					ImageView lImageView = pView.findViewById(R.id.drop_down_img);
//
//					if(!pDiaryHolder.note.isSelected())
//					{
//						expand(pDiaryHolder.note);
//						pDiaryHolder.note.setSelected(true);
//						lTextView.setText(getString(R.string.hide_note));
//						lImageView.setImageResource(R.drawable.collapse_item);
//					}
//					else
//					{
//						collapse(pDiaryHolder.note);
//						pDiaryHolder.note.setSelected(false);
//						lTextView.setText(getString(R.string.show_note));
//						lImageView.setImageResource(R.drawable.expand_item);
//					}
//				}
//			});

		}

		@Override
		public int getItemCount() {
			return mCompleteMoods.size();
		}
		private CompleteMood getItem(int i)
		{
			return mCompleteMoods.get(i);
		}
	}

	private class DiaryHolder extends RecyclerView.ViewHolder{

		ImageView moodImage;
		TextView date;
		TextView note;
		CardView diaryItem;
//		TextView anxiety;
//		TextView selfConfidence;
//		TextView energyLevel;
//		TextView medication;
//		TextView mood;
//		View show_note;


		DiaryHolder(@NonNull View itemView) {
			super(itemView);

			moodImage = itemView.findViewById(R.id.moodImage);
//			anxiety = itemView.findViewById(R.id.anxiety_value);
//			selfConfidence = itemView.findViewById(R.id.self_confidence_value);
//			energyLevel = itemView.findViewById(R.id.energy_level_value);
			date = itemView.findViewById(R.id.date);
//			medication = itemView.findViewById(R.id.medication);
//			mood = itemView.findViewById(R.id.mood);
			note = itemView.findViewById(R.id.note);
			diaryItem = itemView.findViewById(R.id.card);
//			show_note = itemView.findViewById(R.id.show_note);
		}
	}

	public static void expand(final View v) {
		v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		final int targetHeight = v.getMeasuredHeight();

		// Older versions of android (pre API 21) cancel animations for views with a height of 0.
		v.getLayoutParams().height = 1;
		v.setVisibility(View.VISIBLE);
		Animation a = new Animation()
		{
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				v.getLayoutParams().height = interpolatedTime == 1
						? ViewGroup.LayoutParams.WRAP_CONTENT
						: (int)(targetHeight * interpolatedTime);
				v.requestLayout();
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp/ms
		a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
		v.startAnimation(a);
	}

	public static void collapse(final View v) {
		final int initialHeight = v.getMeasuredHeight();

		Animation a = new Animation()
		{
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				if(interpolatedTime == 1){
					v.setVisibility(View.GONE);
				}else{
					v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
					v.requestLayout();
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp/ms
		a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
		v.startAnimation(a);
	}
}
