package wasif.whatevervalue.com.pukaarapp.models;

public class Question4 {

    private String question;
    private String answer;

    public Question4(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    Question4(){

    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
