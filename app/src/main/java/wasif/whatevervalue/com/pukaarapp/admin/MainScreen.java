package wasif.whatevervalue.com.pukaarapp.admin;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.login.QuestionActivity1;

public class MainScreen extends AppCompatActivity {

    private static final String TAG = "MainScreen";

    TextView loginButton;
    Button getStarted;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainpage);

        loginButton = findViewById(R.id.login_button);
        getStarted=findViewById(R.id.getStarted);
        loginButton.setPaintFlags(loginButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLoginPage();
            }
        });

        getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToQuestionActivity1();
            }
        });
    }

    public void goToLoginPage(){
        Intent intent= new Intent(this,LoginScreen.class);
        startActivity(intent);
    }

    public void goToQuestionActivity1(){
        Intent intent= new Intent(this, QuestionActivity1.class);
        startActivity(intent);
    }

}
