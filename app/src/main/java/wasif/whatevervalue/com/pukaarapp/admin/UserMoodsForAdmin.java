package wasif.whatevervalue.com.pukaarapp.admin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Objects;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.dashboard.DiaryFragment;

public class UserMoodsForAdmin extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_moods_for_admin);

		String UID = Objects.requireNonNull(getIntent().getExtras()).getString("userID");

		getSupportFragmentManager().beginTransaction()
				.add( R.id.container, DiaryFragment.getInstance(UID, false, "")
						, DiaryFragment.TAG)
				.commit();

	}
}
