package wasif.whatevervalue.com.pukaarapp.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class BackgroundService extends IntentService {
	// TODO: Rename actions, choose action names that describe tasks that this
	// IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
	private static final String ACTION_User = "wasif.whatevervalue.com.pukaarapp.services.action.User";
	private static final String ACTION_Therapist = "wasif.whatevervalue.com.pukaarapp.services.action.Therapist";
	private static final String ACTION_Admin = "wasif.whatevervalue.com.pukaarapp.services.action.Therapist";

	// TODO: Rename parameters

	public BackgroundService() {
		super("BackgroundService");
	}

	/**
	 * Starts this service to perform action User with the given parameters. If
	 * the service is already performing a task this action will be queued.
	 *
	 * @see IntentService
	 */
	// TODO: Customize helper method
	public static void startActionUser(Context context) {
		Intent intent = new Intent(context, BackgroundService.class);
		intent.setAction(ACTION_User);
		context.startService(intent);
	}

	/**
	 * Starts this service to perform action Therapist with the given parameters. If
	 * the service is already performing a task this action will be queued.
	 *
	 * @see IntentService
	 */
	// TODO: Customize helper method
	public static void startActionTherapist(Context context) {
		Intent intent = new Intent(context, BackgroundService.class);
		intent.setAction(ACTION_Therapist);
		context.startService(intent);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (intent != null) {
			final String action = intent.getAction();
			if (ACTION_User.equals(action)) {
				handleActionUser();
			} else if (ACTION_Therapist.equals(action)) {
				handleActionTherapist();
			} else if (ACTION_Admin.equals(action)) {
				handleActionAdmin();
			}
		}
	}

	/**
	 * Handle action User in the provided background thread with the provided
	 * parameters.
	 */
	private void handleActionUser() {

		String UID = FirebaseAuth.getInstance().getUid();

	}

	/**
	 * Handle action Therapist in the provided background thread with the provided
	 * parameters.
	 */
	private void handleActionTherapist() {
	}

	private void handleActionAdmin() {
	}
}
