package wasif.whatevervalue.com.pukaarapp.session;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.dashboard.DashboardActivity;
import wasif.whatevervalue.com.pukaarapp.dashboard.MainFragment;
import wasif.whatevervalue.com.pukaarapp.intefaces.GetTAG;
import wasif.whatevervalue.com.pukaarapp.models.CompleteMood;
import wasif.whatevervalue.com.pukaarapp.therapistEnd.SelectMoodList;
import wasif.whatevervalue.com.pukaarapp.therapistEnd.TherapistPanelActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddNoteFragment extends Fragment implements GetTAG {


	public static final String TAG = "AddNoteFragment";

	public AddNoteFragment() {
		// Required empty public constructor
	}


	private static String mood;
	private static boolean mTherapist = false;
	private static String mUID = "";

	public static AddNoteFragment newInstance(String pMood, boolean therapist, String uid)
	{
		mTherapist = therapist;
		mUID = uid;
		mood = pMood;
		return new AddNoteFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_notes, null);

		final SeekBar anxiety = view.findViewById(R.id.anxiety_selection);
		final SeekBar confidence = view.findViewById(R.id.self_confidence_selection);
		final SeekBar energy = view.findViewById(R.id.energy_level_selection);

		SeekBar.OnSeekBarChangeListener lOnSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar pSeekBar, int pI, boolean pB) {
				int progress = pSeekBar.getProgress();
				if(pSeekBar == anxiety)
					progress-=10;

				((TextView)pSeekBar.getTag()).setText("" + (progress));
			}

			@Override
			public void onStartTrackingTouch(SeekBar pSeekBar) {

			}

			@Override
			public void onStopTrackingTouch(SeekBar pSeekBar) {

			}
		};

		final TextView note = view.findViewById(R.id.note);

		TextView anxietyValue = view.findViewById(R.id.anxiety_value);
		TextView confidenceValue = view.findViewById(R.id.self_confidence_value);
		final TextView energyValue = view.findViewById(R.id.energy_level_value);
		anxiety.setTag(anxietyValue);
		confidence.setTag(confidenceValue);
		energy.setTag(energyValue);

		anxiety.setOnSeekBarChangeListener(lOnSeekBarChangeListener);
		confidence.setOnSeekBarChangeListener(lOnSeekBarChangeListener);
		energy.setOnSeekBarChangeListener(lOnSeekBarChangeListener);

		TextView save = view.findViewById(R.id.save);

		save.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				final int anxietyValue = anxiety.getProgress()-10;
				final int confidenceValue = confidence.getProgress();
				final int energyValue = energy.getProgress();
				Date today = Calendar.getInstance().getTime();
				Calendar cal = Calendar.getInstance();
				cal.setTime(today);

				CompleteMood lCompleteMood = new CompleteMood(today.getTime(), mood, anxietyValue, confidenceValue, energyValue, note.getText().toString());
				String notesNode = "notes";
//				String moodAvgNode = "moodsAvg";
				if(mTherapist)
					notesNode = "therapistNotes";
				DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(notesNode).child(Objects.requireNonNull(FirebaseAuth.getInstance().getUid()));
				if(mTherapist) {
					ref = ref.child(mUID);
//					moodAvgNode = "moodsAvgTherapist";
				}
				ref.push().setValue(lCompleteMood);

//				DatabaseReference avg = FirebaseDatabase.getInstance().getReference().child(moodAvgNode)
//						.child(FirebaseAuth.getInstance().getUid());
//				if(mTherapist)
//					avg = avg.child(mUID);

//				final String finalMoodAvgNode = moodAvgNode;
//				final DatabaseReference finalAvg = avg;
//				avg.addListenerForSingleValueEvent(new ValueEventListener() {
//					@Override
//					public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {
//						int anxietySum = 0;
//						int energySum = 0;
//						int confidenceSum = 0;
//						int n = 0;
//						if(pDataSnapshot.getChildrenCount() > 0) {
//							anxietySum = ((Long) Objects.requireNonNull(pDataSnapshot.child("anxiety-sum").getValue())).intValue();
//							energySum = ((Long) Objects.requireNonNull(pDataSnapshot.child("energy-sum").getValue())).intValue();
//							confidenceSum = ((Long) Objects.requireNonNull(pDataSnapshot.child("confidence-sum").getValue())).intValue();
//							n = ((Long) Objects.requireNonNull(pDataSnapshot.child("n").getValue())).intValue();
//						}
//						anxietySum += anxietyValue;
//						energySum += energyValue;
//						confidenceSum += confidenceValue;
//						n++;
//
//						HashMap<String, Integer> lHashMap = new HashMap<>();
//						lHashMap.put("anxiety-sum", anxietySum);
//						lHashMap.put("energy-sum", energySum);
//						lHashMap.put("confidence-sum", confidenceSum);
//						lHashMap.put("n", n);
//
////						DatabaseReference reff = FirebaseDatabase.getInstance().getReference().child(finalMoodAvgNode)
////								.child(FirebaseAuth.getInstance().getUid());
////
////						if(mTherapist)
////							reff = reff.child(mUID);
//						finalAvg.setValue(lHashMap);
//					}
//
//					@Override
//					public void onCancelled(@NonNull DatabaseError pDatabaseError) {
//
//					}
//				});
				if(getActivity() instanceof DashboardActivity)
				((DashboardActivity)getActivity()).changeFragment(new MainFragment(), MainFragment.TAG);
				else if(getActivity() instanceof SelectMoodList)
					startActivity(new Intent(getContext(), TherapistPanelActivity.class));


			}
		});



		return view;
	}
	@Override
	public void onDestroy() {
		mTherapist = false;
		mUID = "";
		super.onDestroy();
	}
	@Override
	public String getTAG() {
		return TAG;
	}
}
