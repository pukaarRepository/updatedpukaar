package wasif.whatevervalue.com.pukaarapp.admin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.session.SessionFragment;

public class AdminChatActivity extends AppCompatActivity {

	public static final String TherapistID = "TherapistID";
	public static final String UserID = "userID";
	public static final String UserName = "UserName";
	public static final String TherapistName = "TherapistName";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_therapist_chat);

		getSupportFragmentManager().beginTransaction()
				.add(R.id.container, SessionFragment.getInstance(getIntent().getExtras().getString(UserID)
						, getIntent().getExtras().getString(TherapistID)
						)
						, SessionFragment.TAG).commit();

	}

}
