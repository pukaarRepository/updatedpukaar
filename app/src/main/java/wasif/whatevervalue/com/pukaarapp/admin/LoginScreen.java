package wasif.whatevervalue.com.pukaarapp.admin;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Timer;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.dashboard.DashboardActivity;
import wasif.whatevervalue.com.pukaarapp.login.IntroActivity;
import wasif.whatevervalue.com.pukaarapp.login.LoginActivity;
import wasif.whatevervalue.com.pukaarapp.login.RegisterActivity;
import wasif.whatevervalue.com.pukaarapp.models.Therapist;
import wasif.whatevervalue.com.pukaarapp.models.Timings;
import wasif.whatevervalue.com.pukaarapp.models.User;
import wasif.whatevervalue.com.pukaarapp.therapistEnd.TherapistPanelActivity;
import wasif.whatevervalue.com.pukaarapp.utils.FirebaseMethods;

import static android.widget.Toast.LENGTH_SHORT;

public class LoginScreen extends AppCompatActivity {

    private Timer timer;

    TextView signupButton;




    //firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mUserDatabase;

    private Context mContext;
    private ProgressBar mProgressBar;
    private EditText mEmail, mPassword;
    private TextView mPleaseWait;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private FirebaseMethods mFirebaseMethods;

    private static final String TAG = "LoginScreen";


    String question_1,answer_1;
    String question_2,answer_2;
    String question_3,answer_3;
    String question_4,answer_4;
    String question_5,answer_5,question_6;
    ArrayList<String> answer_6;
    ProgressBar progressBar;
    Button btnLogin;

    NumberProgressBar numberProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginpage);

        signupButton = findViewById(R.id.signup_button);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mPleaseWait = (TextView) findViewById(R.id.pleaseWait);
        btnLogin = (Button) findViewById(R.id.btn_login);
        signupButton.setPaintFlags(signupButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        mUserDatabase= FirebaseDatabase.getInstance().getReference().child("users");
        mEmail=findViewById(R.id.txt_email);
        mPassword=findViewById(R.id.txt_password);
        mFirebaseMethods= new FirebaseMethods(LoginScreen.this);


        mPleaseWait.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);




        setupFirebaseAuth();
        init();


        question_1= getIntent().getStringExtra("question1");
        answer_1= getIntent().getStringExtra("answer1");
        question_2= getIntent().getStringExtra("question2");
        answer_2= getIntent().getStringExtra("answer2");
        question_3= getIntent().getStringExtra("question3");
        answer_3= getIntent().getStringExtra("answer3");
        question_4= getIntent().getStringExtra("question4");
        answer_4= getIntent().getStringExtra("answer4");
        question_5= getIntent().getStringExtra("question5");
        answer_5= getIntent().getStringExtra("answer5");
        question_6= getIntent().getStringExtra("question6");
        answer_6= getIntent().getStringArrayListExtra("answer6");

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToRegisterPage();
            }
        });

    }


    public void goToRegisterPage(){
        Intent intent= new Intent(this, RegisterActivity.class);
        intent.putExtra("question1",question_1);
        intent.putExtra("answer1",answer_1);
        intent.putExtra("question2",question_2);
        intent.putExtra("answer2",answer_2);
        intent.putExtra("question3",question_3);
        intent.putExtra("answer3",answer_3);
        intent.putExtra("question4",question_4);
        intent.putExtra("answer4",answer_4);
        intent.putExtra("question5",question_5);
        intent.putExtra("answer5",answer_5);
        intent.putExtra("question6",question_6);
        intent.putExtra("answer6",answer_6);
        startActivity(intent);
    }
    public void goToIntroPage(){
        Intent intent= new Intent(this, IntroActivity.class);
        startActivity(intent);
    }


    private boolean isStringNull(String string){
        Log.d(TAG, "isStringNull: checking string if null.");

        if(string.equals("")){
            return true;
        }
        else{
            return false;
        }
    }

    /*
    ------------------------------------ Firebase ---------------------------------------------
     */

    private void init(){

        //initialize the button for logging in
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnLogin.setEnabled(false);
                Log.d(TAG, "onClick: attempting to log in.");
                final String email = mEmail.getText().toString();
                final String password = mPassword.getText().toString();

                if (isStringNull(email) && isStringNull(password)) {
                    Toast.makeText(mContext, "You must fill out all the fields", LENGTH_SHORT).show();
                } else {


                    mProgressBar.setVisibility(View.VISIBLE);
                    mPleaseWait.setVisibility(View.VISIBLE);


                    getAdmin(new LoginActivity.FirebaseCallBack() {
                        @Override
                        public void onCallback(wasif.whatevervalue.com.pukaarapp.models.Admin adminObj) {
                            if (adminObj.getEmail().equals(email) && adminObj.getPassword().equals(password)) {
                                Log.d(TAG, "onCallback: I am inside admin");
                                //goToAdminPanel();

                            } else {

                                mAuth.signInWithEmailAndPassword(email, password)
                                        .addOnCompleteListener(LoginScreen.this, new OnCompleteListener<AuthResult>() {
                                            @Override
                                            public void onComplete(@NonNull Task<AuthResult> task) {
                                                Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                                                FirebaseUser user = mAuth.getCurrentUser();

                                                // If sign in fails, display a message to the user. If sign in succeeds
                                                // the auth state listener will be notified and logic to handle the
                                                // signed in user can be handled in the listener.
                                                if (!task.isSuccessful()) {
                                                    Log.w(TAG, "signInWithEmail:failed", task.getException());

                                                    btnLogin.setEnabled(true);




                                                    Toast.makeText(LoginScreen.this, "Auth failed", LENGTH_SHORT).show();
                                                    //mProgressBar.setVisibility(View.GONE);
                                                    //mPleaseWait.setVisibility(View.GONE);
                                                } else{
                                                    checkThePerson();
                                                }
                                            }
                                        });
                            }
                        }
                    });
                }



            }
        });


         /*
         If the user is logged in then navigate to HomeActivity and call 'finish()'
          */
    }



    private void checkThePerson(){

        String current_user_id = mAuth.getCurrentUser().getUid();

        Log.d(TAG, "onComplete: " + current_user_id);

        checkTherapist(new FirebaseCallBack2() {
            @Override
            public void onCallback(String isThisTherapist) {

                Log.d(TAG, "onCallback: " + isThisTherapist);

                if (isThisTherapist.equals("IamTherapist")) {
                    Log.d(TAG, "onCallback: here inside therapist");
                    btnLogin.setEnabled(true);
                    goToTherapistPanel();
                }
                else{


                    btnLogin.setEnabled(true);
                    mProgressBar.setVisibility(View.INVISIBLE);
                    goToDashboardActivity();


                }
            }
        }, current_user_id);
    }


    private void checkUser(final FirebaseCallBack1 firebaseCallback){

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference("users");

        final String userID= mAuth.getCurrentUser().getUid();

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String found="false";


                if(found.equals("true")){
                    firebaseCallback.onCallback("therapistAssigned");
                }
                else{
                    firebaseCallback.onCallback("therapistNotAssigned");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    public interface FirebaseCallBack1 {
        void onCallback(String isUserAvailable);
    }

    public void goToTherapistPanel(){
        Intent intent = new Intent(LoginScreen.this, TherapistPanelActivity.class);
        startActivity(intent);
    }




    public void goToDashboardActivity(){

        Intent intent = new Intent(LoginScreen.this, DashboardActivity.class);
        startActivity(intent);
    }


    private void getAdmin(final LoginActivity.FirebaseCallBack firebaseCallback){

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference("Admin");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                wasif.whatevervalue.com.pukaarapp.models.Admin admin=dataSnapshot.getValue(wasif.whatevervalue.com.pukaarapp.models.Admin.class);
                Log.d(TAG, "onDataChange: "+ admin.getEmail());
                Log.d(TAG, "onDataChange: "+ admin.getPassword());
                firebaseCallback.onCallback(admin);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    private void checkTherapist(final FirebaseCallBack2 firebaseCallback, final String therapistID){

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference("therapists");
        Log.d(TAG, "checkTherapist: here now");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String found="false";

                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){

                    Therapist therapist= new Therapist();
                    Timings timings= new Timings();
                    therapist.setTherapist_id(dataSnapshot1.getKey());
                    therapist.setName(dataSnapshot1.child("name").getValue().toString());
                    therapist.setDescription(dataSnapshot1.child("description").getValue().toString());
                    therapist.setImageURL(dataSnapshot1.child("imageURL").getValue().toString());
                    timings.setMonday(dataSnapshot1.child("Timings").child("monday").getValue().toString());
                    timings.setTuesday(dataSnapshot1.child("Timings").child("tuesday").getValue().toString());
                    timings.setWednesday(dataSnapshot1.child("Timings").child("wednesday").getValue().toString());
                    timings.setThursday(dataSnapshot1.child("Timings").child("thursday").getValue().toString());
                    timings.setFriday(dataSnapshot1.child("Timings").child("friday").getValue().toString());
                    timings.setSaturday(dataSnapshot1.child("Timings").child("saturday").getValue().toString());
                    timings.setSunday(dataSnapshot1.child("Timings").child("sunday").getValue().toString());
                    therapist.setSpecialities(dataSnapshot1.child("specialities").getValue().toString());
                    therapist.setUsersHelped(( dataSnapshot1.child("usersHelped").getValue().toString()));
                    therapist.setTreatmentApproach(dataSnapshot1.child("treatmentApproach").getValue().toString());
                    therapist.setTimeZone(dataSnapshot1.child("timeZone").getValue().toString());
                    therapist.setLicense(dataSnapshot1.child("license").getValue().toString());
                    therapist.setLicenseDes(dataSnapshot1.child("licenseDes").getValue().toString());
                    therapist.setJoinedTime(dataSnapshot1.child("joinedTime").getValue().toString());
                    therapist.setTimePracticeMonths((dataSnapshot1.child("timePracticeMonths").getValue().toString()));
                    therapist.setDegree(dataSnapshot1.child("degree").getValue().toString());
                    for (DataSnapshot assignedUser : dataSnapshot1.child("assignedUsers").getChildren()) {
                        therapist.setAssignedUsers(assignedUser.getValue().toString());
                    }

                    therapist.setTimings(timings);

                    //Log.d(TAG, "onDataChange: "+ therapist.getAssignedToUser(0));

                    if(therapist.getTherapist_id().equals(therapistID)){
                        Log.d(TAG, "onDataChange: matched");
                        found="true";
                        break;
                    }
                }
                if(found.equals("true")){
                    firebaseCallback.onCallback("IamTherapist");
                }
                else{
                    firebaseCallback.onCallback("IamNotTherapist");
                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



    public interface FirebaseCallBack2 {
        void onCallback(String isThisTherapist);
    }


    public interface FirebaseCallBack {
        void onCallback(wasif.whatevervalue.com.pukaarapp.models.Admin adminObj);
    }

    /**
     * Setup the firebase auth object
     */
    private void setupFirebaseAuth(){
        Log.d(TAG, "setupFirebaseAuth: setting up firebase auth.");

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();

                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
}
