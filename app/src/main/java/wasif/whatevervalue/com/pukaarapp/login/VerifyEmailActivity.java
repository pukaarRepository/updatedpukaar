package wasif.whatevervalue.com.pukaarapp.login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.dashboard.DashboardActivity;
import wasif.whatevervalue.com.pukaarapp.session.SessionFragment;
import wasif.whatevervalue.com.pukaarapp.utils.FirebaseMethods;

import static android.widget.Toast.LENGTH_SHORT;

public class VerifyEmailActivity extends AppCompatActivity {

    private static final String TAG = "VerifyEmailActivity";


    private static final Boolean CHECK_IF_VERIFIED = false;

    //firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mUserDatabase;


    String question_1,answer_1;
    String question_2,answer_2;
    String question_3,answer_3;
    String question_4,answer_4;
    String question_5,answer_5,question_6;
    ArrayList<String> answer_6;

    String email,password;




    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_email);


        question_1= getIntent().getStringExtra("question1");
        answer_1= getIntent().getStringExtra("answer1");
        question_2= getIntent().getStringExtra("question2");
        answer_2= getIntent().getStringExtra("answer2");
        question_3= getIntent().getStringExtra("question3");
        answer_3= getIntent().getStringExtra("answer3");
        question_4= getIntent().getStringExtra("question4");
        answer_4= getIntent().getStringExtra("answer4");
        question_5= getIntent().getStringExtra("question5");
        answer_5= getIntent().getStringExtra("answer5");
        question_6= getIntent().getStringExtra("question6");
        answer_6= getIntent().getStringArrayListExtra("answer6");
        email=getIntent().getStringExtra("email");
        password=getIntent().getStringExtra("password");

        login();



    }

    private void login(){

        mAuth=FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(VerifyEmailActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                        FirebaseUser user = mAuth.getCurrentUser();

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail:failed", task.getException());

                            Toast.makeText(VerifyEmailActivity.this,"Auth failed",
                                    LENGTH_SHORT).show();
                        }
                        else{
                            try{
                                assert user != null;
                                if(user.isEmailVerified()){

                                    String current_user_id= mAuth.getCurrentUser().getUid();

                                    FirebaseMethods firebaseMethods= new FirebaseMethods(VerifyEmailActivity.this);
                                    firebaseMethods.addQuestion(question_1,answer_1,current_user_id);
                                    firebaseMethods.addQuestion(question_2,answer_2,current_user_id);
                                    firebaseMethods.addQuestion(question_3,answer_3,current_user_id);
                                    firebaseMethods.addQuestion(question_4,answer_4,current_user_id);
                                    firebaseMethods.addQuestion(question_5,answer_5,current_user_id);
                                    firebaseMethods.addMainQuestion(question_6,answer_6,current_user_id);
                                    Log.d(TAG, "onComplete: success. email is verified.");
                                    Intent intent = new Intent(VerifyEmailActivity.this, DashboardActivity.class);
                                    startActivity(intent);
                                }
                                else{
                                    Toast.makeText(VerifyEmailActivity.this, "Email is not verified \n check your email inbox.", LENGTH_SHORT).show();
                                    //mAuth.signOut();
                                }
                            }catch (NullPointerException e){
                                Log.e(TAG, "onComplete: NullPointerException: " + e.getMessage() );
                            }
                        }

                        // ...
                    }
                });
    }

    @Override
    protected void onResume() {
        login();
        super.onResume();
    }
}
