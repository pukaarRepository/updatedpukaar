package wasif.whatevervalue.com.pukaarapp.usersQuestioningPortal;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.dashboard.DashboardActivity;
import wasif.whatevervalue.com.pukaarapp.intefaces.GetTAG;
import wasif.whatevervalue.com.pukaarapp.models.ChatForum;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddPostFragment extends Fragment implements GetTAG {


	public static final String TAG = "AddPostFrgment";

	public AddPostFragment() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.add_post, null);

		final EditText post = view.findViewById(R.id.post);
		TextView share = view.findViewById(R.id.share);
		final Fragment lFragment = this;
		share.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				final String text = post.getText().toString();
				final String uid = FirebaseAuth.getInstance().getUid();

				assert uid != null;
				DatabaseReference user = FirebaseDatabase.getInstance().getReference().child("users")
						.child(uid);
				user.addListenerForSingleValueEvent(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {


						String lUsername = (String) pDataSnapshot.child("name").getValue();
						String pic= pDataSnapshot.child("ImageURL").toString();

						DatabaseReference ref = FirebaseDatabase.getInstance()
								.getReference().child("posts")
								.push();
//						String QID = ref.getKey();

						HashMap<String, Object> hashmap = new HashMap<>();
						hashmap.put("userName", lUsername);
						hashmap.put("userImg", pic);
						hashmap.put("userID", uid);
						hashmap.put("timestamp", Calendar.getInstance().getTime().getTime());
						hashmap.put("text", text);

						ref.setValue(hashmap);

						Objects.requireNonNull(getActivity()).getSupportFragmentManager()
								.beginTransaction().remove(lFragment)
								.commit();
					}

					@Override
					public void onCancelled(@NonNull DatabaseError pDatabaseError) {

					}
				});


			}
		});

		return view;
	}

	@Override
	public String getTAG() {
		return TAG;
	}
}
