package wasif.whatevervalue.com.pukaarapp.dashboard;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.database.MyDatabase;
import wasif.whatevervalue.com.pukaarapp.intefaces.GetTAG;
import wasif.whatevervalue.com.pukaarapp.models.Therapist;
import wasif.whatevervalue.com.pukaarapp.models.User;
import wasif.whatevervalue.com.pukaarapp.session.SessionFragment;
import wasif.whatevervalue.com.pukaarapp.session.SettingsFragment;
import wasif.whatevervalue.com.pukaarapp.session.TherapistFragment;
import wasif.whatevervalue.com.pukaarapp.utils.SectionsPagerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements GetTAG {

	public static final String TAG = "MainFragment";

	//widgets
	private ViewPager mViewPager;
	private FrameLayout mFrameLayout;
	private View mView;
	private RelativeLayout mRelativeLayout;
	private int selected=0;
	private ImageButton chat;
	private ImageButton settings;
	private CircleImageView therapist;
	FirebaseDatabase firebaseDatabase;
	DatabaseReference myRef;
	FirebaseAuth mAuth;
	String profilePicture="";

	public MainFragment() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		@SuppressLint("InflateParams") View lView = inflater.inflate(R.layout.fragment_main, null);
		mView = lView;
		mViewPager = lView.findViewById(R.id.viewpager_container);
		chat = lView.findViewById(R.id.chat_tab);
		settings = lView.findViewById(R.id.settings_tab);
		therapist = lView.findViewById(R.id.therapist_tab);
		mAuth=FirebaseAuth.getInstance();

		mFrameLayout = lView.findViewById(R.id.container);
		mRelativeLayout = lView.findViewById(R.id.relLayoutParent);

		setupViewPager();

		getTheTherapistPicture();

//		getAllUsers();

//		FacebookSdk.sdkInitialize(getApplicationContext());
//		Log.d("AppLog", "key:" + FacebookSdk.getApplicationSignature(getActivity()));


		return lView;
	}

	/**
	 * Responsible for adding the 3 tabs: Settings, Session, Therapist Profile
	 */
	private void setupViewPager(){
		SectionsPagerAdapter adapter = new SectionsPagerAdapter(Objects.requireNonNull(getActivity()).getSupportFragmentManager()){
		};

		adapter.addFragment(new SettingsFragment()); //index 0
		adapter.addFragment(new SessionFragment()); //index 1
		adapter.addFragment(new TherapistFragment()); //index 2
		mViewPager.setCurrentItem(1);
		mViewPager.setAdapter(adapter);


		mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int pI, float pV, int pI1) {

			}

			@Override
			public void onPageSelected(int pI) {
				if(getActivity() instanceof DashboardActivity) {

					selected = pI;

					if (selected == 2) {
						chat.setImageResource(R.drawable.message_icon);
						Picasso.get().load(profilePicture)
								.placeholder(R.drawable.user_selected).into(therapist);
						settings.setImageResource(R.drawable.menu);
						//mViewPager.setCurrentItem(2);
					} else if (selected == 1) {
						chat.setImageResource(R.drawable.message_icon_selected);
						Picasso.get().load(profilePicture)
								.placeholder(R.drawable.user_selected).into(therapist);
						 settings.setImageResource(R.drawable.menu);
//				    mViewPager.setCurrentItem(2);
					} else if (selected == 0) {
						chat.setImageResource(R.drawable.message_icon);
						Picasso.get().load(profilePicture)
								.placeholder(R.drawable.user_selected).into(therapist);
						settings.setImageResource(R.drawable.menu_selected);
//				    mViewPager.setCurrentItem(2);
					}

				}

			}

			@Override
			public void onPageScrollStateChanged(int pI) {

			}
		});

		settings.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				if(selected != 0) {
					chat.setImageResource(R.drawable.message_icon);
					therapist.setImageResource(R.drawable.user_unselected);
					settings.setImageResource(R.drawable.menu_selected);
					mViewPager.setCurrentItem(0);
					selected = 0;
				}
			}
		});

		chat.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				if(selected != 1) {
					chat.setImageResource(R.drawable.message_icon_selected);
					therapist.setImageResource(R.drawable.user_unselected);
					settings.setImageResource(R.drawable.menu);
					mViewPager.setCurrentItem(1);
					selected = 1;
				}
			}
		});

		therapist.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				if(selected != 2) {
					chat.setImageResource(R.drawable.message_icon);
					therapist.setImageResource(R.drawable.user_selected);
					settings.setImageResource(R.drawable.menu);
					mViewPager.setCurrentItem(2);
					selected = 2;
				}
			}
		});

//		TabLayout tabLayout = mView.findViewById(R.id.tabs);
//		tabLayout.setupWithViewPager(mViewPager);

//		Objects.requireNonNull(tabLayout.getTabAt(0)).setIcon(R.drawable.ic_arrow);
//		Objects.requireNonNull(tabLayout.getTabAt(1)).setIcon(R.drawable.ic_arrow);
//		Objects.requireNonNull(tabLayout.getTabAt(2)).setIcon(R.drawable.ic_arrow);
	}

	public void getAllUsers(){

        myRef=FirebaseDatabase.getInstance().getReference().child("users");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                MyDatabase db= MyDatabase.getInstance(getContext());
                for( DataSnapshot ds: dataSnapshot.getChildren()){
                    User user= ds.getValue(User.class);
                    //db.setUser(user);
                }


                //getTheTherapistPicture();

                setupViewPager();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



	public void getTheTherapistPicture(){


		profilePicture="https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg";


		myRef=FirebaseDatabase.getInstance().getReference().child("therapists");

		String currentID= mAuth.getCurrentUser().getUid();

		Log.d(TAG, "getTheTherapistPicture: "+ currentID);

		MyDatabase db = MyDatabase.getInstance(getContext());
		User user= db.getUser(currentID);

		Log.d(TAG, "getTheTherapistPicture: "+ user.getName());
		Log.d(TAG, "getTheTherapistPicture: "+ user.getAssignedTherapist());

		int index=0;

		for(int i=0;i<user.getStatus().size();i++){
			if(user.getStatus().get(i).equals("current")){
				index=i;
			}
		}

		if(!user.getAssignedTherapist().get(index).equals("none")){
			Therapist therapist=db.getTherapistProfile(user.getAssignedTherapist().get(index));
			profilePicture=therapist.getImageURL();
		}
		else{
			profilePicture="https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg";
		}




	}

	@Override
	public String getTAG() {
		return TAG;
	}
}
