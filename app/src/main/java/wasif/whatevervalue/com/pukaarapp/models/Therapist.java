package wasif.whatevervalue.com.pukaarapp.models;

import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by User on 6/26/2017.
 */

public class Therapist implements Serializable {

    private String name;
    private String therapist_id;
    private String phone_number;
    private String email;
    private String description;
    private String imageURL;
    private String license;
    private ArrayList<String> assignedUsers= new ArrayList<>();

    private String degree;
    private String joinedTime;
    private String licenseDes;
    private String specialities;
    private String timePracticeMonths;
    private String timeZone;
    private String treatmentApproach;
    private String usersHelped;
    private Timings timings;



    public Therapist(){

    }

    public Therapist(String name, String therapist_id, String phone_number, String email, String description, String imageURL, String license, ArrayList<String> assignedUsers, String degree, String joinedTime, String licenseDes, String specialities, String timePracticeMonths, String timeZone, String treatmentApproach, String usersHelped, Timings timings) {
        this.name = name;
        this.therapist_id = therapist_id;
        this.phone_number = phone_number;
        this.email = email;
        this.description = description;
        this.imageURL = imageURL;
        this.license = license;
        this.assignedUsers = assignedUsers;
        this.degree = degree;
        this.joinedTime = joinedTime;
        this.licenseDes = licenseDes;
        this.specialities = specialities;
        this.timePracticeMonths = timePracticeMonths;
        this.timeZone = timeZone;
        this.treatmentApproach = treatmentApproach;
        this.usersHelped = usersHelped;
        this.timings = timings;
    }

    public Timings getTimings() {
        return timings;
    }

    public void setTimings(Timings timings) {
        this.timings = timings;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTherapist_id() {
        return therapist_id;
    }

    public void setTherapist_id(String therapist_id) {
        this.therapist_id = therapist_id;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public ArrayList<String> getAssignedUsers() {
        return assignedUsers;
    }

    public void setAssignedUsers(String assignedUsers) {
        this.assignedUsers.add(assignedUsers);
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getJoinedTime() {
        return joinedTime;
    }

    public void setJoinedTime(String joinedTime) {
        this.joinedTime = joinedTime;
    }

    public String getLicenseDes() {
        return licenseDes;
    }

    public void setLicenseDes(String licenseDes) {
        this.licenseDes = licenseDes;
    }

    public String getSpecialities() {
        return specialities;
    }

    public void setSpecialities(String specialities) {
        this.specialities = specialities;
    }

    public String getTimePracticeMonths() {
        return timePracticeMonths;
    }

    public void setTimePracticeMonths(String timePracticeMonths) {
        this.timePracticeMonths = timePracticeMonths;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getTreatmentApproach() {
        return treatmentApproach;
    }

    public void setTreatmentApproach(String treatmentApproach) {
        this.treatmentApproach = treatmentApproach;
    }

    public String getUsersHelped() {
        return usersHelped;
    }

    public void setUsersHelped(String usersHelped) {
        this.usersHelped = usersHelped;
    }
}
