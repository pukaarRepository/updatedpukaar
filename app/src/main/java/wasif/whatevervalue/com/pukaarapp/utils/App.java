package wasif.whatevervalue.com.pukaarapp.utils;

import android.app.Application;
import android.content.Context;

/**
 * Created by Taha Malik on 10/02/2019.
 **/
public class App extends Application {

	private static Context mContext;

	@Override
	public void onCreate() {
		super.onCreate();
		mContext = this;
	}

	public static Context getContext(){
		return mContext;
	}
}