package wasif.whatevervalue.com.pukaarapp.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 6/26/2017.
 */

public class User implements Parcelable{

    private static final String TAG = "User";

    private String userID;
    private String phone_number;
    private String email;
    private String name;
    private String imageURL;
    private ArrayList<String> assignedTherapist= new ArrayList<>();
    private String password;
    private ArrayList<String> status= new ArrayList<>();

    public User( String email, String name,String imageURL, String phone_number,ArrayList<String> assignedTherapist,String password,ArrayList<String> status) {
        this.phone_number = phone_number;
        this.email = email;
        this.name = name;
        this.imageURL=imageURL;
        this.assignedTherapist=assignedTherapist;
        this.password=password;
        this.status=status;
    }

    public ArrayList<String> getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status.add(status);
    }

    public String getUser_id() {
        return userID;
    }

    public void setUser_id(String userID) {
        this.userID = userID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public User() {

    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getAssignedTherapist() {
        return assignedTherapist;
    }

    public void setAssignedTherapist(String assignedTherapist) {
        Log.d(TAG, "setAssignedTherapist: "+ assignedTherapist);
        this.assignedTherapist.add(assignedTherapist);
    }

    public static Creator<User> getCREATOR() {
        return CREATOR;
    }

    protected User(Parcel in) {
        phone_number = in.readString();
        email = in.readString();
        name = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };


    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public String toString() {
        return "User{" +
                ", phone_number='" + phone_number + '\'' +
                ", email='" + email + '\'' +
                ", username='" + name + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(phone_number);
        dest.writeString(email);
        dest.writeString(name);
    }
}
