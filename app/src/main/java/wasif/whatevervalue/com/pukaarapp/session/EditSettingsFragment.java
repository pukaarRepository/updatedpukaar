package wasif.whatevervalue.com.pukaarapp.session;


import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.admin.LoginScreen;
import wasif.whatevervalue.com.pukaarapp.intefaces.GetTAG;
import wasif.whatevervalue.com.pukaarapp.login.IntroActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditSettingsFragment extends Fragment implements GetTAG {


	public EditSettingsFragment() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		View lView = inflater.inflate(R.layout.fragment_edit_settings
				, container, false);

		final View logout = lView.findViewById(R.id.logout);
		logout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				FirebaseAuth.getInstance().signOut();
				Intent lIntent = new Intent(getContext(), LoginScreen.class);
				startActivity(lIntent);
			}
		});

		final View shareMoodsSettings = lView.findViewById(R.id.share_mood);

		final String UID = FirebaseAuth.getInstance().getUid();
		final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

		final boolean[] isTherapist = {false};

		ref.child("therapists")
				.addListenerForSingleValueEvent(
						new ValueEventListener() {
							@Override
							public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {

								if (pDataSnapshot.hasChild
										(UID + "/" + "therapist_id")) {
									//TODO: Open Therapist Fragment
									shareMoodsSettings.setVisibility(View.GONE);
									isTherapist[0] = true;
								}
							}

							@Override
							public void onCancelled(@NonNull DatabaseError pDatabaseError) {

							}
						});

		assert UID != null;
		if(!isTherapist[0])
		ref.child("ShareMood").child(UID)
				.addListenerForSingleValueEvent(
						new ValueEventListener() {
							@Override
							public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {

								if(pDataSnapshot.getValue() != null) {

									String share_mood = "false";
									if(pDataSnapshot.hasChild("share_mood"))
										share_mood = ((String)pDataSnapshot.child("share_mood").getValue());

									if(share_mood.equals("true"))
									{
										((TextView)shareMoodsSettings.findViewById(R.id.checkbox))
												.setCompoundDrawables(null,
												null, getResources()
														.getDrawable(android.R.drawable.checkbox_on_background)
												, null);
										shareMoodsSettings.setTag("selected");
										shareMoodsSettings.invalidate();


									}
									else
									{
										((TextView)shareMoodsSettings
												.findViewById(R.id.checkbox))
												.setCompoundDrawables(null,null
														, null, null);
										shareMoodsSettings.setTag("null");
										shareMoodsSettings.invalidate();

									}

									shareMoodsSettings.setVisibility(View.VISIBLE);

								}
								else {
									ref.child("ShareMood").child(UID).child("share_mood").setValue("false");
									shareMoodsSettings.setTag("null");
									shareMoodsSettings.invalidate();

								}

								shareMoodsSettings.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View pView) {
										if(shareMoodsSettings.getTag().equals("null"))
										{
											ref.child("ShareMood").child(UID).child("share_mood").setValue("selected")
													.addOnCompleteListener(new OnCompleteListener<Void>() {
														@Override
														public void onComplete(@NonNull Task<Void> pTask) {
															((TextView)shareMoodsSettings.findViewById(R.id.checkbox)).setCompoundDrawables(null,
																	null
																	, getResources().getDrawable(android.R.drawable.checkbox_on_background)
																	, null);
															shareMoodsSettings.invalidate();
															shareMoodsSettings.setTag("selected");

														}
													});
										}
										else
										{
											ref.child("ShareMood").child(UID)
													.child("share_mood").setValue("null")
													.addOnCompleteListener(new OnCompleteListener<Void>() {
														@Override
														public void onComplete(@NonNull Task<Void> pTask) {
															((TextView)shareMoodsSettings
																	.findViewById(R.id.checkbox))
																	.setCompoundDrawables(null,
																	null, null, null);
															shareMoodsSettings.setTag("null");
															shareMoodsSettings.invalidate();


														}
													});
										}
									}
								});


							}

							@Override
							public void onCancelled(@NonNull DatabaseError pDatabaseError) {

							}
						});

		final String Email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
		ref.child("Admin").addListenerForSingleValueEvent(
				new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {
						if(Objects.requireNonNull(pDataSnapshot
								.child("email")
								.getValue()).equals(Email))
						{

						}
					}

					@Override
					public void onCancelled(@NonNull DatabaseError pDatabaseError) {

					}
				}
		);
		return lView;

	}


	public final static String TAG = "EditSettingsFragment";

	@Override
	public String getTAG() {
		return TAG;
	}


}
