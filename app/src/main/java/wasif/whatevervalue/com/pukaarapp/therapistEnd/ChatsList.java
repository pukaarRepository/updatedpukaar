package wasif.whatevervalue.com.pukaarapp.therapistEnd;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.models.User;
import wasif.whatevervalue.com.pukaarapp.utils.FirebaseMethods;

public class ChatsList extends AppCompatActivity {

    private static final String TAG = "ChatsList";

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mUserIds = new ArrayList<>();
    private ArrayList<User> mUsers= new ArrayList<>();

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats_list);
        Log.d(TAG, "onCreate: started.");

        initImageBitmaps();
    }


    private void initImageBitmaps(){
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                getUsers(new FirebaseCallBack() {
                    @Override
                    public void onCallback(ArrayList<User> users ) {

                        for(int i=0;i<users.size();i++){
                            Log.d(TAG, "onCallback: "+ users.get(i).getName());
                            mNames.add(users.get(i).getName());
                            mImageUrls.add(users.get(i).getImageURL());
                            mUserIds.add(users.get(i).getUser_id());
                        }

                        initRecyclerView();

                    }
                },dataSnapshot);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



    private void getUsers(final FirebaseCallBack firebaseCallback, DataSnapshot dataSnapshot){

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        mAuth = FirebaseAuth.getInstance();
        String therapistID= mAuth.getCurrentUser().getUid();

        //String current_id=mAuth.getCurrentUser().getUid();

        //Log.d(TAG, "getUsers: "+ current_id);

        final FirebaseMethods firebaseMethods= new FirebaseMethods(this);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final ArrayList<String> userIDs=firebaseMethods.getAssignedUsersIDs(dataSnapshot);
                Log.d(TAG, "onDataChange: "+ userIDs);

                myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        mUsers=firebaseMethods.getAssignedUsers(dataSnapshot,userIDs);

                        Log.d(TAG, "onDataChange: "+ mUsers);


                        firebaseCallback.onCallback(mUsers);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public interface FirebaseCallBack {
        void onCallback(ArrayList<User> users);
    }

    private void initRecyclerView(){
        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = findViewById(R.id.recyclerv_view);
        ChatsListAdapter adapter = new ChatsListAdapter(this, mNames, mImageUrls,mUserIds);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
