package wasif.whatevervalue.com.pukaarapp.admin;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.database.MyDatabase;
import wasif.whatevervalue.com.pukaarapp.models.Therapist;
import wasif.whatevervalue.com.pukaarapp.utils.FirebaseMethods;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by User on 1/1/2018.
 */

@SuppressWarnings({"unused", "UnnecessaryLocalVariable"})
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> implements View.OnClickListener {

    private static final String TAG = "RecyclerViewAdapter";

    private ArrayList<String> mImageNames;
    private ArrayList<String> mImages;
    private ArrayList<String> mTherapistIds;
    private Context mContext;
    String phoneNo;
    String message;
    final int MY_PERMISSIONS_REQUEST_SEND_SMS = 1000;
    final int PERMISSION_REQUEST_CODE=1;


    public RecyclerViewAdapter(Context context, ArrayList<String> imageNames, ArrayList<String> images, ArrayList<String> ids) {
        Log.d(TAG, "RecyclerViewAdapter: I am hereee");
        mImageNames = imageNames;
        mImages = images;
        mTherapistIds = ids;
        mContext = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: I am inside");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Glide.with(mContext)
                .asBitmap()
                .load(mImages.get(position))
                .into(holder.image);

        holder.imageName.setText(mImageNames.get(position));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on: " + mImageNames.get(position));

                Toast.makeText(mContext, mImageNames.get(position), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(mContext, GalleryActivity.class);
                intent.putExtra("image_url", mImages.get(position));
                intent.putExtra("image_name", mImageNames.get(position));
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImageNames.size();
    }

    @Override
    public void onClick(View v) {

//        if (v.equals(R.id.btn_cancel)) {
//            Log.d("Nothing", "Nothing");
//        }
    }


    @SuppressWarnings("WeakerAccess")
    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView image;
        TextView imageName;
        RelativeLayout parentLayout;
        Button removeTherapist;
        Button updateTherapist;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            imageName = itemView.findViewById(R.id.image_name);
            parentLayout = itemView.findViewById(R.id.parent_layout);
            removeTherapist = itemView.findViewById(R.id.btn_cancel);
            updateTherapist=itemView.findViewById(R.id.btn_update);

            removeTherapist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeAt(getAdapterPosition());
                }
            });

            updateTherapist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateAt(getAdapterPosition());
                }
            });
        }


        public void removeAt(int position) {

            FirebaseMethods firebaseMethods= new FirebaseMethods(mContext);
            String therapistID=mTherapistIds.get(position);
            firebaseMethods.removeTherapist(therapistID);


            MyDatabase db = MyDatabase.getInstance(getApplicationContext());
            db.deleteTherapist(therapistID);


            mImageNames.remove(position);
            mTherapistIds.remove(position);
            mImages.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, mImages.size());
        }

        public void updateAt(int position){


            MyDatabase db = MyDatabase.getInstance(getApplicationContext());
            String therapistID=mTherapistIds.get(position);
            Therapist therapist=db.getTherapistProfile(therapistID);
            goToAdminActivity(therapist);

        }


        public void goToAdminActivity(Therapist therapist){
           // Intent intent= new Intent(getApplicationContext(), AdminActivity.class);
            //intent.putExtra("therapist",therapist);
            //intent.putExtra("path","update");
            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
            //mContext.startActivity(intent);
        }
    }
}















