package wasif.whatevervalue.com.pukaarapp.usersQuestioningPortal;


import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.database.MyDatabase;
import wasif.whatevervalue.com.pukaarapp.intefaces.GetTAG;
import wasif.whatevervalue.com.pukaarapp.models.Response;
import wasif.whatevervalue.com.pukaarapp.models.User;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PostDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PostDetailFragment extends Fragment implements GetTAG {
	public static final String TAG = "PostDetailFragment";
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_USERNAME = "param1";
	private static final String ARG_USERPIC = "param2";
	private static final String ARG_POST = "post";
	private static final String ARG_DATE = "date";
	private static final String ARG_COMMENTS = "comments";
	private static final String ARG_QID = "QID";

	// TODO: Rename and change types of parameters
	private String username;
	private String pic;
	private String QID;
	private String post;
	private String date;
	private int commentNo;


	public PostDetailFragment() {
		// Required empty public constructor
	}

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @param pUserName Parameter 1.
	 * @param pUserPic Parameter 2.
	 * @return A new instance of fragment PostDetailFragment.
	 */
	// TODO: Rename and change types and number of parameters
	public static PostDetailFragment newInstance(@NonNull String pUserName, @NonNull String pUserPic, @NonNull String pPost, @NonNull String date, int comments, @NonNull String QID) {
		PostDetailFragment fragment = new PostDetailFragment();
		Bundle args = new Bundle();
		args.putString(ARG_USERNAME, pUserName);
		args.putString(ARG_USERPIC, pUserPic);
		args.putString(ARG_POST, pPost);
		args.putString(ARG_DATE, date);
		args.putInt(ARG_COMMENTS, comments);
		args.putString(ARG_QID, QID);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			username = getArguments().getString(ARG_USERNAME);
			pic = getArguments().getString(ARG_USERPIC);
			post = getArguments().getString(ARG_POST);
			date = getArguments().getString(ARG_DATE);
			commentNo = getArguments().getInt(ARG_COMMENTS);
			QID = getArguments().getString(ARG_QID);
		}
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View lView = inflater.inflate(R.layout.complete_post, container, false);
		TextView userNameView = lView.findViewById(R.id.username);
		TextView dateView = lView.findViewById(R.id.date);
		TextView postView = lView.findViewById(R.id.query);
		TextView commentsView = lView.findViewById(R.id.comment);
		CircleImageView userpic = lView.findViewById(R.id.user_pic);
		Picasso.get().load(pic).fit().centerInside()
				.placeholder(R.drawable.default_avatar).into(userpic);
		ImageView back = lView.findViewById(R.id.back);
		final Fragment lFragment = this;
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
						.remove(lFragment)
						.commit();
			}
		});
		userNameView.setText(username);
		dateView.setText(date);
		postView.setText(post);
		commentsView.setText(commentNo+" Comments");

		final RecyclerView comments = lView.findViewById(R.id.comments);
		ArrayList<Response> lResponses = new ArrayList<>();

		comments.setAdapter(new ResponseAdapter(lResponses));
		comments.setLayoutManager(new LinearLayoutManager(getContext()));
		loadResponsesIn(comments, QID);
		comments.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);

				if (!recyclerView.canScrollVertically(1)) {
					loadResponsesIn(comments, QID);
				}
			}
		});

		final EditText sendResponse = lView.findViewById(R.id.send_response);
		TextView postBtn = lView.findViewById(R.id.post_btn);
		postBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {

				DatabaseReference ref= FirebaseDatabase.getInstance()
						.getReference().child("posts").child(QID).child("comments").push();
				MyDatabase lMyDatabase = MyDatabase.getInstance(getContext());
				String uid = FirebaseAuth.getInstance().getUid();
				User lUser = new User();
				HashMap<String, Object> lHashMap = new HashMap<>();
				lHashMap.put("text", sendResponse.getText().toString());
				lHashMap.put("timestamp", Calendar.getInstance().getTime().getTime());
				//lHashMap.put("userName", lUser.getName());
				lHashMap.put("userID", FirebaseAuth.getInstance().getUid());
				//lHashMap.put("userImg", lUser.getImageURL());

				ref.setValue(lHashMap);

				((ResponseAdapter) Objects.requireNonNull(comments.getAdapter())).mResponses.add(new Response(ref.getKey(),
						uid, Calendar.getInstance().getTime().getTime(),
						sendResponse.getText().toString(),
						lUser.getName(), lUser.getImageURL()));
				sendResponse.setText("");

			}
		});

		return lView;
	}

	@Override
	public String getTAG() {
		return TAG;
	}

	private class ResponseAdapter extends RecyclerView.Adapter<ResponseHolder>
	{

		ArrayList<Response> mResponses;

		ResponseAdapter(@NonNull ArrayList<Response> pResponses) {
			mResponses = pResponses;
		}

		@NonNull
		@Override
		public ResponseHolder onCreateViewHolder(@NonNull ViewGroup pViewGroup, int pI) {
			View lView = LayoutInflater.from(getContext())
					.inflate(R.layout.comment_view, pViewGroup, false);
			return new ResponseHolder(lView);
		}

		@Override
		public void onBindViewHolder(@NonNull final ResponseHolder pResponseHolder, int pI) {
			final Response lResponse = getItem(pI);

			pResponseHolder.response.setText(lResponse.text);
			Picasso.get().load(lResponse.userImg).placeholder(R.drawable.default_avatar)
					.fit().centerInside().into(pResponseHolder.mCircleImageView);
			pResponseHolder.username.setText(lResponse.username);
//			String uid = lResponse.userID;
//			DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
//					.child(getString(R.string.users_table_firebase))
//					.child(uid);
//			ref.addListenerForSingleValueEvent(new ValueEventListener() {
//				@Override
//				public void onDataChange(@NonNull final DataSnapshot pDataSnapshot) {
//					final String picURL = (String) pDataSnapshot.child("imageURL")
//							.getValue();
//					final String username = (String) pDataSnapshot.child("name").getValue();
//					pResponseHolder.username.setText(username);
//					Picasso.get().load(picURL).placeholder(R.drawable.default_avatar).
//							fit().centerInside().into(pResponseHolder.mCircleImageView);
//					notifyDataSetChanged();
//				}
//
//				@Override
//				public void onCancelled(@NonNull DatabaseError pDatabaseError) {
//
//				}
//			});
		}

		private Response getItem(int pI) {
			return mResponses.get(pI);
		}

		@Override
		public int getItemCount() {
			return mResponses.size();
		}
	}

	private class ResponseHolder extends RecyclerView.ViewHolder {

		CircleImageView mCircleImageView;
		TextView username;
		TextView response;

		ResponseHolder(@NonNull View itemView) {
			super(itemView);
			mCircleImageView = itemView.findViewById(R.id.user_pic);
			username = itemView.findViewById(R.id.username);
			response = itemView.findViewById(R.id.response);
		}

	}


	private void loadResponsesIn(final RecyclerView pRecyclerView, String QID) {

		final ArrayList<Response> pResponses = ((ResponseAdapter) Objects.requireNonNull(pRecyclerView.getAdapter())).mResponses;

		DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
		ref = ref.child("posts")
				.child(QID).child("comments");
		Query query = ref.orderByChild("timestamp").limitToFirst(30);

		if(pResponses.size() > 0)
		{
			query = query.startAt(pResponses.get(pResponses.size()-1).timeStamp+1);
		}

		query.addListenerForSingleValueEvent(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {

						Iterable<DataSnapshot> lDataSnapshots = pDataSnapshot.getChildren();
						int num = 0;
						for (DataSnapshot dshot :
								lDataSnapshots) {
							Response lResponse = new Response(dshot.getKey()
									, Objects.requireNonNull(dshot.child("userID").getValue()).toString()
									, (long) dshot.child("timestamp").getValue()
									,(String) dshot.child("text").getValue(), username, pic);

							pResponses.add(lResponse);


						}
						pRecyclerView.getAdapter().notifyDataSetChanged();
					}

					@Override
					public void onCancelled(@NonNull DatabaseError pDatabaseError) {

					}
				});



	}

}
