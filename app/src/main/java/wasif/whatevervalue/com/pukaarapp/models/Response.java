package wasif.whatevervalue.com.pukaarapp.models;

import java.util.Date;

/**
 * Created by Taha Malik on 01/02/2019.
 **/
public class Response {

	public String userID;
	public long timeStamp;
	public String text;
	public String RID;
	public String username;
	public String userImg;

	public Response(String pRID, String pUID, long pDate, String pResponse, String pUsername, String pUserImg) {
		RID = pRID;
		userID = pUID;
		timeStamp = pDate;
		text = pResponse;
		username = pUsername;
		userImg = pUserImg;
//		QID = pQID;
	}
}
