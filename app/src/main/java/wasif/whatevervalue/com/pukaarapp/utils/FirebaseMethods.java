package wasif.whatevervalue.com.pukaarapp.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.Objects;

import wasif.whatevervalue.com.pukaarapp.database.MyDatabase;
import wasif.whatevervalue.com.pukaarapp.models.Therapist;
import wasif.whatevervalue.com.pukaarapp.models.Timings;
import wasif.whatevervalue.com.pukaarapp.models.UserQuestions;
import wasif.whatevervalue.com.pukaarapp.models.User;

public class FirebaseMethods extends Activity {

    private static final String TAG = "FirebaseMethods";
    private Context mContext;


    //firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private DatabaseReference myRef1;
    private String userID;
    private DatabaseReference mUserDatabase;
    private StorageReference mStorageReference;
    ArrayList<String> specialitiesMatched= new ArrayList<>();
    final int PERMISSION_REQUEST_CODE=1;

    public FirebaseMethods(Context context) {
        //Log.d(TAG, "FirebaseMethods: hello sup?");
        //mAuth = FirebaseAuth.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        myRef1 = mFirebaseDatabase.getReference();
        mStorageReference = FirebaseStorage.getInstance().getReference();
        mContext = context;
        if(mAuth.getCurrentUser() != null){
            userID = mAuth.getCurrentUser().getUid();
        }
    }

    public void addNewUser(String userCurrentID,String email, String nickname, String profile_photo,String phoneNumber,ArrayList<String> assignedTherapist,String password){

        User user = new User( email,  nickname, profile_photo,phoneNumber,null,password ,null);

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference("users");

        myRef.child(userCurrentID).setValue(user);

        myRef.child(userCurrentID).child("assignedTherapist").child("none").setValue("none");

        //deleteUselessQuestions();


    }

    public void deleteUselessQuestions(){
        myRef=FirebaseDatabase.getInstance().getReference("questions");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot ds: dataSnapshot.getChildren()){
                    if(!ds.hasChild("userID")){
                        ds.getRef().removeValue();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void registerNewEmail(final String email, final String name, final String password, final String profilePicture, final String phoneNumber, final String assignedTherapist){

        final String[] userCurrentID = {""};

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(mContext, "Register didn't happen",
                                    Toast.LENGTH_SHORT).show();

                        }
                        else if(task.isSuccessful()){


                            userCurrentID[0] =mAuth.getCurrentUser().getUid();
                            ArrayList<String> assignedTherapists= new ArrayList<>();
                            addNewUser(userCurrentID[0],email,name,"none",phoneNumber,assignedTherapists,password);

                            User dbUser= new User();
                            dbUser.setEmail(email);
                            dbUser.setName(name);
                            dbUser.setAssignedTherapist(assignedTherapist);
                            dbUser.setPhone_number(phoneNumber);
                            dbUser.setImageURL(profilePicture);
                            dbUser.setUser_id(userCurrentID[0]);
                            dbUser.setPassword(password);
                            dbUser.setStatus("none");


                            MyDatabase db = MyDatabase.getInstance(mContext);
                            db.setUser(dbUser);


                            //send verificaton email
                            sendVerificationEmail();
                            //userID = mAuth.getCurrentUser().getUid();
                            //Log.d(TAG, "onComplete: Authstate changed: " + userID);
                        }


                    }
                });
    }

    public void sendVerificationEmail(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        Log.d(TAG, "sendVerificationEmail: Inside Verification function");

        if(user != null){
            user.sendEmailVerification()
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){

                            }else{
                                Toast.makeText(mContext, "couldn't send verification email.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    public String sendUserID(String userID){
        return userID;
    }


    public void registerNewTherapistEmail(final String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(mContext, "Register didn't happen",
                                    Toast.LENGTH_SHORT).show();

                        }
                        else if(task.isSuccessful()){
                            //send verificaton email
                            //sendVerificationEmail();

                            //userID = mAuth.getCurrentUser().getUid();
                            Log.d(TAG, "onComplete: Authstate changed: " + userID);
                        }

                    }
                });
    }

    public void uploadNewPhoto(final String imgUrl,
                               Bitmap bm){


        Log.d(TAG, "uploadNewPhoto: attempting to uplaod new photo.");
        String link="";
        FilePaths filePaths = new FilePaths();


        String user_id = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();
            final StorageReference storageReference = mStorageReference
                    .child(filePaths.FIREBASE_IMAGE_STORAGE + "/" + user_id + "/profile_photo");

            //convert image url to bitmap
            if (bm == null) {
                bm = ImageManager.getBitmap(imgUrl);
            }
            byte[] bytes = ImageManager.getBytesFromBitmap(bm, 100);

            UploadTask uploadTask;
            uploadTask = storageReference.putBytes(bytes);

            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw Objects.requireNonNull(task.getException());
                    }
                    return storageReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        System.out.println("Upload " + downloadUri);
                        //Toast.makeText(mActivity, "Successfully uploaded", Toast.LENGTH_SHORT).show();
                        if (downloadUri != null) {

                            String photoStringLink = downloadUri.toString(); //YOU WILL GET THE DOWNLOAD URL HERE !!!!
                            System.out.println("Upload " + photoStringLink);

                            Toast.makeText(mContext, "photo upload success", Toast.LENGTH_SHORT).show();

                            //add the new photo to 'photos' node and 'user_photos' node
                            //setProfilePhoto(photoStringLink);
                        }
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d(TAG, "onFailure: Photo upload failed.");
                    Toast.makeText(mContext, "Photo upload failed ", Toast.LENGTH_SHORT).show();
                }
            });
        }

    private void setProfilePhoto(String url){
        Log.d(TAG, "setProfilePhoto: setting new profile image: " + url);

        myRef = mFirebaseDatabase.getReference("therapists");

        myRef.child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                .child("profile_photo")
                .setValue(url);
    }

    public void setTheCurrentUser(DataSnapshot dataSnapshot2){

        String userID=mAuth.getCurrentUser().getUid();

        User user= new User();




        for(DataSnapshot dataSnapshot: dataSnapshot2.child("users").getChildren()) {
            if(dataSnapshot.getKey().equals(userID)){
                Log.d(TAG, "setTheCurrentUser: I am awesome af ");
                user.setUser_id(dataSnapshot.getKey());
                user.setImageURL(dataSnapshot.child("imageURL").getValue().toString());
                user.setPassword(dataSnapshot.child("password").getValue().toString());
                user.setEmail(dataSnapshot.child("email").getValue().toString());
                user.setName(dataSnapshot.child("name").getValue().toString());
                Log.d(TAG, "getAllUsers: "+ dataSnapshot.child("name").getValue().toString());
                user.setPhone_number(dataSnapshot.child("phone_number").getValue().toString());
                for(DataSnapshot dataSnapshot1 : dataSnapshot.child("assignedTherapist").getChildren()){
                    Log.d(TAG, "getAllUsers: "+ dataSnapshot1.getKey());
                    user.setAssignedTherapist(dataSnapshot1.getKey().toString());
                    user.setStatus(dataSnapshot1.getValue().toString());
                }
            }
            else{
                Log.d(TAG, "setTheCurrentUser: kia backwas hai");
            }
        }

        MyDatabase myDatabase= MyDatabase.getInstance(mContext);
        myDatabase.setUser(user);
    }

    public void setTheCurrentTherapist(DataSnapshot dataSnapshot2){

        String userID=mAuth.getCurrentUser().getUid();


        MyDatabase myDatabase= MyDatabase.getInstance(mContext);

        User user= myDatabase.getUser(userID);

        int index=0;
        for(int i=0;i<user.getStatus().size();i++){
            if(user.getStatus().get(i).equals("current")){
                index=i;
            }
        }

        String therapistID= user.getAssignedTherapist().get(index);

        Therapist therapist= new Therapist();
        Timings timings= new Timings();


        for(DataSnapshot dataSnapshot: dataSnapshot2.child("therapists").getChildren()) {
            if(dataSnapshot.getKey().equals(therapistID)){
                Log.d(TAG, "setTheCurrentUser: I am awesome af ");
                therapist.setTherapist_id(therapistID);
                therapist.setDegree(dataSnapshot.child("degree").getValue().toString());
                therapist.setDescription(dataSnapshot.child("description").getValue().toString());
                therapist.setImageURL(dataSnapshot.child("imageURL").getValue().toString());
                therapist.setJoinedTime(dataSnapshot.child("joinedTime").getValue().toString());
                therapist.setLicense(dataSnapshot.child("license").getValue().toString());
                therapist.setLicenseDes(dataSnapshot.child("licenseDes").getValue().toString());
                therapist.setName(dataSnapshot.child("name").getValue().toString());
                therapist.setSpecialities(dataSnapshot.child("specialities").getValue().toString());
                therapist.setTimePracticeMonths(dataSnapshot.child("timePracticeMonths").getValue().toString());
                therapist.setTimeZone(dataSnapshot.child("timeZone").getValue().toString());
                therapist.setTreatmentApproach(dataSnapshot.child("treatmentApproach").getValue().toString());
                therapist.setUsersHelped(dataSnapshot.child("usersHelped").getValue().toString());
                timings.setMonday(dataSnapshot.child("Timings").child("monday").getValue().toString());
                timings.setTuesday(dataSnapshot.child("Timings").child("tuesday").getValue().toString());
                timings.setWednesday(dataSnapshot.child("Timings").child("wednesday").getValue().toString());
                timings.setThursday(dataSnapshot.child("Timings").child("thursday").getValue().toString());
                timings.setFriday(dataSnapshot.child("Timings").child("friday").getValue().toString());
                timings.setSaturday(dataSnapshot.child("Timings").child("saturday").getValue().toString());
                timings.setSunday(dataSnapshot.child("Timings").child("sunday").getValue().toString());

                for(DataSnapshot dataSnapshot1 : dataSnapshot.child("assignedUsers").getChildren()){
                    Log.d(TAG, "getAllUsers: "+ dataSnapshot1.getKey());
                    therapist.setAssignedUsers(dataSnapshot1.getKey().toString());
                }
            }
            else{
                Log.d(TAG, "setTheCurrentUser: kia backwas hai");
            }
        }

        therapist.setTimings(timings);

        MyDatabase myDatabase1= MyDatabase.getInstance(mContext);
        myDatabase1.setTherapistProfile(therapist);
    }

    public static String getTableAsString(SQLiteDatabase db, String tableName) {
        Log.d(TAG, "getTableAsString called");
        String tableString = String.format("Table %s:\n", tableName);
        Cursor allRows  = db.rawQuery("SELECT * FROM " + tableName, null);
        if (allRows.moveToFirst() ){
            String[] columnNames = allRows.getColumnNames();
            do {
                for (String name: columnNames) {
                    tableString += String.format("%s: %s\n", name,
                            allRows.getString(allRows.getColumnIndex(name)));
                }
                tableString += "\n";

            } while (allRows.moveToNext());
        }

        return tableString;
    }

    public void removeTherapist(String therapist_id){

        myRef = mFirebaseDatabase.getReference("therapists");
        myRef.child(therapist_id).removeValue();
    }

    public void getTherapistForUpdate(final FirebaseCallBack firebaseCallback,String therapist_id){
        myRef = mFirebaseDatabase.getReference("therapists");

        myRef.child(therapist_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Therapist therapist= dataSnapshot.getValue(Therapist.class);
                firebaseCallback.onCallback(therapist);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    public interface FirebaseCallBack {
        void onCallback(Therapist therapist);
    }

    public int getQuestionsCount(DataSnapshot dataSnapshot){
        int count = 0;
        for(DataSnapshot ds: dataSnapshot
                .child("questions")
                .getChildren()){
            count++;
        }
        return count;
    }

    public int getAnswersCount(DataSnapshot dataSnapshot,String user_id){

        int count=0;


        //myRef = mFirebaseDatabase.getReference("question");


        for(DataSnapshot ds: dataSnapshot.child("questions").getChildren()){
            UserQuestions user= ds.getValue(UserQuestions.class);
            assert user != null;
            Log.d(TAG, "getAnswersCount: "+ user.getUserID());
            Log.d(TAG, "getAnswersCount: "+ user_id);
            if(user.getUserID().equals(user_id)){

                String ptsd= user.getQuestion6().getPost_dramatic_stress_disorder();

            }

        }
        return count;
    }

    public Boolean findSpeciality(String userSpeciality,ArrayList<String> specialities){
        boolean exists=false;
        Log.d(TAG, "findSpeciality: "+ userSpeciality);
        for(int i=0;i<specialities.size();i++){
            Log.d(TAG, "findSpeciality: "+ specialities.get(i));
            if(userSpeciality.equals(specialities.get(i))){
                exists=true;
                break;
            }
        }
        return exists;
    }

    public void acceptNotification(String therapistID,String userID){
        myRef = mFirebaseDatabase.getReference();

        myRef.child("users")
                .child(userID)
                .child("assignedTherapist")
                .removeValue();

        myRef.child("users")
                .child(userID)
                .child("assignedTherapist")
                .child(therapistID)
                .setValue("current");

        myRef.child("therapists")
                .child(therapistID)
                .child("assignedUsers")
                .child(userID)
                .setValue(userID);
    }

    public ArrayList<String> getAssignedUsersIDs(DataSnapshot dataSnapshot1){
        ArrayList<String> userIDs= new ArrayList<>();
        DataSnapshot dataSnapshot= dataSnapshot1.child("therapists").child(mAuth.getCurrentUser().getUid());
        for (DataSnapshot assignedToUser : dataSnapshot.child("assignedUsers").getChildren()) {
            userIDs.add(assignedToUser.getValue().toString());
        }
        return userIDs;
    }

    public ArrayList<User> getAssignedUsers(DataSnapshot dataSnapshot,ArrayList<String> userIDs){
        ArrayList<User> mUsers= new ArrayList<>();
        for(DataSnapshot dataSnapshot1 : dataSnapshot.child("users").getChildren()){
            User user= new User();
            for(int i=0;i<userIDs.size();i++){
                if(dataSnapshot1.getKey().equals(userIDs.get(i))){
                    user.setUser_id(dataSnapshot1.getKey());
                    user.setImageURL(dataSnapshot1.child("imageURL").getValue().toString());
                    user.setPassword(dataSnapshot1.child("password").getValue().toString());
                    user.setEmail(dataSnapshot1.child("email").getValue().toString());
                    user.setName(dataSnapshot1.child("name").getValue().toString());
                    Log.d(TAG, "getAllUsers: "+ dataSnapshot1.child("name").getValue().toString());
                    user.setPhone_number(dataSnapshot1.child("phone_number").getValue().toString());
                    for(DataSnapshot dataSnapshot2 : dataSnapshot1.child("assignedTherapist").getChildren()){
                        Log.d(TAG, "getAllUsers: "+ dataSnapshot2.getKey());
                        user.setAssignedTherapist(dataSnapshot2.getKey().toString());
                        user.setStatus(dataSnapshot2.getValue().toString());
                    }
                    mUsers.add(user);
                    break;
                }
            }
        }
        return mUsers;
    }

    public ArrayList<User> getAllUsers(DataSnapshot dataSnapshot2){

        ArrayList<User> users= new ArrayList<>();
        for(DataSnapshot dataSnapshot: dataSnapshot2.child("users").getChildren()) {
            User user= new User();
            user.setUser_id(dataSnapshot.getKey());
            user.setImageURL(dataSnapshot.child("imageURL").getValue().toString());
            user.setPassword(dataSnapshot.child("password").getValue().toString());
            user.setEmail(dataSnapshot.child("email").getValue().toString());
            user.setName(dataSnapshot.child("name").getValue().toString());
            Log.d(TAG, "getAllUsers: "+ dataSnapshot.child("name").getValue().toString());
            user.setPhone_number(dataSnapshot.child("phone_number").getValue().toString());
            for(DataSnapshot dataSnapshot1 : dataSnapshot.child("assignedTherapist").getChildren()){
                Log.d(TAG, "getAllUsers: "+ dataSnapshot1.getKey());
                user.setAssignedTherapist(dataSnapshot1.getKey().toString());
                user.setStatus(dataSnapshot1.getValue().toString());
            }
            users.add(user);
        }
        return users;
    }

    public void addAllTherapists(DataSnapshot dataSnapshot1){

        for(DataSnapshot dataSnapshot: dataSnapshot1.child("therapists").getChildren()) {
            Therapist therapist = new Therapist();
            Timings timings= new Timings();
            therapist.setTherapist_id(dataSnapshot.getKey());
            therapist.setDegree(dataSnapshot.child("degree").getValue().toString());
            therapist.setJoinedTime(dataSnapshot.child("joinedTime").getValue().toString());
            therapist.setLicenseDes(dataSnapshot.child("licenseDes").getValue().toString());
            therapist.setLicense(dataSnapshot.child("license").getValue().toString());
            therapist.setTimeZone(dataSnapshot.child("timeZone").getValue().toString());
            therapist.setTimePracticeMonths(dataSnapshot.child("timePracticeMonths").getValue().toString());
            therapist.setUsersHelped(dataSnapshot.child("usersHelped").getValue().toString());
            therapist.setTreatmentApproach(dataSnapshot.child("treatmentApproach").getValue().toString());
            therapist.setName(dataSnapshot.child("name").getValue().toString());
            therapist.setDescription(dataSnapshot.child("description").getValue().toString());
            therapist.setImageURL(dataSnapshot.child("imageURL").getValue().toString());
            timings.setMonday(dataSnapshot.child("Timings").child("monday").getValue().toString());
            timings.setTuesday(dataSnapshot.child("Timings").child("tuesday").getValue().toString());
            timings.setWednesday(dataSnapshot.child("Timings").child("wednesday").getValue().toString());
            timings.setThursday(dataSnapshot.child("Timings").child("thursday").getValue().toString());
            timings.setFriday(dataSnapshot.child("Timings").child("friday").getValue().toString());
            timings.setSaturday(dataSnapshot.child("Timings").child("saturday").getValue().toString());
            timings.setSunday(dataSnapshot.child("Timings").child("sunday").getValue().toString());
            therapist.setTimings(timings);
            therapist.setSpecialities(dataSnapshot.child("specialities").getValue().toString());
            for (DataSnapshot assignedToUser : dataSnapshot.child("assignedUsers").getChildren()) {
                therapist.setAssignedUsers(assignedToUser.getValue().toString());
            }
            MyDatabase ab = MyDatabase.getInstance(mContext);
            Log.d(TAG, "onDataChange: " + therapist);
            ab.setTherapistProfile(therapist);
        }
    }
    public void letKnowTherapists(String userID,String phoneNumber){
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermission()) {
                Log.e("permission", "Permission already granted.");
            } else {
                requestPermission();
            }
        }

        if(checkPermission()) {

            Log.d(TAG, "letKnowTherapists: Message is being sent");

//Get the default SmsManager//

            SmsManager smsManager = SmsManager.getDefault();
            String message="These specialities ";

            for(int i=0;i<specialitiesMatched.size();i++){
                message= specialitiesMatched.get(i)+",";
            }

            message= message+ " have been matched with the following user id: "+ userID + ". Please respond asap.";

//Send the SMS//

            smsManager.sendTextMessage(phoneNumber, null, message, null, null);

            Log.d(TAG, "letKnowTherapists: Message sent");
        }else {
            Toast.makeText(mContext, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(mContext, Manifest.permission.SEND_SMS);
	    return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.SEND_SMS}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(mContext,
                            "Permission accepted", Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(mContext,
                            "Permission denied", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }

    public void addQuestion(String question,String answer, String userID){
        myRef = mFirebaseDatabase.getReference();

        Log.d(TAG, "addQuestion: I am inside");


        myRef.child("users")
                .child(userID)
                .child("questions")
                .child(question)
                .setValue(answer);
    }

    public void addMainQuestion(String question,ArrayList<String> answers, String userID){
        myRef = mFirebaseDatabase.getReference();

        Log.d(TAG, "addQuestion: I am inside");

        for(int i=0;i<answers.size();i++){
            myRef.child("users")
                    .child(userID)
                    .child("questions")
                    .child(question)
                    .child(""+i)
                    .setValue(answers.get(i));
        }
    }

    public void addSpecialities(String key,String value,String therapist_id){

        myRef = mFirebaseDatabase.getReference("therapists");

        Log.d(TAG, "addQuestion: I am inside");

        myRef.child(therapist_id)
                .child("speciality")
                .child(key)
                .setValue(value);

    }

    public void addQuestionsToUser(){}


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
