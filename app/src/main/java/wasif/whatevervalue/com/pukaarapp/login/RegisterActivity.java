package wasif.whatevervalue.com.pukaarapp.login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.utils.FirebaseMethods;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "RegisterActivity";
    private static final Boolean CHECK_IF_VERIFIED = false;

    //firebase
    //private FirebaseAuth mAuth;
    //private FirebaseAuth.AuthStateListener mAuthListener;

    private Context mContext;
    private ProgressBar mProgressBar;
    private TextView mPleaseWait;
    private TextView mSignin;
    private FirebaseMethods firebaseMethods;

    //firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;

    CircleImageView mProfilePicture;
    EditText mNickname,mPhoneNumber,mEmail,mConfirmEmail,mPassword;
    String nickname,phoneNumber,email,confirmEmail,password,profilePicture;
    Button mRegisterButton;


    String question_1,answer_1;
    String question_2,answer_2;
    String question_3,answer_3;
    String question_4,answer_4;
    String question_5,answer_5,question_6;
    ArrayList<String> answer_6;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registerpage);
        mContext = RegisterActivity.this;
        firebaseMethods = new FirebaseMethods(mContext);
        Log.d(TAG, "onCreate: started.");


        question_1= getIntent().getStringExtra("question1");
        answer_1= getIntent().getStringExtra("answer1");
        question_2= getIntent().getStringExtra("question2");
        answer_2= getIntent().getStringExtra("answer2");
        question_3= getIntent().getStringExtra("question3");
        answer_3= getIntent().getStringExtra("answer3");
        question_4= getIntent().getStringExtra("question4");
        answer_4= getIntent().getStringExtra("answer4");
        question_5= getIntent().getStringExtra("question5");
        answer_5= getIntent().getStringExtra("answer5");
        question_6= getIntent().getStringExtra("question6");
        answer_6= getIntent().getStringArrayListExtra("answer6");

        initWidgets();
        setupFirebaseAuth();
        init();



    }

    private void initWidgets(){
        Log.d(TAG, "initWidgets: Initializing Widgets.");
        mProfilePicture=findViewById(R.id.profilePicture);
        mNickname=findViewById(R.id.nickname);
        mPhoneNumber=findViewById(R.id.phoneNumber);
        mEmail=findViewById(R.id.email);
        mConfirmEmail=findViewById(R.id.confirmEmail);
        mPassword=findViewById(R.id.password);
        mRegisterButton=findViewById(R.id.registerButton);
    }


    private void init(){
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email = mEmail.getText().toString();
                nickname = mNickname.getText().toString();
                password = mPassword.getText().toString();
                phoneNumber=mPhoneNumber.getText().toString();
                confirmEmail=mConfirmEmail.getText().toString();
                profilePicture=mProfilePicture.toString();

                if(checkInputs(email, nickname, password)){

                    if(checkEmail(email,confirmEmail)){
                        FirebaseMethods firebaseMethods= new FirebaseMethods(mContext);
                        firebaseMethods.registerNewEmail(email,nickname,password,profilePicture,phoneNumber,"none");
                        goToVerifyEmailActivity();
                    }
                }
            }
        });
    }

    private boolean checkInputs(String email, String nickname, String password){
        Log.d(TAG, "checkInputs: checking inputs for null values.");
        if(email.equals("") || nickname.equals("") || password.equals("")){
            Toast.makeText(mContext, "All fields must be filled out.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean checkEmail(String email, String confirmEmail){
        if(email.equals(confirmEmail)){
            return true;
        }
        Toast.makeText(mContext, "Emails are not matched.", Toast.LENGTH_SHORT).show();
        return false;
    }

    public void goToLoginPage(){
        Intent intent= new Intent(this,LoginActivity.class);
        startActivity(intent);
    }

    public void goToVerifyEmailActivity(){
        Intent intent= new Intent(this,VerifyEmailActivity.class);
        intent.putExtra("question1",question_1);
        intent.putExtra("answer1",answer_1);
        intent.putExtra("question2",question_2);
        intent.putExtra("answer2",answer_2);
        intent.putExtra("question3",question_3);
        intent.putExtra("answer3",answer_3);
        intent.putExtra("question4",question_4);
        intent.putExtra("answer4",answer_4);
        intent.putExtra("question5",question_5);
        intent.putExtra("answer5",answer_5);
        intent.putExtra("question6",question_6);
        intent.putExtra("answer6",answer_6);
        intent.putExtra("email",email);
        intent.putExtra("nickname",nickname);
        intent.putExtra("password",password);
        intent.putExtra("profilePicture",profilePicture);
        intent.putExtra("phoneNumber",phoneNumber);
        startActivity(intent);
    }
    public void goToIntroPage(){
        Intent intent= new Intent(this,IntroActivity.class);
        startActivity(intent);
    }

     /*
    ------------------------------------ Firebase ---------------------------------------------
     */


    /**
     * Setup the firebase auth object
     */
    private void setupFirebaseAuth(){
        Log.d(TAG, "setupFirebaseAuth: setting up firebase auth.");

        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase=FirebaseDatabase.getInstance();
        myRef=mFirebaseDatabase.getReference();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();

                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    FirebaseDatabase database = FirebaseDatabase.getInstance();

                    myRef=database.getReference();


                    myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //checkIfUsernameExists(username);

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                    finish();

                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
    }
}
