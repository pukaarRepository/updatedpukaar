package wasif.whatevervalue.com.pukaarapp.login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.admin.LoginScreen;
import wasif.whatevervalue.com.pukaarapp.utils.FirebaseMethods;

public class QuestionActivity6 extends AppCompatActivity implements View.OnClickListener {

    //firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private FirebaseMethods mFirebaseMethods;

    private int questionCount = 0;

    private static final String TAG = "questionActivity6";

    Button answer1,answer2,answer3,answer4,answer5,answer6;
    Button answer7,answer8,answer9,answer10,answer11,answer12;
    Button answer13,answer14,answer15,answer16,answer17,answer18;
    Boolean check1,check2,check3,check4,check5,check6;
    Boolean check7,check8,check9,check10,check11,check12;
    Boolean check13,check14,check15,check16,check17,check18;
    FirebaseMethods firebaseMethods;
    TextView question6;
    Button button1;
    ArrayList<String> answers;
    Drawable img;


    String question_1,answer_1;
    String question_2,answer_2;
    String question_3,answer_3;
    String question_4,answer_4;
    String question_5,answer_5;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question6);
        answer1=findViewById(R.id.answer1);
        answer2=findViewById(R.id.answer2);
        answer3=findViewById(R.id.answer3);
        answer4=findViewById(R.id.answer4);
        answer5=findViewById(R.id.answer5);
        answer6=findViewById(R.id.answer6);
        answer7=findViewById(R.id.answer7);
        answer8=findViewById(R.id.answer8);
        answer9=findViewById(R.id.answer9);
        answer10=findViewById(R.id.answer10);
        answer11=findViewById(R.id.answer11);
        answer12=findViewById(R.id.answer12);
        answer13=findViewById(R.id.answer13);
        answer14=findViewById(R.id.answer14);
        answer15=findViewById(R.id.answer15);
        answer16=findViewById(R.id.answer16);
        answer17=findViewById(R.id.answer17);
        answer18=findViewById(R.id.answer18);
        question6=findViewById(R.id.question6);
        button1=findViewById(R.id.button1);
        mFirebaseMethods= new FirebaseMethods(QuestionActivity6.this);

        check1=false;
        check2=false;
        check3=false;
        check4=false;
        check5=false;
        check6=false;
        check7=false;
        check8=false;
        check9=false;
        check10=false;
        check11=false;
        check12=false;
        check13=false;
        check14=false;
        check15=false;
        check16=false;
        check17=false;
        check18=false;

        setupFirebaseAuth();

        answers= new ArrayList<>();

        answer1.setOnClickListener(this);
        answer2.setOnClickListener(this);
        answer3.setOnClickListener(this);
        answer4.setOnClickListener(this);
        answer5.setOnClickListener(this);
        answer6.setOnClickListener(this);
        answer7.setOnClickListener(this);
        answer8.setOnClickListener(this);
        answer9.setOnClickListener(this);
        answer10.setOnClickListener(this);
        answer11.setOnClickListener(this);
        answer12.setOnClickListener(this);
        answer13.setOnClickListener(this);
        answer14.setOnClickListener(this);
        answer15.setOnClickListener(this);
        answer16.setOnClickListener(this);
        answer17.setOnClickListener(this);
        answer18.setOnClickListener(this);

        button1.setOnClickListener(this);


        question_1= getIntent().getStringExtra("question1");
        answer_1= getIntent().getStringExtra("answer1");
        question_2= getIntent().getStringExtra("question2");
        answer_2= getIntent().getStringExtra("answer2");
        question_3= getIntent().getStringExtra("question3");
        answer_3= getIntent().getStringExtra("answer3");
        question_4= getIntent().getStringExtra("question4");
        answer_4= getIntent().getStringExtra("answer4");
        question_5= getIntent().getStringExtra("question5");
        answer_5= getIntent().getStringExtra("answer5");






    }

    /**
     * Setup the firebase auth object
     */
    private void setupFirebaseAuth(){
        Log.d(TAG, "setupFirebaseAuth: setting up firebase auth.");
        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        Log.d(TAG, "onDataChange: image count: " + questionCount);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();


                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };



        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                questionCount = mFirebaseMethods.getQuestionsCount(dataSnapshot);
                Log.d(TAG, "onDataChange: question count: " + questionCount);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void goToLoginPage(ArrayList<String> answers){
        Intent intent= new Intent(this,LoginScreen.class);
        intent.putExtra("question1",question_1);
        intent.putExtra("answer1",answer_1);
        intent.putExtra("question2",question_2);
        intent.putExtra("answer2",answer_2);
        intent.putExtra("question3",question_3);
        intent.putExtra("answer3",answer_3);
        intent.putExtra("question4",question_4);
        intent.putExtra("answer4",answer_4);
        intent.putExtra("question5",question_5);
        intent.putExtra("answer5",answer_5);
        intent.putExtra("question6",question6.getText().toString());
        intent.putExtra("answer6",answers);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.answer1:
                if(!check1){
                    img=getDrawable(R.drawable.ic_donee);
                    answer1.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer1.getText().toString());
                    check1=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer1.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer1.getText().toString());
                    check1=false;
                }
                break;

            case R.id.answer2:
                if(!check2){
                    img=getDrawable(R.drawable.ic_donee);
                    answer2.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer2.getText().toString());
                    check2=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer2.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer2.getText().toString());
                    check2=false;
                }
                break;

            case R.id.answer3:
                if(!check3){
                    img=getDrawable(R.drawable.ic_donee);
                    answer3.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer3.getText().toString());
                    check3=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer3.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer3.getText().toString());
                    check3=false;
                }
                break;

            case R.id.answer4:
                if(!check4){
                    img=getDrawable(R.drawable.ic_donee);
                    answer4.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer4.getText().toString());
                    check4=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer4.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer4.getText().toString());
                    check4=false;
                }
                break;

            case R.id.answer5:
                if(!check5){
                    img=getDrawable(R.drawable.ic_donee);
                    answer5.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer5.getText().toString());
                    check5=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer5.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer5.getText().toString());
                    check5=false;
                }
                break;

            case R.id.answer6:
                if(!check6){
                    img=getDrawable(R.drawable.ic_donee);
                    answer6.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer6.getText().toString());
                    check6=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer6.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer6.getText().toString());
                    check6=false;
                }
                break;

            case R.id.answer7:
                if(!check7){
                    img=getDrawable(R.drawable.ic_donee);
                    answer7.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer7.getText().toString());
                    check7=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer7.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer7.getText().toString());
                    check7=false;
                }
                break;

            case R.id.answer8:
                if(!check8){
                    img=getDrawable(R.drawable.ic_donee);
                    answer8.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer8.getText().toString());
                    check8=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer8.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer8.getText().toString());
                    check8=false;
                }
                break;

            case R.id.answer9:
                if(!check9){
                    img=getDrawable(R.drawable.ic_donee);
                    answer9.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer9.getText().toString());
                    check9=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer9.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer9.getText().toString());
                    check9=false;
                }
                break;

            case R.id.answer10:
                if(!check10){
                    img=getDrawable(R.drawable.ic_donee);
                    answer10.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer10.getText().toString());
                    check10=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer10.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer10.getText().toString());
                    check10=false;
                }
                break;


            case R.id.answer11:
                if(!check11){
                    img=getDrawable(R.drawable.ic_donee);
                    answer11.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer11.getText().toString());
                    check11=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer11.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer11.getText().toString());
                    check11=false;
                }
                break;


            case R.id.answer12:
                if(!check12){
                    img=getDrawable(R.drawable.ic_donee);
                    answer12.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer12.getText().toString());
                    check12=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer12.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer12.getText().toString());
                    check12=false;
                }
                break;


            case R.id.answer13:
                if(!check13){
                    img=getDrawable(R.drawable.ic_donee);
                    answer13.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer13.getText().toString());
                    check13=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer13.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer13.getText().toString());
                    check13=false;
                }
                break;


            case R.id.answer14:
                if(!check14){
                    img=getDrawable(R.drawable.ic_donee);
                    answer14.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer14.getText().toString());
                    check14=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer14.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer14.getText().toString());
                    check14=false;
                }
                break;


            case R.id.answer15:
                if(!check15){
                    img=getDrawable(R.drawable.ic_donee);
                    answer15.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer15.getText().toString());
                    check15=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer15.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer15.getText().toString());
                    check15=false;
                }
                break;


            case R.id.answer16:
                if(!check16){
                    img=getDrawable(R.drawable.ic_donee);
                    answer16.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer16.getText().toString());
                    check16=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer16.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer16.getText().toString());
                    check16=false;
                }
                break;


            case R.id.answer17:
                if(!check17){
                    img=getDrawable(R.drawable.ic_donee);
                    answer17.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer17.getText().toString());
                    check17=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer17.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer17.getText().toString());
                    check17=false;
                }
                break;


            case R.id.answer18:
                if(!check18){
                    img=getDrawable(R.drawable.ic_donee);
                    answer18.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,img,null);
                    answers.add(answer18.getText().toString());
                    check18=true;
                }
                else{
                    img=getDrawable(R.drawable.ic_donee);
                    answer18.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null);
                    answers.remove(answer18.getText().toString());
                    check18=false;
                }
                break;

            case R.id.button1:
                goToLoginPage(answers);
                break;

        }
    }
}
