package wasif.whatevervalue.com.pukaarapp.therapistEnd;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;
import java.util.Stack;

import de.hdodenhof.circleimageview.CircleImageView;
import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.database.DatabaseSchema;
import wasif.whatevervalue.com.pukaarapp.database.MyDatabase;
import wasif.whatevervalue.com.pukaarapp.utils.FirebaseMethods;
import wasif.whatevervalue.com.pukaarapp.utils.SectionsPagerAdapter;

@SuppressWarnings("unused")
public class TherapistPanelActivity extends AppCompatActivity  {


	Bitmap bitmap;
	boolean editTextOpen = false;
	View fullScreenLayout;
    private static final String TAG = "TherapistPanelActivity";
    private Context mContext= TherapistPanelActivity.this;
    private static final int ACTIVITY_NUM = 0;
    DrawerLayout drawer;
    Fragment current;

    Stack<Fragment> mFragmentStack = new Stack<>();
    public int selected = 0;

    static ImageView zoomImage;
	FrameLayout frameLayout;


    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;


    private FirebaseAuth mAuth;
    boolean doubleBackToExitPressedOnce = false;
    TextView firstUserWait;


    //widgets
    private ViewPager mViewPager;
    private FrameLayout mFrameLayout;
    private View mView;
    private RelativeLayout mRelativeLayout;
    private ImageButton chat;
    private ImageButton settings;
    private CircleImageView therapist;
    FirebaseDatabase firebaseDatabase;
    String profilePicture="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        zoomImage= new ImageView(this);
        setContentView(R.layout.layout_center_viewpager);

        mViewPager = findViewById(R.id.viewpager_container);

        mAuth = FirebaseAuth.getInstance();


        MyDatabase db = MyDatabase.getInstance(mContext);

        setupViewPager();



        Log.d(TAG, "onCreate Therapist: " +
                FirebaseMethods.getTableAsString(db.getReadableDatabase(), DatabaseSchema.TherapistInfoTable.NAMEOFTABLE));

        Log.d(TAG, "onCreate Users: " +
                FirebaseMethods.getTableAsString(db.getReadableDatabase(), DatabaseSchema.UsersTable.NAME));
//		tabs = findViewById(R.id.tabs);
//	    final ImageButton settings = tabs.findViewById(R.id.settings_tab);
//	    final ImageButton chat = tabs.findViewById(R.id.chat_tab);
//	    final ImageButton therapist = tabs.findViewById(R.id.therapist_tab);




//        getSupportFragmentManager().beginTransaction()
//                .add(R.id.container, new MainFragment(), MainFragment.TAG)
//                .commit();
    }

    public int getCurrentTabNumber(){
        return mViewPager.getCurrentItem();
    }


    private void setupViewPager(){
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(Objects.requireNonNull(TherapistPanelActivity.this).getSupportFragmentManager()){
        };

        adapter.addFragment(new TherapistSettingsFragment()); //index 0
        adapter.addFragment(new TherapistPanelFragment()); //index 1
        mViewPager.setAdapter(adapter);

	}

}
