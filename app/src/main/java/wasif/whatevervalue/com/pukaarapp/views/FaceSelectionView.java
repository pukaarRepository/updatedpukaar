package wasif.whatevervalue.com.pukaarapp.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import wasif.whatevervalue.com.pukaarapp.R;

/**
 * Created by Taha Malik on 25/01/2019.
 **/
public class FaceSelectionView extends View {
	Paint mPaint = new Paint();
	final RectF oval = new RectF();
	Path myPath = new Path();
	Rect mRect = new Rect();

	public final static String[] moods = {"Depressed", "Sad", "Upset", "Ok"
			, "Crying", "Happy", "Excited"};

	public final static int[] mColors = {R.color.depressed, R.color.sad
			, R.color.upset, R.color.ok, R.color.fine
			, R.color.happy, R.color.excited};

	double[] x;
	double[] y;

	int selectedI;

	Bitmap[] mBitmap;

	private double centerX;
	private double centerY;
	private int r;

	public FaceSelectionView(Context context) {
		super(context);
	}

	public FaceSelectionView(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
	}

	public FaceSelectionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public FaceSelectionView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	private void initialize()
	{

		mPaint.setAlpha(255/2);
		x = new double[7];
		y = new double[7];
		int strokeWidth = 15;
		mPaint.setStrokeWidth(strokeWidth);
		mPaint.setStyle(Paint.Style.STROKE);

		r = (getRight() - getLeft())/2;
		r -= strokeWidth*2;

		centerX = r+2*strokeWidth;
		centerY = r+2*strokeWidth;

		double lx = r / 1.4142;
		double ly = r / 1.4142;

//		double x1 = getXOrY((double) (centerY + l), r, centerX, centerY, 0);
		x[0] = centerX - lx;
		x[1] = centerX - r;
//		double x3 = getXOrY((double) (centerY - l), r, centerX, centerY, 0);
		x[2] = centerX - lx;
		x[3] = centerX;
//		double x5 = getXOrY((double) (centerY - l), r, centerX, centerY, 1);
		x[4] = centerX + lx;
		x[5] = centerX + r;
		x[6] = centerX + lx;

		y[0] = centerY + ly;
		y[1] = centerY;
		y[2] = centerY - ly;
		y[3] = centerY - r;
		y[4] = centerY - ly;
		y[5] = centerY;
		y[6] = centerY + ly;

		selectedI = 4;

		initializeBitmap();

	}


	public static int getColorID(int mood)
	{
		if(mood == 0)
		{
			return R.color.depressed;
		} else if(mood == 1)
		{
			return R.color.sad;
		} else if(mood == 2)
		{
			return R.color.upset;
		} else if(mood == 3)
		{
			return R.color.ok;
		} else if(mood == 4)
		{
			return R.color.fine;
		} else if(mood == 5)
		{
			return R.color.happy;
		} else if(mood == 6)
		{
			return R.color.excited;
		}
		else throw new IllegalArgumentException();

	}

	public static int getImageID(int mood, Context pContext)
	{

		if(mood == 0)
		{
			return R.drawable.face_depressed_mdpi;
		} else if(mood == 1)
		{
			return R.drawable.face_sad_mdpi;
		} else if(mood == 2)
		{
			return R.drawable.face_upset_mdpi;
		} else if(mood == 3)
		{
			return R.drawable.face_ok_mdpi;
		} else if(mood == 4)
		{
			return R.drawable.face_fine_mdpi;
		} else if(mood == 5)
		{
			return R.drawable.face_happy_mdpi;
		} else if(mood == 6)
		{
			return R.drawable.face_excited_mdpi;
		}
		else throw new IllegalArgumentException();
	}

	public static int getfullFaceImageID(int mood)
	{

		if(mood == 0)
		{
			return R.drawable.depressed;
		} else if(mood == 1)
		{
			return R.drawable.sad;
		} else if(mood == 2)
		{
			return R.drawable.upset;
		} else if(mood == 3)
		{
			return R.drawable.fine;
		} else if(mood == 4)
		{
			return R.drawable.crying;
		} else if(mood == 5)
		{
			return R.drawable.happy;
		} else if(mood == 6)
		{
			return R.drawable.excited;
		}
		else throw new IllegalArgumentException();
	}


	public int getSelection()
	{
		return selectedI;
	}

	private void initializeBitmap() {

		mBitmap = new Bitmap[7];
		BitmapDrawable lBitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.face_depressed_mdpi);
		mBitmap[0] = lBitmapDrawable.getBitmap();
		lBitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.face_sad_mdpi);
		mBitmap[1] = lBitmapDrawable.getBitmap();
		lBitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.face_upset_mdpi);
		mBitmap[2] = lBitmapDrawable.getBitmap();
		lBitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.face_ok_mdpi);
		mBitmap[3] = lBitmapDrawable.getBitmap();
		lBitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.face_fine_mdpi);
		mBitmap[4] = lBitmapDrawable.getBitmap();
		lBitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.face_happy_mdpi);
		mBitmap[5] = lBitmapDrawable.getBitmap();
		lBitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.face_excited_mdpi);
		mBitmap[6] = lBitmapDrawable.getBitmap();
		mRect.set((int)centerX - r/2, (int) centerY - r/2, (int)centerX+r/2, (int)centerY+r/2);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		initialize();
	}

	@Override
	protected void onDraw(Canvas canvas) {


		mPaint.setColor(Color.LTGRAY);
		mPaint.setAlpha(255);

		for(int i = 0; i < 7; i++)
		{
			canvas.drawCircle((float) x[i], (float) y[i], 8, mPaint);
		}

		mPaint.setColor(Color.LTGRAY);
		oval.set((float)x[1], (float)y[3], (float) x[5], (float) (centerY + r));
//		int startAngle = (int) (180 / Math.PI * Math.atan2(y1 - y7, x1 - x7));
		int startAngle = 135;
		myPath.arcTo(oval, startAngle, (float) 270, true);
		canvas.drawPath(myPath, mPaint);
		mPaint.setColor(Color.WHITE);
		canvas.drawCircle((float) x[selectedI], (float) y[selectedI], 20, mPaint);
		canvas.drawBitmap(mBitmap[selectedI], null, mRect, null);
	}

	public float get_x(int pI) {
		return (float) x[pI];
	}

	public float get_y(int pI) {
		return (float) y[pI];
	}

	public void setSelectedI(int pI) {
		selectedI = pI;
	}

	public int getSelectedI() {
		return selectedI;
	}

	public static int getMoodIndex(String mood)
	{
		for(int i = 0; i < 7; i++)
		{
			if(moods[i].toLowerCase().equals(mood.toLowerCase()))
			{
				return i;
			}
		}
		return -1;
	}
}
