package wasif.whatevervalue.com.pukaarapp.session;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.dashboard.DashboardActivity;
import wasif.whatevervalue.com.pukaarapp.database.MyDatabase;
import wasif.whatevervalue.com.pukaarapp.intefaces.GetTAG;
import wasif.whatevervalue.com.pukaarapp.models.Therapist;
import wasif.whatevervalue.com.pukaarapp.models.User;

public class TherapistFragment extends Fragment implements GetTAG {

    private static final String TAG = "TherapistFragment";
    private FirebaseAuth mAuth;

    CircleImageView imageURL;
    TextView therapistName,degree,treatmentApproach,license,licenseDes,specialities,timePracticeMonths,usersHelped,joinedTime;
    TextView therapistDescription,timeZone;
    TextView monday,tuesday,wednesday,thursday,friday,saturday,sunday;





    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_therapist, container, false);

//	    ImageButton lImageButton = ((DashboardActivity)getActivity()).tabs.findViewById(R.id.settings_tab);
//	    lImageButton.setImageResource(R.drawable.menu);
//
//
//	    lImageButton = ((DashboardActivity)getActivity()).tabs.findViewById(R.id.chat_tab);
//	    lImageButton.setImageResource(R.drawable.message_icon);
//
//	    lImageButton = ((DashboardActivity)getActivity()).tabs
//                .findViewById(R.id.therapist_tab);
//	    lImageButton.setImageResource(R.drawable.user_selected);

        imageURL=view.findViewById(R.id.imageURL);
        timeZone=view.findViewById(R.id.timeZone);
        usersHelped=view.findViewById(R.id.usersHelped);
        joinedTime=view.findViewById(R.id.joinedTime);
        specialities=view.findViewById(R.id.specialities);
        timePracticeMonths=view.findViewById(R.id.timePracticeMonths);
        degree=view.findViewById(R.id.degree);
        treatmentApproach=view.findViewById(R.id.treatmentApproach);
        therapistName=view.findViewById(R.id.therapistName);
        therapistDescription = view.findViewById(R.id.therapist_description);
        license=view.findViewById(R.id.license);
        licenseDes=view.findViewById(R.id.licenseDes);
        monday=view.findViewById(R.id.monday);
        tuesday=view.findViewById(R.id.tuesday);
        wednesday=view.findViewById(R.id.wednesday);
        thursday=view.findViewById(R.id.thursday);
        friday=view.findViewById(R.id.friday);
        saturday=view.findViewById(R.id.saturday);
        sunday=view.findViewById(R.id.sunday);

        therapistDescription.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

            	view.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction()&MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
        therapistDescription.setMovementMethod(new ScrollingMovementMethod());
        Log.d(TAG, "onCreateView: started.");
        checkUser();

        return view;
    }



    public void checkUser(){
        mAuth = FirebaseAuth.getInstance();
        String currentUserID=mAuth.getCurrentUser().getUid();

        MyDatabase db = MyDatabase.getInstance(getContext());

        User user=db.getUser(currentUserID);

        int index=0;
        for(int i=0;i<user.getStatus().size();i++){
            if(user.getStatus().get(i).equals("current")){
                index=i;
            }
        }
        Log.d(TAG, "checkUser: "+ user.getAssignedTherapist().get(index));
        String therapistID= user.getAssignedTherapist().get(index);

        Therapist therapist=db.getTherapistProfile(therapistID);

       // Log.d(TAG, "checkUser: "+ therapist.getTimings().getMonday());

        therapistName.setText(therapist.getName());
        timeZone.setText(therapist.getTimeZone());
        usersHelped.setText(therapist.getUsersHelped());
        licenseDes.setText(therapist.getLicenseDes());
        license.setText(therapist.getLicense());
        degree.setText(therapist.getDegree());
        timePracticeMonths.setText(therapist.getTimePracticeMonths());
        therapistDescription.setText(therapist.getDescription());
        specialities.setText(therapist.getSpecialities());
        treatmentApproach.setText(therapist.getTreatmentApproach());
        joinedTime.setText(therapist.getJoinedTime());
        license.setText(therapist.getLicense());
        monday.setText(therapist.getTimings().getMonday());
        tuesday.setText(therapist.getTimings().getTuesday());
        wednesday.setText(therapist.getTimings().getWednesday());
        thursday.setText(therapist.getTimings().getThursday());
        friday.setText(therapist.getTimings().getFriday());
        saturday.setText(therapist.getTimings().getSaturday());
        sunday.setText(therapist.getTimings().getSunday());
        Picasso.get().load(therapist.getImageURL()).placeholder(R.drawable.ariana).into(imageURL);
    }


    @Override
    public String getTAG() {
        return TAG;
    }
}
