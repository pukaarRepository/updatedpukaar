package wasif.whatevervalue.com.pukaarapp.models;

public class Question5 {

    private String question;
    private String answer;

    public Question5(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    Question5(){

    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
