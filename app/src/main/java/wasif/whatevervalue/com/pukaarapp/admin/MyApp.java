package wasif.whatevervalue.com.pukaarapp.admin;

import android.app.Application;

import wasif.whatevervalue.com.pukaarapp.utils.TypefaceUtil;

public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/wonderbar.otf");
    }
}
