package wasif.whatevervalue.com.pukaarapp.dashboard;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Objects;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.database.MyDatabase;
import wasif.whatevervalue.com.pukaarapp.intefaces.GetTAG;
import wasif.whatevervalue.com.pukaarapp.models.CompleteMood;
import wasif.whatevervalue.com.pukaarapp.models.Mood;
import wasif.whatevervalue.com.pukaarapp.session.AddNoteFragment;
import wasif.whatevervalue.com.pukaarapp.therapistEnd.SelectMoodList;
import wasif.whatevervalue.com.pukaarapp.utils.DateConverter;
import wasif.whatevervalue.com.pukaarapp.views.FaceSelectionView;

public class MoodDiaryFragment extends Fragment implements GetTAG {

	public static final String[] weekDays = new String[]{"Mon", "Tue", "Wed", "Thr", "Fri", "Sat", "Sun"};

	private View view;
	private static boolean mTherapist = false;
	private static String mUID = "";
	public static MoodDiaryFragment getInstance(boolean therapist, String uid)
	{
		mTherapist = therapist;
		mUID = uid;
		return new MoodDiaryFragment();
	}

	public static final String TAG = "MoodDiaryFragment";
	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(@NonNull LayoutInflater pLayoutInflater, ViewGroup parent, Bundle saveInstanceState) {

		view = pLayoutInflater.inflate(R.layout.fragment_set_mood, null);
		Toolbar toolbar = view.findViewById(R.id.toolbar);
		((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);

//		Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar())
//				.setDisplayHomeAsUpEnabled(true);
//
//		Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar())
//				.setHomeButtonEnabled(true);
//
//

		View back = toolbar.findViewById(R.id.back);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				Objects.requireNonNull(getActivity()).onBackPressed();
			}
		});

		View crying = view.findViewById(R.id.crying);
		TextView cryingValue = view.findViewById(R.id.crying_value);
		crying.setTag(cryingValue);
		View depressed = view.findViewById(R.id.depressed);
		TextView depressedValue = view.findViewById(R.id.depressed_value);
		depressed.setTag(depressedValue);
		View ok = view.findViewById(R.id.ok);
		TextView okValue = view.findViewById(R.id.ok_value);
		ok.setTag(okValue);
		View sad = view.findViewById(R.id.sad);
		TextView sadValue = view.findViewById(R.id.sad_value);
		sad.setTag(sadValue);
		final View excited = view.findViewById(R.id.excited);
		TextView excitedValue = view.findViewById(R.id.excited_value);
		excited.setTag(excitedValue);
		View.OnClickListener lOnClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				String mood = ((TextView)pView.getTag()).getText().toString();
				if(getActivity() instanceof DashboardActivity)
				((DashboardActivity)Objects.requireNonNull(getActivity())).changeFragment(
						AddNoteFragment.newInstance(mood, mTherapist, mUID), AddNoteFragment.TAG);
				else if(getActivity() instanceof SelectMoodList)
					((SelectMoodList)Objects.requireNonNull(getActivity())).changeFragment(
							AddNoteFragment.newInstance(mood, mTherapist, mUID), AddNoteFragment.TAG);

			}
		};

		excited.setOnClickListener(lOnClickListener);
		sad.setOnClickListener(lOnClickListener);
		depressed.setOnClickListener(lOnClickListener);
		ok.setOnClickListener(lOnClickListener);
		crying.setOnClickListener(lOnClickListener);

		return view;
	}
//		StaticLabelsFormatter lStaticLabelsFormatter = new StaticLabelsFormatter(lDepressionGraph);
//		lStaticLabelsFormatter.setHorizontalLabels(new String[]{"Mon", "Tue", "Wed", "Thr", "Fri", "Sat", "Sun"});

//////		ArrayList<CompleteMood> moods = MyDatabase.getInstance(getContext()).getThisWeeksCompleteMood();
//////		initializeGraphs(view, moods);
//////		int mood = 3;
//////		if(moods != null){
//////			mood = moods.get(0).mood;
////		}
////		ImageView lImageView = view.findViewById(R.id.selected_mood);
////		MyDatabase db = MyDatabase.getInstance(getContext());
////		final CollapsingToolbarLayout ctl = view.findViewById(R.id.toolbar_layout);
////		AppBarLayout abl = view.findViewById(R.id.app_bar);
////
////		final int finalMood = mood;
////		abl.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
////			@Override
////			public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
////				if (Math.abs(verticalOffset)-appBarLayout.getTotalScrollRange() == 0)
////				{
////					// Collapsed
////					ctl.setTitle("Pukaar");
////				}
////				else
////				{
////					// Expanded
////					ctl.setTitle(FaceSelectionView.moods[finalMood]);
////				}
////			}
////		});
////		try {
////
////			lImageView.setImageDrawable(getResources().getDrawable(FaceSelectionView.getImageID(mood, getContext())));
////			ctl.setBackgroundResource(FaceSelectionView.getColorID(mood));
////			ctl.setContentScrimResource(FaceSelectionView.getColorID(mood));
////			ctl.setStatusBarScrimResource(FaceSelectionView.getColorID(mood));
////
////		}
////
////		catch (IllegalArgumentException ex)
////		{
////			ex.printStackTrace();
////		}
////		catch (ArrayIndexOutOfBoundsException ex)
////		{
////			ex.printStackTrace();
////		}
////
////		FloatingActionButton fab = view.findViewById(R.id.fab);
////		fab.setOnClickListener(new View.OnClickListener() {
////			@Override
////			public void onClick(View view) {
////				((DashboardActivity) Objects.requireNonNull(getActivity())).changeFragment(new AddMoodFragment(), AddMoodFragment.TAG);
////			}
////		});
////		return view;
////	}
////
////	private void initializeGraphs(View view, ArrayList<CompleteMood> lMoods) {
////
////		GraphView lGraphView = view.findViewById(R.id.anxiety_graph);
////
////		lGraphView.getViewport().setMinY(-10);
////		lGraphView.getViewport().setMaxY(10);
////		lGraphView.getViewport().setMinX(0);
////		lGraphView.getViewport().setMaxX(6);
////		lGraphView.getViewport().setXAxisBoundsManual(true);
////		lGraphView.getViewport().setYAxisBoundsManual(true);
////		lGraphView.getViewport().setScrollable(true);
////		lGraphView.getGridLabelRenderer().setNumHorizontalLabels(7);
////		lGraphView.getGridLabelRenderer().setNumVerticalLabels(21);
//////		lDepressionGraph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(lNumberFormat,lNumberFormat1));
////		lGraphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
////			@Override
////			public String formatLabel(double value, boolean isValueX) {
////				if (!isValueX) {
////					return String.valueOf(value);
////				} else {
////					if (value >= 0 && value <= 6) {
////						return weekDays[(int) (value)];
////					}
////				}
////				return null;
////			}
////		});
////
////
////		if (lMoods != null && lMoods.size() != 0) {
////
////			int k;
////			for(k = 0; k < lMoods.size()-1; k++)
////			{
////				if(CompleteMood.getNoOfDay(lMoods.get(k).day) >
////						CompleteMood.getNoOfDay(lMoods.get(k+1).day))
////				{
////					break;
////				}
////			}
////
////			DataPoint[] lDataPoint = new DataPoint[k+1];
////			DataPoint[] lDataPoint1 = new DataPoint[lMoods.size()-(k+1)];
////			int i = 0;
////			int j = 0;
////			int prev = 0;
////
////			for (CompleteMood lMood
////					: lMoods) {
////				int temp = CompleteMood.getNoOfDay(lMood.day);
////				if(temp < prev)
////				{
////					prev = 8;
////					lDataPoint1[j++] = new DataPoint(temp, lMood.anxiety);
////				}
////				else {
////					lDataPoint[i++] = new DataPoint(temp, lMood.anxiety);
////					prev = temp;
////				}
////			}
////
////			LineGraphSeries<DataPoint> series = new LineGraphSeries<>(lDataPoint);
//////			series.setColor(Color.GREEN);
////			series.setDrawDataPoints(true);
////			series.setDataPointsRadius(10);
////			series.setThickness(8);
////
////			LineGraphSeries<DataPoint> series1 = new LineGraphSeries<>(lDataPoint1);
////
////			series1.setDrawDataPoints(true);
////			series1.setDataPointsRadius(10);
////			series1.setThickness(8);
////
////			if(j != 0)
////			{
////				series.setTitle("Last Week");
////				series1.setTitle("This Week");
////				series.setColor(Color.GRAY);
////				series1.setColor(Color.BLUE);
////				lGraphView.addSeries(series1);
////				lGraphView.addSeries(series);
////			}
////			else
////			{
////				series.setTitle("This Week");
//////				series1.setTitle("This Week");
//////				series1.setColor(Color.GRAY);
////				series.setColor(Color.BLUE);
////				lGraphView.addSeries(series1);
////				lGraphView.addSeries(series);
////			}
////		}
////
////		lGraphView = view.findViewById(R.id.energy_level_graph);
////		lGraphView.getViewport().setMinY(-10);
////		lGraphView.getViewport().setMaxY(10);
////		lGraphView.getViewport().setMinX(0);
////		lGraphView.getViewport().setMaxX(6);
////		lGraphView.getViewport().setXAxisBoundsManual(true);
////		lGraphView.getViewport().setYAxisBoundsManual(true);
////		lGraphView.getViewport().setScrollable(true);
////		lGraphView.getGridLabelRenderer().setNumHorizontalLabels(7);
////		lGraphView.getGridLabelRenderer().setNumVerticalLabels(21);
//////		lDepressionGraph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(lNumberFormat,lNumberFormat1));
////		lGraphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
////			@Override
////			public String formatLabel(double value, boolean isValueX) {
////				if (!isValueX) {
////					return String.valueOf(value);
////				} else {
////					if (value >= 0 && value <= 6) {
////						return weekDays[(int) (value)];
////					}
////				}
////				return null;
////			}
////		});
////
////		if (lMoods != null && lMoods.size() != 0) {
////
////			int k;
////			for(k = 0; k < lMoods.size()-1; k++)
////			{
////				if(CompleteMood.getNoOfDay(lMoods.get(k).day) >
////						CompleteMood.getNoOfDay(lMoods.get(k+1).day))
////				{
////					break;
////				}
////			}
////
////			DataPoint[] lDataPoint = new DataPoint[k+1];
////			DataPoint[] lDataPoint1 = new DataPoint[lMoods.size()-(k+1)];
////			int i = 0;
////			int j = 0;
////			int prev = 0;
////
////			for (CompleteMood lMood : lMoods) {
////				int temp = CompleteMood.getNoOfDay(lMood.day);
////				if(temp < prev)
////				{
////					prev = 8;
////					lDataPoint1[j++] = new DataPoint(temp, lMood.energyLevel);
////				}
////				else {
////					lDataPoint[i++] = new DataPoint(temp, lMood.energyLevel);
////					prev = temp;
////				}
////			}
////
////			LineGraphSeries<DataPoint> series = new LineGraphSeries<>(lDataPoint);
//////			series.setColor(Color.GREEN);
////			series.setDrawDataPoints(true);
////			series.setDataPointsRadius(10);
////			series.setThickness(8);
////
////			LineGraphSeries<DataPoint> series1 = new LineGraphSeries<>(lDataPoint1);
////
////			series1.setDrawDataPoints(true);
////			series1.setDataPointsRadius(10);
////			series1.setThickness(8);
////
////			if(j != 0)
////			{
////				series.setTitle("Last Week");
////				series1.setTitle("This Week");
////				series.setColor(Color.GRAY);
////				series1.setColor(Color.BLUE);
////				lGraphView.addSeries(series1);
////				lGraphView.addSeries(series);
////			}
////			else
////			{
////				series.setTitle("This Week");
//////				series1.setTitle("This Week");
//////				series1.setColor(Color.GRAY);
////				series.setColor(Color.BLUE);
////				lGraphView.addSeries(series1);
////				lGraphView.addSeries(series);
////			}
////		}
////
////		lGraphView = view.findViewById(R.id.self_confidence_graph);
////
////		lGraphView.getViewport().setMinY(-10);
////		lGraphView.getViewport().setMaxY(10);
////		lGraphView.getViewport().setMinX(0);
////		lGraphView.getViewport().setMaxX(6);
////		lGraphView.getViewport().setXAxisBoundsManual(true);
////		lGraphView.getViewport().setYAxisBoundsManual(true);
////		lGraphView.getViewport().setScrollable(true);
////		lGraphView.getGridLabelRenderer().setNumHorizontalLabels(7);
////		lGraphView.getGridLabelRenderer().setNumVerticalLabels(21);
////		lGraphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
////			@Override
////			public String formatLabel(double value, boolean isValueX) {
////				if (!isValueX) {
////					return String.valueOf(value);
////				} else {
////					if (value >= 0 && value <= 6) {
////						return weekDays[(int) (value)];
////					}
////				}
////				return null;
////			}
////		});
////
////		if (lMoods != null && lMoods.size() != 0) {
////
////			int k;
////			for(k = 0; k < lMoods.size()-1; k++)
////			{
////				if(CompleteMood.getNoOfDay(lMoods.get(k).day) >
////						CompleteMood.getNoOfDay(lMoods.get(k+1).day))
////				{
////					break;
////				}
////			}
////
////			DataPoint[] lDataPoint = new DataPoint[k+1];
////			DataPoint[] lDataPoint1 = new DataPoint[lMoods.size()-(k+1)];
////			int i = 0;
////			int j = 0;
////			int prev = 0;
////
////			for (CompleteMood lMood : lMoods) {
////				int temp = CompleteMood.getNoOfDay(lMood.day);
////				if(temp < prev)
////				{
////					prev = 8;
////					lDataPoint1[j++] = new DataPoint(temp, lMood.selfConfidence);
////				}
////				else {
////					lDataPoint[i++] = new DataPoint(temp, lMood.selfConfidence);
////					prev = temp;
////				}
////			}
////
////			LineGraphSeries<DataPoint> series = new LineGraphSeries<>(lDataPoint);
//////			series.setColor(Color.GREEN);
////			series.setDrawDataPoints(true);
////			series.setDataPointsRadius(10);
////			series.setThickness(8);
////
////			LineGraphSeries<DataPoint> series1 = new LineGraphSeries<>(lDataPoint1);
////
////			series1.setDrawDataPoints(true);
////			series1.setDataPointsRadius(10);
////			series1.setThickness(8);
////
////			if(j != 0)
////			{
////				series.setTitle("Last Week");
////				series1.setTitle("This Week");
////				series.setColor(Color.GRAY);
////				series1.setColor(Color.BLUE);
////				lGraphView.addSeries(series1);
////				lGraphView.addSeries(series);
////			}
////			else
////			{
////				series.setTitle("This Week");
//////				series1.setTitle("This Week");
//////				series1.setColor(Color.GRAY);
////				series.setColor(Color.BLUE);
////				lGraphView.addSeries(series1);
////				lGraphView.addSeries(series);
////			}
////		}
////
//////		lGraphView = view.findViewById(R.id.ok_graph);
//////
//////		lGraphView.getViewport().setMinY(-10);
//////		lGraphView.getViewport().setMaxY(10);
//////		lGraphView.getViewport().setMinX(0);
//////		lGraphView.getViewport().setMaxX(6);
//////		lGraphView.getViewport().setXAxisBoundsManual(true);
//////		lGraphView.getViewport().setYAxisBoundsManual(true);
//////		lGraphView.getViewport().setScrollable(true);
//////		lGraphView.getGridLabelRenderer().setNumHorizontalLabels(7);
//////		lGraphView.getGridLabelRenderer().setNumVerticalLabels(21);
////////		lDepressionGraph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(lNumberFormat,lNumberFormat1));
//////		lGraphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter()
//////		{
//////			@Override
//////			public String formatLabel(double value, boolean isValueX) {
//////				if(!isValueX)
//////				{
//////					return String.valueOf(value);
//////				}
//////				else
//////				{
//////					if(value >= 0 && value <= 6)
//////					{
//////						return weekDays[(int) (value)];
//////					}
//////				}
//////				return null;
//////			}
//////		});
//////		lMoods= lMyDatabase.getThisWeekSad();
//////
//////
//////		if(lMoods != null)
//////		{
//////			DataPoint[] lDataPoint = new DataPoint[lMoods.size()];
//////			int i = 0;
//////
//////			for(Mood lMood : lMoods)
//////			{
//////				lDataPoint[i++] = new DataPoint(lMood.day, lMood.value);
//////			}
//////
//////
//////			LineGraphSeries<DataPoint> series3 = new LineGraphSeries<>(lDataPoint);
//////
//////			series3.setTitle("Random Curve 1");
//////			series3.setColor(Color.GREEN);
//////			series3.setDrawDataPoints(true);
//////			series3.setDataPointsRadius(10);
//////			series3.setThickness(8);
//////
//////			lGraphView.addSeries(series3);
//////
//////		}
//////
//////
//////		lGraphView = view.findViewById(R.id.fine_graph);
//////
//////		lGraphView.getViewport().setMinY(-10);
//////		lGraphView.getViewport().setMaxY(10);
//////		lGraphView.getViewport().setMinX(0);
//////		lGraphView.getViewport().setMaxX(6);
//////		lGraphView.getViewport().setXAxisBoundsManual(true);
//////		lGraphView.getViewport().setYAxisBoundsManual(true);
//////		lGraphView.getViewport().setScrollable(true);
//////		lGraphView.getGridLabelRenderer().setNumHorizontalLabels(7);
//////		lGraphView.getGridLabelRenderer().setNumVerticalLabels(21);
////////		lDepressionGraph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(lNumberFormat,lNumberFormat1));
//////		lGraphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter()
//////		{
//////			@Override
//////			public String formatLabel(double value, boolean isValueX) {
//////				if(!isValueX)
//////				{
//////					return String.valueOf(value);
//////				}
//////				else
//////				{
//////					if(value >= 0 && value <= 6)
//////					{
//////						return weekDays[(int) (value)];
//////					}
//////				}
//////				return null;
//////			}
//////		});
//////
//////		lMoods= lMyDatabase.getThisWeekSad();
//////
//////
//////		if(lMoods != null)
//////		{
//////			DataPoint[] lDataPoint = new DataPoint[lMoods.size()];
//////			int i = 0;
//////
//////			for(Mood lMood : lMoods)
//////			{
//////				lDataPoint[i++] = new DataPoint(lMood.day, lMood.value);
//////			}
//////
//////
//////			LineGraphSeries<DataPoint> series4 = new LineGraphSeries<>(lDataPoint);
//////
//////			series4.setTitle("Random Curve 1");
//////			series4.setColor(Color.GREEN);
//////			series4.setDrawDataPoints(true);
//////			series4.setDataPointsRadius(10);
//////			series4.setThickness(8);
//////
//////			lGraphView.addSeries(series4);
//////
//////		}
//////
//////		lGraphView = view.findViewById(R.id.happy_graph);
//////
//////		lGraphView.getViewport().setMinY(-10);
//////		lGraphView.getViewport().setMaxY(10);
//////		lGraphView.getViewport().setMinX(0);
//////		lGraphView.getViewport().setMaxX(6);
//////		lGraphView.getViewport().setXAxisBoundsManual(true);
//////		lGraphView.getViewport().setYAxisBoundsManual(true);
//////		lGraphView.getViewport().setScrollable(true);
//////		lGraphView.getGridLabelRenderer().setNumHorizontalLabels(7);
//////		lGraphView.getGridLabelRenderer().setNumVerticalLabels(21);
////////		lDepressionGraph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(lNumberFormat,lNumberFormat1));
//////		lGraphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter()
//////		{
//////			@Override
//////			public String formatLabel(double value, boolean isValueX) {
//////				if(!isValueX)
//////				{
//////					return String.valueOf(value);
//////				}
//////				else
//////				{
//////					if(value >= 0 && value <= 6)
//////					{
//////						return weekDays[(int) (value)];
//////					}
//////				}
//////				return null;
//////			}
//////		});
//////
//////		lMoods= lMyDatabase.getThisWeekSad();
//////
//////
//////		if(lMoods != null)
//////		{
//////			DataPoint[] lDataPoint = new DataPoint[lMoods.size()];
//////			int i = 0;
//////
//////			for(Mood lMood : lMoods)
//////			{
//////				lDataPoint[i++] = new DataPoint(lMood.day, lMood.value);
//////			}
//////
//////
//////			LineGraphSeries<DataPoint> series5 = new LineGraphSeries<>(lDataPoint);
//////
//////			series5.setTitle("Random Curve 1");
//////			series5.setColor(Color.GREEN);
//////			series5.setDrawDataPoints(true);
//////			series5.setDataPointsRadius(10);
//////			series5.setThickness(8);
//////
//////			lGraphView.addSeries(series5);
//////
//////		}
//////
//////		lGraphView = view.findViewById(R.id.excited_graph);
//////
//////		lGraphView.getViewport().setMinY(-10);
//////		lGraphView.getViewport().setMaxY(10);
//////		lGraphView.getViewport().setMinX(0);
//////		lGraphView.getViewport().setMaxX(6);
//////		lGraphView.getViewport().setXAxisBoundsManual(true);
//////		lGraphView.getViewport().setYAxisBoundsManual(true);
//////		lGraphView.getViewport().setScrollable(true);
//////		lGraphView.getGridLabelRenderer().setNumHorizontalLabels(7);
//////		lGraphView.getGridLabelRenderer().setNumVerticalLabels(21);
////////		lDepressionGraph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(lNumberFormat,lNumberFormat1));
//////		lGraphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter()
//////		{
//////			@Override
//////			public String formatLabel(double value, boolean isValueX) {
//////				if(!isValueX)
//////				{
//////					return String.valueOf(value);
//////				}
//////				else
//////				{
//////					if(value >= 0 && value <= 6)
//////					{
//////						return weekDays[(int) (value)];
//////					}
//////				}
//////				return null;
//////			}
//////		});
//////
//////		lMoods= lMyDatabase.getThisWeekSad();
//////
//////
//////		if(lMoods != null)
//////		{
//////			DataPoint[] lDataPoint = new DataPoint[lMoods.size()];
//////			int i = 0;
//////
//////			for(Mood lMood : lMoods)
//////			{
//////				lDataPoint[i++] = new DataPoint(lMood.day, lMood.value);
//////			}
//////
//////
//////			LineGraphSeries<DataPoint> series6 = new LineGraphSeries<>(lDataPoint);
//////
//////			series6.setTitle("Random Curve 1");
//////			series6.setColor(Color.GREEN);
//////			series6.setDrawDataPoints(true);
//////			series6.setDataPointsRadius(10);
//////			series6.setThickness(8);
//////
//////			lGraphView.addSeries(series6);
////
//////	}
//	}

	@Override
	public String getTAG() {
		return TAG;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(null);
	}

	@Override
	public void onDestroy() {
		mTherapist = false;
		mUID = "";
		super.onDestroy();
	}

	//	public void update()
//	{
//
////		StaticLabelsFormatter lStaticLabelsFormatter = new StaticLabelsFormatter(lDepressionGraph);
////		lStaticLabelsFormatter.setHorizontalLabels(new String[]{"Mon", "Tue", "Wed", "Thr", "Fri", "Sat", "Sun"});
//
//		initializeGraphs(view);
//
//	}
}
