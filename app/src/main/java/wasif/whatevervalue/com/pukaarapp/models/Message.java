package wasif.whatevervalue.com.pukaarapp.models;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.utils.App;

/**
 * Created by AkshayeJH on 24/07/17.
 **/

public class Message {

	public Message() {
	}

	private String message, type = "text";
    private long time;
    private boolean seen;
    private String from;
    private String to;

    private Bitmap mBitmap;
    private Bitmap placeHolder;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    private String messageID;

    public String getMessageID() {
        return messageID;
    }

    public long getTImeinMillis()
    {return time;}
    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public Message(String from) {
        this.from = from;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Message(final String message, String type, long time, String from, boolean seen) {

	    this.message = message;
	    this.type = type;
	    this.time = time;
	    this.seen = seen;
	    this.from = from;

    }

    public void initializeImages(Context pContext)
    {
	    placeHolder = BitmapFactory.decodeResource(pContext.getResources()
			    , R.drawable.ic_menu_gallery );

	    if (type.equals("image"))
	    {

		    Thread lThread = new Thread(new Runnable() {
			    @Override
			    public void run() {
				    URL url = null;
				    try {
					    url = new URL(message);
				    } catch (MalformedURLException pE) {
					    pE.printStackTrace();
				    }
				    try {
					    if (url != null) {

						    mBitmap = BitmapFactory.decodeStream(url
								    .openConnection()
								    .getInputStream());

					    }
				    } catch (IOException pE) {
					    pE.printStackTrace();
				    }
			    }
		    });
		    lThread.start();

	    }

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return new Date(time).toString();
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

}
