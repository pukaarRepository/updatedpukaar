package wasif.whatevervalue.com.pukaarapp.session;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.models.Message;
import wasif.whatevervalue.com.pukaarapp.models.User;
import wasif.whatevervalue.com.pukaarapp.utils.DateConverter;
import wasif.whatevervalue.com.pukaarapp.utils.Utility;

import static wasif.whatevervalue.com.pukaarapp.utils.App.getContext;

public class TherapistChat extends AppCompatActivity {


    private String mChatUser;
    private Toolbar mChatToolbar;
    private DatabaseReference mRootRef;
    private TextView mTitleView;
    private TextView mLastSeenView;
    private CircleImageView mProfileImage;
    private FirebaseAuth mAuth;
    private String mCurrentUserId;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private ImageButton mChatAddBtn;
    private ImageButton mChatSendBtn;
    private EditText mChatMessageView;

    int limit = 0;

    private RecyclerView mMessagesList;
    private SwipeRefreshLayout mRefreshLayout;

    private final LinkedList<Message> mMessageList = new LinkedList<>();
    private LinearLayoutManager mLinearLayout;
    private MessageAdapter mAdapter;

    private static final int TOTAL_ITEMS_TO_LOAD = 10;
    private int mCurrentPage = 1;

    private static final int GALLERY_PICK = 1;

    // Storage Firebase
    private StorageReference mImageStorage;


    private static final int REQUEST_CAMERA = 1202;
    private static final String TAKE_PHOTO = "TAKE PHOTO";
    private static final String CHOOSE_FROM_GALLERY = "Choose From Gallery";
    private static final CharSequence CANCEL = "CANCEL";
    private int SELECT_FILE = 1;
    final int SPEAK_INTENT = 10;
    final int CROP_PIC = 2;
    private Bitmap bitmap;

    Button lButton;


    //New Solution
    private int itemPos = 0;

    private String mLastKey = "";
    private String mPrevKey = "";

    private static final String TAG = "TherapistChat";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mChatUser=getIntent().getStringExtra("userID");
        Log.d(TAG, "onCreate: "+ mChatUser);

        mRootRef = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mCurrentUserId = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();


        mChatAddBtn = findViewById(R.id.chat_add_btn);
        mChatSendBtn = findViewById(R.id.chat_send_btn);
        mChatMessageView = findViewById(R.id.chat_message_view);

        Log.d(TAG, "onCreateView: "+ mChatSendBtn);

        mAdapter = new MessageAdapter(mMessageList, "", "", false);
//		mAdapter = new MessageListAdapter(messagesList, getApplicationContext());

        mCurrentUserId=mAuth.getCurrentUser().getUid();

        if(mChatUser == null)
        {
            return;
        }
        Log.d(TAG, "onCallback: "+ mChatUser);

        mMessagesList = findViewById(R.id.messages_list);
        mRefreshLayout = findViewById(R.id.message_swipe_layout);
        mLinearLayout = new LinearLayoutManager(getContext());

        mMessagesList.setHasFixedSize(false);
        mMessagesList.setLayoutManager(mLinearLayout);
        mMessagesList.setAdapter(mAdapter);

        final RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(getApplicationContext()) {
            @Override protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };

        //------- IMAGE STORAGE ---------
        mImageStorage = FirebaseStorage.getInstance().getReference();

        Log.d(TAG, "onCreateView: "+ mChatUser);
        Log.d(TAG, "onCreateView: "+ mCurrentUserId);

        loadMessages();

        //mTitleView.setText(userName);

        mRootRef.child("users").child(mChatUser).addValueEventListener(new ValueEventListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String online="";

                if(dataSnapshot.hasChild("online")){

                    online = Objects.requireNonNull(dataSnapshot.child("online").getValue()).toString();

                }


                if(online.equals("true")) {

                    mLastSeenView.setText("Online");
                } else {

                    GetTimeAgo getTimeAgo = new GetTimeAgo();

                    long lastTime=12;

                    String lastSeenTime = GetTimeAgo.getTimeAgo(lastTime,
                            getApplicationContext());

                    //mLastSeenView.setText(lastSeenTime);

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mChatSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sendMessage();
            }
        });



        mChatAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectImage();

            }
        });



        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mCurrentPage++;

                itemPos = 0;

                loadMoreMessages();
            }
        });
    }

    public void setUserImage(String thumb_image, Context ctx){

    }

    private void loadMoreMessages() {

        DatabaseReference messageRef = mRootRef.child("messages")
                .child(mCurrentUserId)
                .child(mChatUser);

        Query messageQuery = messageRef.orderByChild("time")
                .endAt(mMessageList.get(limit).getTime())
                .limitToLast(10);

        final int[] index = {0};

        final String lastKey = mMessageList.get(0).getTime();

        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String s) {

                Message message = dataSnapshot.getValue(Message.class);
                assert message != null;
                message.initializeImages(getApplicationContext());
                String messageKey = message.getTime();

                assert messageKey != null;
                if(!messageKey.equals(lastKey)) {
                    mMessageList.add(index[0]++, message);

                    Log.d("TOTALKEYS", "Last Key : " + mLastKey + " | Prev Key : " + mPrevKey + " | Message Key : " + messageKey);

                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });

        messageQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {
                mAdapter.notifyDataSetChanged();
                mLinearLayout.scrollToPositionWithOffset(10, 0);
                mRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError pDatabaseError) {

            }
        });

    }

    private void getOtherUser(final SessionFragment.FirebaseCallBack firebaseCallback){

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference("users");

        myRef.child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                User user=dataSnapshot.getValue(User.class);
                firebaseCallback.onCallback(user);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



    public interface FirebaseCallBack {
        void onCallback(User user);
    }

    private void getID(String id){
        mChatUser=id;
    }

    private void loadMessages() {

        DatabaseReference messageRef = mRootRef.child("messages").child(mCurrentUserId).child(mChatUser);

        Query messageQuery = messageRef.orderByChild("time")
                .limitToLast(mCurrentPage * TOTAL_ITEMS_TO_LOAD);

        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String s) {

                Message message = dataSnapshot.getValue(Message.class);
                assert message != null;
                message.initializeImages(getApplicationContext());

                Log.d(TAG, "onChildAdded: "+ message.getMessage());

                itemPos++;

                if(itemPos == 1){

                    String messageKey = dataSnapshot.getKey();

                    mLastKey = messageKey;
                    mPrevKey = messageKey;

                }

                mMessageList.add(message);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        messageQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {

                mAdapter.notifyDataSetChanged();
                mMessagesList.scrollToPosition(mMessageList.size() + 1);
//				mMessagesList.smoothScrollToPositionFromTop(messagesList.size(), 3);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError pDatabaseError) {

            }
        });

    }

    private void sendMessage() {

        final String message = mChatMessageView.getText().toString();

        if(!TextUtils.isEmpty(message)){


            DatabaseReference user_message_push = mRootRef.child("messages")
                    .child(mCurrentUserId).child(mChatUser).push();

            String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
            String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

            String push_id = user_message_push.getKey();


            Date lDate = Calendar.getInstance().getTime();
            String time = DateConverter.convertToString(lDate);
//            final Message lMessage = new Message(message, "text", time, mCurrentUserId, false);
            final Map<String, Object> messageMap = new HashMap<>();
            messageMap.put("message", message);
            messageMap.put("seen", false);
            messageMap.put("type", "text");
            messageMap.put("time", time);
            messageMap.put("from", mCurrentUserId);
            messageMap.put("to", mChatUser);

            final Map<String, Object> messageUserMap = new HashMap<>();
            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

            mChatMessageView.setText("");

//			mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("seen").setValue(true);
//			mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("timestamp").setValue(ServerValue.TIMESTAMP);
//
//			mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("seen").setValue(false);
//			mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("timestamp").setValue(ServerValue.TIMESTAMP);



            mRootRef.updateChildren(messageUserMap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> pTask) {
                            if(pTask.isSuccessful())
                            {
//								messagesList.add(lMessage);
                                mAdapter.notifyDataSetChanged();
                                mMessagesList.scrollToPosition(mMessageList.size()+1);
                                //TODO: message send
                                Log.d("pTash", "Is Successful");
                            }
                            else
                            {
                                mRootRef.updateChildren(messageUserMap)
                                        .addOnCompleteListener(this);
                            }
                        }
                    });

        }

    }

    public String userChoosenTask;


    public void selectImage() {

        final CharSequence[] items = {TAKE_PHOTO, CHOOSE_FROM_GALLERY,
                CANCEL};

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(Objects.requireNonNull(getContext()));
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(getContext(), Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                if (items[item].equals(TAKE_PHOTO)) {
                    userChoosenTask = TAKE_PHOTO;
                    if (result)
                        cameraIntent();

                } else if (items[item].equals(CHOOSE_FROM_GALLERY)) {
                    userChoosenTask = CHOOSE_FROM_GALLERY;
                    if (result)
                        galleryIntent();

                } else if (items[item].equals(CANCEL)) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    Uri imageUri;

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_PICK && resultCode == RESULT_OK){

            Uri imageUri = data.getData();

            final String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
            final String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

            DatabaseReference user_message_push = mRootRef.child("messages")
                    .child(mCurrentUserId).child(mChatUser).push();

            final String push_id = user_message_push.getKey();


            final StorageReference filepath = mImageStorage.child("message_images").child( push_id + ".jpg");

            UploadTask uploadTask = null;
            uploadTask = filepath.putFile(imageUri);

            Task<Uri> urlTask=uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if(!task.isSuccessful()){
                        throw Objects.requireNonNull(task.getException());
                    }
                    return filepath.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        //System.out.println("Upload " + downloadUri);
                        Log.d(TAG, "onComplete: "+ downloadUri);
                        //Toast.makeText(mActivity, "Successfully uploaded", Toast.LENGTH_SHORT).show();
                        if (downloadUri != null) {

                            String photoStringLink = downloadUri.toString();
                            Date lDate = Calendar.getInstance().getTime();
                            String time = DateConverter.convertToString(lDate);
//                            final Message lMessage = new Message(photoStringLink, "image", mCurrentUserId, time, false);

                            Map<String, Object> messageMap = new HashMap<>();
                            messageMap.put("message", photoStringLink);
                            messageMap.put("seen", false);
                            messageMap.put("type", "image");
                            messageMap.put("time", time);
                            messageMap.put("from", mCurrentUserId);
                            messageMap.put("to", mChatUser);

                            final Map<String, Object> messageUserMap = new HashMap<String, Object>();
                            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
                            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

                            mChatMessageView.setText("");

                            mRootRef.updateChildren(messageUserMap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> pTask) {
                                            if (pTask.isSuccessful())
                                            {
//												messagesList.add(lMessages);
                                            }
                                            else
                                            {
                                                mRootRef.updateChildren(messageUserMap)
                                                        .addOnCompleteListener(this);

                                            }
                                        }
                                    });
                        }

                    }
                }
            });

        }

    }


    private void onBitmapGet(Uri data) {
        final String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
        final String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

        DatabaseReference user_message_push = mRootRef.child("messages")
                .child(mCurrentUserId).child(mChatUser).push();

        final String push_id = user_message_push.getKey();

        final StorageReference filepath = mImageStorage.child("message_images").child( push_id + ".jpg");

        UploadTask uploadTask = null;
        uploadTask = filepath.putFile(data);

        Task<Uri> urlTask=uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if(!task.isSuccessful()){
                    throw task.getException();
                }
                return filepath.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    //System.out.println("Upload " + downloadUri);
                    Log.d(TAG, "onComplete: "+ downloadUri);
                    //Toast.makeText(mActivity, "Successfully uploaded", Toast.LENGTH_SHORT).show();
                    if (downloadUri != null) {

                        String photoStringLink = downloadUri.toString();

                        Map<String, Object> messageMap = new HashMap<String, Object>();
                        messageMap.put("message", photoStringLink);
                        messageMap.put("seen", false);
                        messageMap.put("type", "image");
                        messageMap.put("time", ServerValue.TIMESTAMP);
                        messageMap.put("from", mCurrentUserId);

                        Map<String, Object> messageUserMap = new HashMap<>();
                        messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
                        messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

                        mChatMessageView.setText("");

                        mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                if(databaseError != null){

                                    Log.d("CHAT_LOG", databaseError.getMessage().toString());

                                }

                            }
                        });
                    }

                }
            }
        });


    }

    private void onCaptureImageResult(@NonNull Intent data) {

//		assert data.getExtras() != null;

//		ByteArrayOutputStream bytes = new ByteArrayOutputStream();

//        Bitmap pic = thumbnail.copy(Bitmap.Config.ARGB_8888, false);
//
//        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//
//        File destination = new File(Environment.getExternalStorageDirectory(),
//                System.currentTimeMillis() + ".jpg");
//
//        FileOutputStream fo;
//        try {
//
//            destination.createNewFile();
//            fo = new FileOutputStream(destination);
//            fo.write(bytes.toByteArray());
//            fo.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        ivImage.setImageBitmap(thumbnail);
        bitmap = (Bitmap) data.getExtras().get("data");

        Uri lUri = getImageUri(getApplicationContext(), bitmap);

        onBitmapGet(lUri);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        onBitmapGet(data.getData());

    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    /**
     * this function does the crop operation.
     */
    private void performCrop(Intent data) {

        Uri picUri = data.getData();
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, CROP_PIC);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            Toast toast = Toast
                    .makeText(getContext(), "This device doesn't support the crop action!", Toast.LENGTH_SHORT);
            toast.show();

            if (userChoosenTask.equals(CHOOSE_FROM_GALLERY))
                onSelectFromGalleryResult(data);
            else if (userChoosenTask.equals(TAKE_PHOTO))
                onCaptureImageResult(data);
        }
    }


}
