package wasif.whatevervalue.com.pukaarapp.dashboard;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Stack;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.database.DatabaseSchema;
import wasif.whatevervalue.com.pukaarapp.database.MyDatabase;
import wasif.whatevervalue.com.pukaarapp.intefaces.GetTAG;
import wasif.whatevervalue.com.pukaarapp.models.Therapist;
import wasif.whatevervalue.com.pukaarapp.models.Timings;
import wasif.whatevervalue.com.pukaarapp.models.User;
import wasif.whatevervalue.com.pukaarapp.session.AddNoteFragment;
import wasif.whatevervalue.com.pukaarapp.session.EditSettingsFragment;
import wasif.whatevervalue.com.pukaarapp.session.SessionFragment;
import wasif.whatevervalue.com.pukaarapp.session.SettingsFragment;
import wasif.whatevervalue.com.pukaarapp.utils.FirebaseMethods;

import static android.view.View.GONE;

@SuppressWarnings("unused")
public class DashboardActivity extends AppCompatActivity  {


	Bitmap bitmap;
	boolean editTextOpen = false;
	View fullScreenLayout;
    public static final String TAG = "Dashboard Activity";
    private Context mContext=DashboardActivity.this;
    private static final int ACTIVITY_NUM = 0;
    DrawerLayout drawer;
    Fragment current;

    Stack<Fragment> mFragmentStack = new Stack<>();
    public int selected = 0;

    static ImageView zoomImage;
	FrameLayout frameLayout;


    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;


    private FirebaseAuth mAuth;
    boolean doubleBackToExitPressedOnce = false;
    TextView firstUserWait;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        zoomImage= new ImageView(this);
        setContentView(R.layout.activity_dashboard);
		zoomImage = findViewById(R.id.zoom_image);
		firstUserWait= findViewById(R.id.firstUser);

        Log.d(TAG, "onCreate: I am in dashboard activity");




        mAuth = FirebaseAuth.getInstance();


        MyDatabase db = MyDatabase.getInstance(mContext);

        setTheCurrentUser();

        //addAllUsers();
        //addAllTherapists();


        Log.d(TAG, "onCreate Users: " +
                FirebaseMethods.getTableAsString(db.getReadableDatabase(), DatabaseSchema.UsersTable.NAME));


        Log.d(TAG, "onCreate Therapists Assigned: " +
                FirebaseMethods.getTableAsString(db.getReadableDatabase(), DatabaseSchema.TherapistsAssignedToUser.NAME));



//		tabs = findViewById(R.id.tabs);
//	    final ImageButton settings = tabs.findViewById(R.id.settings_tab);
//	    final ImageButton chat = tabs.findViewById(R.id.chat_tab);
//	    final ImageButton therapist = tabs.findViewById(R.id.therapist_tab);

        insertDataIntoDatabase(new FirebaseCallBack() {
            @Override
            public void onCallback() {
                changeFragment(new MainFragment(), MainFragment.TAG);
            }
        });




//        getSupportFragmentManager().beginTransaction()
//                .add(R.id.container, new MainFragment(), MainFragment.TAG)
//                .commit();
    }

    public interface FirebaseCallBack {
        void onCallback();
    }

    public void insertDataIntoDatabase(FirebaseCallBack firebaseCallBack){
        //checkUserIsAvailable();
        MyDatabase db = MyDatabase.getInstance(mContext);
        Log.d(TAG, "onCreate: "+
                FirebaseMethods.getTableAsString(db.getReadableDatabase(), DatabaseSchema.UsersTable.NAME));
        firebaseCallBack.onCallback();
    }

    public void setTheCurrentUser(){
        myRef = FirebaseDatabase.getInstance().getReference();
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                FirebaseMethods firebaseMethods= new FirebaseMethods(mContext);
                firebaseMethods.setTheCurrentUser(dataSnapshot);

                setTheCurrentTherapist();


                MyDatabase db = MyDatabase.getInstance(mContext);


                Log.d(TAG, "onCreate Therapists wich ki ae " +
                        FirebaseMethods.getTableAsString(db.getReadableDatabase(), DatabaseSchema.TherapistInfoTable.NAMEOFTABLE));

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setTheCurrentTherapist(){
        myRef = FirebaseDatabase.getInstance().getReference();
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                FirebaseMethods firebaseMethods= new FirebaseMethods(mContext);
                firebaseMethods.setTheCurrentTherapist(dataSnapshot);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void changeFragment(Fragment pFragment, String pTAG)
    {

    	if(current instanceof MainFragment) {
		    List<Fragment> lFragments = getSupportFragmentManager().getFragments();
		    for (Fragment lFragment : lFragments) {
			    getSupportFragmentManager()
					    .beginTransaction()
					    .remove(lFragment)
					    .commit();
		    }
	    }
	    else if(current!=null )
		    getSupportFragmentManager().beginTransaction()
				    .remove(current).commit();

    	current = pFragment;


        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, pFragment, pTAG)
		        .setCustomAnimations(R.animator.fragment_transition_in,
				        R.animator.fragment_transition_out)
                .commit();
    }

    public void checkUserIsAvailable(){

        String userID=mAuth.getCurrentUser().getUid();

        Log.d(TAG, "checkUserIsAvailable: "+ userID);

        MyDatabase db = MyDatabase.getInstance(mContext);


        Boolean userAvailable=db.isUserDataAvailable(userID);

        Log.d(TAG, "checkUserIsAvailable: "+ userAvailable);

        if(!userAvailable){
            Log.d(TAG, "checkUserIsAvailable: adding User in Database First Time");
            myRef=FirebaseDatabase.getInstance().getReference().child("users");
            myRef.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User user= dataSnapshot.getValue(User.class);
                    assert user != null;
                    Log.d(TAG, "onDataChange: "+ user.getAssignedTherapist());
                    MyDatabase ab = MyDatabase.getInstance(mContext);
                    //ab.setUser(user);


                   // checkTherapistIsAvailable();

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        else{
            //checkTherapistIsAvailable();
        }

    }

    public void addAllUsers(){

        myRef = FirebaseDatabase.getInstance().getReference();
        final FirebaseMethods firebaseMethods= new FirebaseMethods(mContext);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //firebaseMethods.addAllUsers(dataSnapshot);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void addAllTherapists(){

        myRef = FirebaseDatabase.getInstance().getReference();
        final FirebaseMethods firebaseMethods= new FirebaseMethods(mContext);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                firebaseMethods.addAllTherapists(dataSnapshot);

                //checkUserIsAvailable();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    /*

    public void checkTherapistIsAvailable(){

        Log.d(TAG, "checkTherapistIsAvailable: I am inside this function");

        String userID=mAuth.getCurrentUser().getUid();

        MyDatabase db = MyDatabase.getInstance(mContext);
        User user=db.getUser(userID);

        final String therapistID= user.getAssignedTherapist();

        Log.d(TAG, "checkTherapistIsAvailable: "+ therapistID);

        if(!therapistID.equals("none")){
            Boolean therapistAvailable=db.isTherapistDataAvailable(therapistID);

            Log.d(TAG, "checkTherapistIsAvailable: "+ therapistAvailable);

            if(!therapistAvailable) {
                Log.d(TAG, "checkTherapistIsAvailable: " + therapistID);
                myRef = FirebaseDatabase.getInstance().getReference().child("therapists");
                myRef.child(therapistID).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Therapist therapist = new Therapist();
                        Timings timings= new Timings();
                        therapist.setTherapist_id(therapistID);
                        therapist.setName(dataSnapshot.child("name").getValue().toString());
                        therapist.setDescription(dataSnapshot.child("description").getValue().toString());
                        therapist.setImageURL(dataSnapshot.child("imageURL").getValue().toString());
                        timings.setMonday(dataSnapshot.child("Timings").child("monday").getValue().toString());
                        timings.setTuesday(dataSnapshot.child("Timings").child("tuesday").getValue().toString());
                        timings.setWednesday(dataSnapshot.child("Timings").child("wednesday").getValue().toString());
                        timings.setThursday(dataSnapshot.child("Timings").child("thursday").getValue().toString());
                        timings.setFriday(dataSnapshot.child("Timings").child("friday").getValue().toString());
                        timings.setSaturday(dataSnapshot.child("Timings").child("saturday").getValue().toString());
                        timings.setSunday(dataSnapshot.child("Timings").child("sunday").getValue().toString());
                        therapist.setSpecialities(dataSnapshot.child("specialities").getValue().toString());
                        therapist.setUsersHelped(( dataSnapshot.child("usersHelped").getValue().toString()));
                        therapist.setTreatmentApproach(dataSnapshot.child("treatmentApproach").getValue().toString());
                        therapist.setTimeZone(dataSnapshot.child("timeZone").getValue().toString());
                        therapist.setLicense(dataSnapshot.child("license").getValue().toString());
                        therapist.setLicenseDes(dataSnapshot.child("licenseDes").getValue().toString());
                        therapist.setJoinedTime(dataSnapshot.child("joinedTime").getValue().toString());
                        therapist.setTimePracticeMonths((dataSnapshot.child("timePracticeMonths").getValue().toString()));
                        therapist.setDegree(dataSnapshot.child("degree").getValue().toString());
                        for (DataSnapshot assignedUser : dataSnapshot.child("assignedUsers").getChildren()) {
                            therapist.setAssignedUsers(assignedUser.getValue().toString());
                        }

                        therapist.setTimings(timings);

                        MyDatabase ab = MyDatabase.getInstance(mContext);
                        Log.d(TAG, "onDataChange: " + therapist);
                        ab.setTherapistProfile(therapist);


                        MyDatabase db = MyDatabase.getInstance(mContext);


                        Log.d(TAG, "onCreate: " +
                                FirebaseMethods.getTableAsString(db.getReadableDatabase(), DatabaseSchema.TherapistInfoTable.NAMEOFTABLE));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    */


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId())
		{
			case android.R.id.home:
				onBackPressed();
		}
		return super.onOptionsItemSelected(item);

	}


	public void selecttab(int i)
	{
	}




	public static void zoomImage(String image){


		Picasso.get().load(image).fit().centerInside().
				placeholder(R.drawable.ic_menu_gallery).into(zoomImage);

		zoomImage.setVisibility(View.VISIBLE);
		zoomImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
	}

	@Override
	public void onBackPressed() {
//		super.onBackPressed();


		if(zoomImage!=null){
            if(zoomImage.getVisibility()==View.VISIBLE){
                zoomImage.setVisibility(View.GONE);
                return;
            }
        }
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
		if(editTextOpen)
		{
			fullScreenLayout.setVisibility(GONE);
			doubleBackToExitPressedOnce=false;

		}
		else if(current instanceof EmotionHistory)
		{
			doubleBackToExitPressedOnce=false;
			Fragment lFragment = DiaryFragment.getInstance(FirebaseAuth.getInstance().getUid(), false,"");

			changeFragment(lFragment,
					((GetTAG)lFragment).getTAG() );
//			current = lFragment;
		}
		else if(current instanceof MoodDiaryFragment
				|| current instanceof DiaryFragment
				|| current instanceof EditSettingsFragment)
		{
			Fragment lFragment = new MainFragment();
			changeFragment(lFragment,
					((GetTAG)lFragment).getTAG() );
//			current = lFragment;
			doubleBackToExitPressedOnce=false;

		}
		else if(current instanceof AddNoteFragment)
		{
			Fragment lFragment = new MoodDiaryFragment();
			changeFragment(lFragment,
					((GetTAG)lFragment).getTAG() );
//			current = lFragment;
			doubleBackToExitPressedOnce=false;

		}
		if(doubleBackToExitPressedOnce)
			Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
//
//		if (mFragmentStack.empty()) {
//			super.onBackPressed();
//		} else {
//			Fragment lFragment = mFragmentStack.pop();
//			if (lFragment == null)
//				super.onBackPressed();
//			else {
//				changeFragment(lFragment, ((GetTAG) lFragment).getTAG(), true);
//				if(lFragment instanceof MoodDiaryFragment)
//					((MoodDiaryFragment)lFragment).update();
//			}
//		}

	}

}
