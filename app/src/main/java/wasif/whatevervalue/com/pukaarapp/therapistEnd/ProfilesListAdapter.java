package wasif.whatevervalue.com.pukaarapp.therapistEnd;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.admin.UserProfile;
import wasif.whatevervalue.com.pukaarapp.utils.FirebaseMethods;

/**
 * Created by User on 1/1/2018.
 */

@SuppressWarnings({"WeakerAccess", "unused"})
public class ProfilesListAdapter extends RecyclerView.Adapter<ProfilesListAdapter.ViewHolder> implements View.OnClickListener {

    private static final String TAG = "ProfilesListAdapter";

    private ArrayList<String> mImageNames;
    private ArrayList<String> mImages;
    private ArrayList<String> mUserIds;
    private Context mContext;
    String phoneNo;
    String message;
    final int MY_PERMISSIONS_REQUEST_SEND_SMS = 1000;
    final int PERMISSION_REQUEST_CODE=1;


    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    @SuppressWarnings("FieldCanBeLocal")
    private FirebaseAuth mAuth;
    private FirebaseMethods mFirebaseMethods;


    public ProfilesListAdapter(Context context, ArrayList<String> imageNames, ArrayList<String> images, ArrayList<String> ids) {
        Log.d(TAG, "RecyclerViewAdapter: I am hereee");
        mImageNames = imageNames;
        mImages = images;
        mUserIds = ids;
        mContext = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: I am inside");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem6, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Glide.with(mContext)
                .asBitmap()
                .load(mImages.get(position))
                .into(holder.image);

        holder.imageName.setText(mImageNames.get(position));
    }

    @Override
    public int getItemCount() {
        return mImageNames.size();
    }

    @Override
    public void onClick(View v) {

        if (v.equals(R.id.btn_cancel)) {

        }
    }


    @SuppressWarnings("WeakerAccess")
    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView image;
        TextView imageName;
        RelativeLayout parentLayout;
        Button assignUser;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.userProfilePicture);
            imageName = itemView.findViewById(R.id.userName);
            parentLayout = itemView.findViewById(R.id.parent_layout);
            assignUser= itemView.findViewById(R.id.assign_user);

            parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String userID= mUserIds.get(getAdapterPosition());
                    Intent intent= new Intent(mContext, UserProfile.class);
                    intent.putExtra("userID",userID);
                    mContext.startActivity(intent);
                }
            });



        }
    }
}














