package wasif.whatevervalue.com.pukaarapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;

import wasif.whatevervalue.com.pukaarapp.models.Message;
import wasif.whatevervalue.com.pukaarapp.models.Mood;
import wasif.whatevervalue.com.pukaarapp.models.Response;
import wasif.whatevervalue.com.pukaarapp.models.Therapist;
import wasif.whatevervalue.com.pukaarapp.models.Timings;
import wasif.whatevervalue.com.pukaarapp.models.User;
import wasif.whatevervalue.com.pukaarapp.utils.DateConverter;

import static wasif.whatevervalue.com.pukaarapp.database.DatabaseSchema.ChatForumTable.QID;


/**
 * Created by Taha Malik on 01/02/2019.
 **/

public class MyDatabase extends SQLiteOpenHelper {

    private static MyDatabase sMyDatabase = null;
    private static final String TAG = "MyDatabase";

    public static MyDatabase getInstance(Context pContext) {
        if (sMyDatabase == null) {
            sMyDatabase = new MyDatabase(pContext);
        }
        return sMyDatabase;
    }

    private MyDatabase(Context context) {

        super(context, "MyDatabase", null, 14);
    }

    @Override
    public void onCreate(SQLiteDatabase pSQLiteDatabase) {
        createTables(pSQLiteDatabase);
    }

    private void createTables(SQLiteDatabase pSQLiteDatabase) {

	    pSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " +
                DatabaseSchema.ChatForumTable.NAME + "(" +
                QID + " VARCHAR PRIMARY KEY , " +
                DatabaseSchema.ChatForumTable.UID + " VARCHAR, " +
                DatabaseSchema.ChatForumTable.QUERY + " VARCHAR, " +
                DatabaseSchema.ChatForumTable.SAME_HERE + " INTEGER, " +
                DatabaseSchema.ChatForumTable.TIMESTAMP + " VARCHAR " +
                ");");

        pSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " +
                DatabaseSchema.ResponseTable.NAME + "(" +
                DatabaseSchema.ResponseTable.RID + " VARCHAR PRIMARY KEY , " +
                DatabaseSchema.ResponseTable.UID + " VARCHAR, " +
                DatabaseSchema.ResponseTable.QUERY_ID + " VARCHAR, " +
                DatabaseSchema.ResponseTable.RESPONSE + " VARCHAR, " +
                DatabaseSchema.ResponseTable.DATE_TIME + " VARCHAR " +
                ");");

        pSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " +
                DatabaseSchema.SameHereTable.NAME + "(" +
                DatabaseSchema.SameHereTable.QUERY_ID + " VARCHAR, " +
                DatabaseSchema.SameHereTable.UID + " VARCHAR, " +
                "PRIMARY KEY(" + DatabaseSchema.SameHereTable.UID +
                " , " + DatabaseSchema.SameHereTable.QUERY_ID +
                "));");


        pSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " +
                DatabaseSchema.ChatTable.NAME + "(" +
                DatabaseSchema.ChatTable.FROM + " VARCHAR, " +
                DatabaseSchema.ChatTable.TO + " VARCHAR, " +
                DatabaseSchema.ChatTable.MessageID + " VARCHAR PRIMARY KEY, " +
                DatabaseSchema.ChatTable.Message + " VARCHAR, " +
                DatabaseSchema.ChatTable.TYPE + " VARCHAR, " +
                DatabaseSchema.ChatTable.DATE + " VARCHAR " +
                ");");


        pSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " +
                DatabaseSchema.TherapistInfoTable.NAMEOFTABLE + "(" +
                DatabaseSchema.TherapistInfoTable.NAME  + " VARCHAR, " +
                DatabaseSchema.TherapistInfoTable.THERAPISTID  + " VARCHAR PRIMARY KEY, " +
                DatabaseSchema.TherapistInfoTable.MONDAY  + " VARCHAR , " +
                DatabaseSchema.TherapistInfoTable.TUESDAY + " VARCHAR, " +
                DatabaseSchema.TherapistInfoTable.WEDNESDAY + " VARCHAR,  " +
                DatabaseSchema.TherapistInfoTable.THURSDAY + " VARCHAR, " +
                DatabaseSchema.TherapistInfoTable.FRIDAY + " VARCHAR, " +
                DatabaseSchema.TherapistInfoTable.SATURDAY + " VARCHAR, " +
                DatabaseSchema.TherapistInfoTable.SUNDAY + " VARCHAR, " +
                DatabaseSchema.TherapistInfoTable.DESCRIPTION + " VARCHAR, " +
                DatabaseSchema.TherapistInfoTable.LICENSE + " VARCHAR, " +
                DatabaseSchema.TherapistInfoTable.LICENSEDES + " VARCHAR, " +
                DatabaseSchema.TherapistInfoTable.TIMEZONE + " VARCHAR, " +
                DatabaseSchema.TherapistInfoTable.TIMEPRACTICEMONTHS + " VARCHAR, " +
                DatabaseSchema.TherapistInfoTable.USERSHELPED + " VARCHAR,  " +
                DatabaseSchema.TherapistInfoTable.IMAGEURL + " VARCHAR,  " +
                DatabaseSchema.TherapistInfoTable.SPECIALITIES + " VARCHAR,  " +
                DatabaseSchema.TherapistInfoTable.DEGREE + " VARCHAR, " +
                DatabaseSchema.TherapistInfoTable.JOINEDTIME + " VARCHAR, " +
                DatabaseSchema.TherapistInfoTable.TREATMENTAPPROACH + " VARCHAR " +
                ");");

        pSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " +
                DatabaseSchema.UsersAssignedToTherapist.NAME + "(" +
                DatabaseSchema.UsersAssignedToTherapist.THERAPISTID  + " VARCHAR, " +
                DatabaseSchema.UsersAssignedToTherapist.USERID + " VARCHAR PRIMARY KEY " +
                ");");

        pSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " +
                DatabaseSchema.TherapistsAssignedToUser.NAME + "(" +
                DatabaseSchema.TherapistsAssignedToUser.USERID  + " VARCHAR, " +
                DatabaseSchema.TherapistsAssignedToUser.THERAPISTID + " VARCHAR PRIMARY KEY, " +
                DatabaseSchema.TherapistsAssignedToUser.STATUS  + " VARCHAR " +
                ");");



        pSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " +
                DatabaseSchema.Specialities.NAME + "(" +
                DatabaseSchema.Specialities.THERAPISTID  + " VARCHAR, " +
                DatabaseSchema.Specialities.SPECIALITY + " VARCHAR, " +
                DatabaseSchema.Specialities.STATUS + " VARCHAR, " +
                "PRIMARY KEY(" + DatabaseSchema.Specialities.THERAPISTID +
                " , " + DatabaseSchema.Specialities.SPECIALITY +
                "));");


        pSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " +
                DatabaseSchema.UsersAssignedToTherapist.NAME + "(" +
                DatabaseSchema.UsersAssignedToTherapist.THERAPISTID  + " VARCHAR, " +
                DatabaseSchema.UsersAssignedToTherapist.USERID + " VARCHAR PRIMARY KEY " +
                ");");


        pSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " +
                DatabaseSchema.UsersTable.NAME + "(" +
                DatabaseSchema.UsersTable.USERID  + " VARCHAR PRIMARY KEY, " +
                DatabaseSchema.UsersTable.PHONENO + " VARCHAR," +
                DatabaseSchema.UsersTable.EMAIL + " VARCHAR," +
                DatabaseSchema.UsersTable.USERNAME + " VARCHAR," +
                DatabaseSchema.UsersTable.PROFILE_PHOTO + " VARCHAR," +
                DatabaseSchema.UsersTable.PASSWORD + " VARCHAR" +
                ");");


        pSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " +
                DatabaseSchema.CompleteMoodTable.NAME + "(" +
                DatabaseSchema.CompleteMoodTable.DATE + " VARCHAR PRIMARY KEY," +
                DatabaseSchema.CompleteMoodTable.DAY + " VARCHAR ," +
                DatabaseSchema.CompleteMoodTable.MOOD + " INTEGER," +
                DatabaseSchema.CompleteMoodTable.ANXIETY + " INTEGER," +
                DatabaseSchema.CompleteMoodTable.ENERGY_LEVEL + " INTEGER," +
                DatabaseSchema.CompleteMoodTable.SELF_CONFIDENCE + " INTEGER," +
                DatabaseSchema.CompleteMoodTable.MED_TAKEN + " INTEGER," +
		        DatabaseSchema.CompleteMoodTable.NOTE + " VARCHAR );");

    }

    public boolean shouldSameHereEnabled(String QID, String UID) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor lCursor = db.query(DatabaseSchema.ChatForumTable.NAME, DatabaseSchema.ChatForumTable.COLS,
                DatabaseSchema.ChatForumTable.QID + " = ? AND " +
                        DatabaseSchema.ChatForumTable.UID + " = ? ", new String[]{QID, UID}, null, null, null);

        if (lCursor.getCount() == 0) {
            lCursor = db.query(DatabaseSchema.SameHereTable.NAME,
                    DatabaseSchema.SameHereTable.COLS, DatabaseSchema.SameHereTable.QUERY_ID + " = ? AND " +
                            DatabaseSchema.SameHereTable.UID + " = ? ",
                    new String[]{QID, UID}, null, null, null);
            if (lCursor.getCount() == 0) {
                lCursor.close();
                return true;
            }
        } else {
            lCursor.close();
            return false;
        }
        lCursor.close();
        return false;
    }

    public int totalSameHere(int QID) {

        SQLiteDatabase db = getReadableDatabase();

        Cursor lCursor = db.query(DatabaseSchema.ChatForumTable.NAME, DatabaseSchema.ChatForumTable.COLS,
                DatabaseSchema.ChatForumTable.QID + " = ?"
                , new String[]{"" + QID}, null, null, null);

        int count = lCursor.getCount();
        lCursor.close();
        return count;

    }

    public int getMessage() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor lCursor = db.query(DatabaseSchema.ChatTable.NAME, DatabaseSchema.ChatTable.COLS, null, null, null, null, null);

        int count = lCursor.getCount();
        lCursor.close();
        return count;
    }

    public Therapist getTherapistProfile(String therapist_id){
        SQLiteDatabase db = getReadableDatabase();

        Log.d(TAG, "getTherapistProfile: "+ therapist_id);

        Cursor lCursor = db.query(DatabaseSchema.TherapistInfoTable.NAMEOFTABLE, DatabaseSchema.TherapistInfoTable.COLS, DatabaseSchema.TherapistInfoTable.THERAPISTID + " = ?"
                , new String[]{"" + therapist_id}, null, null, null, null);


        Cursor mCursor = db.query(DatabaseSchema.UsersAssignedToTherapist.NAME, DatabaseSchema.UsersAssignedToTherapist.COLS, DatabaseSchema.UsersAssignedToTherapist.THERAPISTID + " = ?"
                , new String[]{"" + therapist_id}, null, null, null, null);


        Therapist therapist= new Therapist();

        if(lCursor.moveToFirst()) {

            therapist.setTherapist_id(lCursor.getString(0));
	        therapist.setName(lCursor.getString(1));
	        Timings timings= new Timings();
            timings.setMonday(lCursor.getString(2));
            timings.setTuesday(lCursor.getString(3));
            timings.setWednesday(lCursor.getString(4));
            timings.setThursday(lCursor.getString(5));
            timings.setFriday(lCursor.getString(6));
            timings.setSaturday(lCursor.getString(7));
            timings.setSunday(lCursor.getString(8));
	        therapist.setImageURL(lCursor.getString(9));
	        therapist.setDescription(lCursor.getString(10));
	        therapist.setLicense(lCursor.getString(11));
	        therapist.setLicenseDes(lCursor.getString(12));
	        therapist.setSpecialities(lCursor.getString(13));
	        therapist.setDegree(lCursor.getString(14));
	        therapist.setJoinedTime(lCursor.getString(15));
	        therapist.setTimePracticeMonths(lCursor.getString(16));
	        therapist.setTimeZone(lCursor.getString(17));
	        therapist.setTreatmentApproach(lCursor.getString(18));
	        therapist.setUsersHelped(lCursor.getString(19));

            therapist.setTimings(timings);
        }

        while (mCursor.moveToNext()){
            therapist.setAssignedUsers(mCursor.getString(1));
        }


        lCursor.close();
        mCursor.close();

        return therapist;
    }

    public ArrayList<Therapist> getAllTherapists(){

        SQLiteDatabase db = getReadableDatabase();

        ArrayList<Therapist> therapists= new ArrayList<>();

        Cursor lCursor = db.query(DatabaseSchema.TherapistInfoTable.NAMEOFTABLE, DatabaseSchema.TherapistInfoTable.COLS, null, null, null, null, null);


        Cursor mCursor = db.query(DatabaseSchema.UsersAssignedToTherapist.NAME, DatabaseSchema.UsersAssignedToTherapist.COLS, null, null, null, null, null);

        Cursor nCursor = db.query(DatabaseSchema.Specialities.NAME, DatabaseSchema.Specialities.COLS, null, null, null, null, null);

        Therapist therapist= new Therapist();


        while (lCursor.moveToNext()) {

            therapist.setTherapist_id(lCursor.getString(0));
            therapist.setName(lCursor.getString(1));
            Timings timings= new Timings();
            timings.setMonday(lCursor.getString(2));
            timings.setTuesday(lCursor.getString(3));
            timings.setWednesday(lCursor.getString(4));
            timings.setThursday(lCursor.getString(5));
            timings.setFriday(lCursor.getString(6));
            timings.setSaturday(lCursor.getString(7));
            timings.setSunday(lCursor.getString(8));
            therapist.setImageURL(lCursor.getString(9));
            therapist.setDescription(lCursor.getString(10));
            therapist.setLicense(lCursor.getString(11));
            therapist.setLicenseDes(lCursor.getString(12));
            therapist.setSpecialities(lCursor.getString(13));
            therapist.setDegree(lCursor.getString(14));
            therapist.setJoinedTime(lCursor.getString(15));
            therapist.setTimePracticeMonths(lCursor.getString(16));
            therapist.setTimeZone(lCursor.getString(17));
            therapist.setTreatmentApproach(lCursor.getString(18));
            therapist.setUsersHelped(lCursor.getString(19));
            while(mCursor.moveToNext()){
                therapist.setAssignedUsers(mCursor.getString(1));
            }
            therapist.setTimings(timings);
            therapists.add(therapist);
        }
        return therapists;
    }


    public void addMessage(Message message) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSchema.ChatTable.FROM, message.getFrom());
        contentValues.put(DatabaseSchema.ChatTable.Message, message.getMessage());
        contentValues.put(DatabaseSchema.ChatTable.MessageID, message.getMessageID());
        contentValues.put(DatabaseSchema.ChatTable.TYPE,message.getType());
        contentValues.put(DatabaseSchema.ChatTable.TO,message.getTo());
        contentValues.put(DatabaseSchema.ChatTable.DATE, message.getTime());
        db.insert(DatabaseSchema.ChatTable.NAME,null,contentValues);
    }

    public void setTherapistProfile(Therapist therapist){

        SQLiteDatabase db= getWritableDatabase();

        Log.d(TAG, "setTherapistProfile: setting therapist profile");

        ContentValues contentValues= new ContentValues();
        contentValues.put(DatabaseSchema.TherapistInfoTable.NAME,therapist.getName());
        contentValues.put(DatabaseSchema.TherapistInfoTable.THERAPISTID,therapist.getTherapist_id());
        contentValues.put(DatabaseSchema.TherapistInfoTable.MONDAY,therapist.getTimings().getMonday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.TUESDAY,therapist.getTimings().getTuesday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.WEDNESDAY,therapist.getTimings().getWednesday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.THURSDAY,therapist.getTimings().getThursday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.FRIDAY,therapist.getTimings().getFriday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.SATURDAY,therapist.getTimings().getSaturday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.SUNDAY,therapist.getTimings().getSunday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.SPECIALITIES,therapist.getSpecialities());
        contentValues.put(DatabaseSchema.TherapistInfoTable.IMAGEURL,therapist.getImageURL());
        contentValues.put(DatabaseSchema.TherapistInfoTable.LICENSEDES,therapist.getLicenseDes());
        contentValues.put(DatabaseSchema.TherapistInfoTable.LICENSE,therapist.getLicense());
        contentValues.put(DatabaseSchema.TherapistInfoTable.JOINEDTIME,therapist.getJoinedTime());
        contentValues.put(DatabaseSchema.TherapistInfoTable.DESCRIPTION,therapist.getDescription());
        contentValues.put(DatabaseSchema.TherapistInfoTable.TIMEPRACTICEMONTHS,therapist.getTimePracticeMonths());
        contentValues.put(DatabaseSchema.TherapistInfoTable.TIMEZONE,therapist.getTimeZone());
        contentValues.put(DatabaseSchema.TherapistInfoTable.TREATMENTAPPROACH,therapist.getTreatmentApproach());
        contentValues.put(DatabaseSchema.TherapistInfoTable.USERSHELPED,therapist.getUsersHelped());
        contentValues.put(DatabaseSchema.TherapistInfoTable.DEGREE,therapist.getDegree());

        Log.d(TAG, "setTherapistProfile: "+ therapist.getDescription());

        db.insert(DatabaseSchema.TherapistInfoTable.NAMEOFTABLE,
                null,
                contentValues);


        for (String s : therapist.getAssignedUsers()) {
            ContentValues values = new ContentValues();
            values.put(DatabaseSchema.UsersAssignedToTherapist.THERAPISTID,therapist.getTherapist_id());
            values.put(DatabaseSchema.UsersAssignedToTherapist.USERID, s);
            db.insert(DatabaseSchema.UsersAssignedToTherapist.NAME,
                    null, values);
        }
    }

    public void updateTherapistProfile(Therapist therapist){
        SQLiteDatabase db= getWritableDatabase();

        Log.d(TAG, "setTherapistProfile: setting therapist profile");

        ContentValues contentValues= new ContentValues();
        contentValues.put(DatabaseSchema.TherapistInfoTable.NAME,therapist.getName());
        contentValues.put(DatabaseSchema.TherapistInfoTable.THERAPISTID,therapist.getTherapist_id());
        contentValues.put(DatabaseSchema.TherapistInfoTable.MONDAY,therapist.getTimings().getMonday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.TUESDAY,therapist.getTimings().getTuesday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.WEDNESDAY,therapist.getTimings().getWednesday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.THURSDAY,therapist.getTimings().getThursday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.FRIDAY,therapist.getTimings().getFriday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.SATURDAY,therapist.getTimings().getSaturday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.SUNDAY,therapist.getTimings().getSunday());
        contentValues.put(DatabaseSchema.TherapistInfoTable.SPECIALITIES,therapist.getSpecialities());
        contentValues.put(DatabaseSchema.TherapistInfoTable.IMAGEURL,therapist.getImageURL());
        contentValues.put(DatabaseSchema.TherapistInfoTable.LICENSEDES,therapist.getLicenseDes());
        contentValues.put(DatabaseSchema.TherapistInfoTable.LICENSE,therapist.getLicense());
        contentValues.put(DatabaseSchema.TherapistInfoTable.JOINEDTIME,therapist.getJoinedTime());
        contentValues.put(DatabaseSchema.TherapistInfoTable.DESCRIPTION,therapist.getDescription());
        contentValues.put(DatabaseSchema.TherapistInfoTable.TIMEPRACTICEMONTHS,therapist.getTimePracticeMonths());
        contentValues.put(DatabaseSchema.TherapistInfoTable.TIMEZONE,therapist.getTimeZone());
        contentValues.put(DatabaseSchema.TherapistInfoTable.TREATMENTAPPROACH,therapist.getTreatmentApproach());
        contentValues.put(DatabaseSchema.TherapistInfoTable.USERSHELPED,therapist.getUsersHelped());
        contentValues.put(DatabaseSchema.TherapistInfoTable.DEGREE,therapist.getDegree());

        Log.d(TAG, "setTherapistProfile: "+ therapist.getDescription());

        db.update(DatabaseSchema.TherapistInfoTable.NAMEOFTABLE,
                contentValues,DatabaseSchema.TherapistInfoTable.THERAPISTID + " = ?"
                , new String[]{"" + therapist.getTherapist_id()});



        for (String s : therapist.getAssignedUsers()) {

            ContentValues values = new ContentValues();
            values.put(DatabaseSchema.UsersAssignedToTherapist.USERID, s);
            values.put(DatabaseSchema.UsersAssignedToTherapist.THERAPISTID, therapist.getTherapist_id());
            db.insert(DatabaseSchema.UsersAssignedToTherapist.NAME,
                    null, values);
        }
    }



    public void updateUserProfile(User user){
        SQLiteDatabase db= getWritableDatabase();

        Log.d(TAG, "setUserProfile: setting user profile");

        ContentValues contentValues= new ContentValues();
        contentValues.put(DatabaseSchema.UsersTable.USERID,user.getUser_id());
        contentValues.put(DatabaseSchema.UsersTable.PASSWORD,user.getPassword());
        contentValues.put(DatabaseSchema.UsersTable.PHONENO,user.getPhone_number());
        contentValues.put(DatabaseSchema.UsersTable.PROFILE_PHOTO,user.getImageURL());
        contentValues.put(DatabaseSchema.UsersTable.USERNAME,user.getName());
        contentValues.put(DatabaseSchema.UsersTable.EMAIL,user.getEmail());


        db.update(DatabaseSchema.UsersTable.NAME,
                contentValues,DatabaseSchema.UsersTable.USERID + " = ?"
                , new String[]{"" + user.getUser_id()});
    }

    public void deleteTherapist(String therapist_id){


        SQLiteDatabase db= getWritableDatabase();
        db.delete(DatabaseSchema.TherapistInfoTable.NAMEOFTABLE,
                DatabaseSchema.TherapistInfoTable.THERAPISTID + " = ?"
                , new String[]{"" + therapist_id});


        db.delete(DatabaseSchema.Specialities.NAME,
                DatabaseSchema.Specialities.THERAPISTID + " = ?"
                , new String[]{"" + therapist_id});


        db.delete(DatabaseSchema.UsersAssignedToTherapist.NAME,
                DatabaseSchema.UsersAssignedToTherapist.THERAPISTID + " = ?"
                , new String[]{"" + therapist_id});
    }



    public void setUser(User user){
        SQLiteDatabase db= getWritableDatabase();

        Log.d(TAG, "setUser: Setting the user ");

        ContentValues contentValues= new ContentValues();
        contentValues.put(DatabaseSchema.UsersTable.EMAIL,user.getEmail());
        contentValues.put(DatabaseSchema.UsersTable.USERNAME,user.getName());
        contentValues.put(DatabaseSchema.UsersTable.PHONENO,user.getPhone_number());
        contentValues.put(DatabaseSchema.UsersTable.PROFILE_PHOTO,user.getImageURL());
        contentValues.put(DatabaseSchema.UsersTable.PASSWORD,user.getPassword());
        contentValues.put(DatabaseSchema.UsersTable.USERID,user.getUser_id());

        db.insert(DatabaseSchema.UsersTable.NAME,
                null,
                contentValues);

        for(int i=0;i<user.getAssignedTherapist().size();i++){
            ContentValues values = new ContentValues();
            values.put(DatabaseSchema.TherapistsAssignedToUser.USERID,user.getUser_id());
            values.put(DatabaseSchema.TherapistsAssignedToUser.THERAPISTID, user.getAssignedTherapist().get(i));
            values.put(DatabaseSchema.TherapistsAssignedToUser.STATUS, user.getStatus().get(i));
            db.insert(DatabaseSchema.TherapistsAssignedToUser.NAME,
                    null, values);
        }
    }

    public String getTableAsString(SQLiteDatabase db, String tableName) {
        Log.d(TAG, "getTableAsString called");
        String tableString = String.format("Table %s:\n", tableName);
        Cursor allRows  = db.rawQuery("SELECT * FROM " + tableName, null);
        if (allRows.moveToFirst() ){
            String[] columnNames = allRows.getColumnNames();
            do {
                for (String name: columnNames) {
                    tableString += String.format("%s: %s\n", name,
                            allRows.getString(allRows.getColumnIndex(name)));
                }
                tableString += "\n";

            } while (allRows.moveToNext());
        }

        return tableString;
    }

    public User getUser(String user_id){
        SQLiteDatabase db = getReadableDatabase();

        Log.d(TAG, "getUser: "+ user_id);

        Cursor lCursor = db.query(DatabaseSchema.UsersTable.NAME, DatabaseSchema.UsersTable.COLS,
                DatabaseSchema.UsersTable.USERID + " = ?"
                , new String[]{"" + user_id}, null, null, null);


        Cursor mCursor = db.query(DatabaseSchema.TherapistsAssignedToUser.NAME, DatabaseSchema.TherapistsAssignedToUser.COLS, DatabaseSchema.TherapistsAssignedToUser.USERID + " = ?"
                , new String[]{"" + user_id}, null, null, null, null);
        User user= new User();

        if(lCursor.moveToFirst()){
            user.setUser_id(lCursor.getString(0));
            user.setPhone_number(lCursor.getString(1));
            user.setEmail(lCursor.getString(2));
            user.setName(lCursor.getString(3));
            user.setImageURL(lCursor.getString(4));
            user.setPassword(lCursor.getString(5));
        }

        while (mCursor.moveToNext()){
            user.setAssignedTherapist(mCursor.getString(1));
            user.setStatus(mCursor.getString(2));

            Log.d(TAG, "getUser: ye kia hou raha hai bhai "+mCursor.getString(1) );
            Log.d(TAG, "getUser:bc  "+ mCursor.getString(2) );
        }

        lCursor.close();
        mCursor.close();

        return user;

    }

    public ArrayList<User> getAllUsers(){
        SQLiteDatabase db = getReadableDatabase();


        Cursor lCursor = db.query(DatabaseSchema.UsersTable.NAME, DatabaseSchema.UsersTable.COLS,
                null, null, null, null, null);

        ArrayList<User> users= new ArrayList<>();
        User user= new User();

        while(lCursor.moveToNext()){
            user.setUser_id(lCursor.getString(0));
            user.setPhone_number(lCursor.getString(1));
            user.setEmail(lCursor.getString(2));
            user.setName(lCursor.getString(3));
            user.setImageURL(lCursor.getString(4));
            user.setPassword(lCursor.getString(5));
            users.add(user);
        }

        lCursor.close();

        return users;

    }

    /*



    public ArrayList<User> getAllAssignedUsers(String therapist_id){

        SQLiteDatabase db = getReadableDatabase();

        Log.d(TAG, "getTherapistProfile: "+ therapist_id);

        ArrayList<User> allAssignedUsers= new ArrayList<>();


        Cursor mCursor = db.query(DatabaseSchema.TherapistAssignedToUsers.NAME, DatabaseSchema.TherapistAssignedToUsers.COLS, DatabaseSchema.TherapistAssignedToUsers.THERAPISTID + " = ?"
                , new String[]{"" + therapist_id}, null, null, null, null);

        while(mCursor.moveToNext()){
            allAssignedUsers.add(getAUser(mCursor.getString(1)));
        }

        return  allAssignedUsers;
    }


    public void updateAssignedToTherapist(String assinedToTherapist,String userID){

        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv = new ContentValues();


        cv.put(DatabaseSchema.UsersTable.ASSIGNEDTHERAPIST,assinedToTherapist);


        db.update(DatabaseSchema.UsersTable.NAME,
                cv, DatabaseSchema.UsersTable.USERID + " = ?",
                new String[]{userID});
    }

    */

    public Boolean isTherapistDataAvailable(String therapistID){

        Boolean therapistAvailable=false;

        SQLiteDatabase db = getReadableDatabase();

        Log.d(TAG, "getTherapistProfile: "+ therapistID);



        Cursor mCursor = db.query(DatabaseSchema.TherapistInfoTable.NAMEOFTABLE, DatabaseSchema.TherapistInfoTable.COLS, DatabaseSchema.TherapistInfoTable.THERAPISTID + " = ?"
                , new String[]{"" + therapistID}, null, null, null, null);

        if(mCursor.getCount()>0){
            therapistAvailable=true;
        }

        return therapistAvailable;
    }

    public Boolean isUserDataAvailable(String userID){

        Boolean UserAvailable=false;

        SQLiteDatabase db = getReadableDatabase();

        Log.d(TAG, "getTherapistProfile: "+ userID);



        Cursor mCursor = db.query(DatabaseSchema.UsersTable.NAME, DatabaseSchema.UsersTable.COLS, DatabaseSchema.UsersTable.USERID + " = ?"
                , new String[]{"" + userID}, null, null, null, null);

        if(mCursor.getCount()>0){
            UserAvailable=true;
        }

        return UserAvailable;
    }

    public void updateAssignedToUser(String assignedToUser,String therapistID){

        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv = new ContentValues();


        cv.put(DatabaseSchema.UsersAssignedToTherapist.USERID,assignedToUser);
        cv.put(DatabaseSchema.UsersAssignedToTherapist.THERAPISTID,therapistID);

        db.insert(DatabaseSchema.UsersAssignedToTherapist.NAME,
                null,
                cv);
    }


    public void setSpeciality(String speciality,String therapist_id){

        SQLiteDatabase db= getWritableDatabase();

        ContentValues contentValues= new ContentValues();
        contentValues.put(DatabaseSchema.Specialities.SPECIALITY,speciality);
        contentValues.put(DatabaseSchema.Specialities.THERAPISTID,therapist_id);
        contentValues.put(DatabaseSchema.Specialities.STATUS,"checked");

        db.insert(DatabaseSchema.Specialities.NAME,null,contentValues);
    }

    public ArrayList<String> getSpeciality(String therapist_id){
        SQLiteDatabase db = getReadableDatabase();

        Cursor lCursor = db.query(DatabaseSchema.Specialities.NAME, DatabaseSchema.Specialities.COLS,
                DatabaseSchema.Specialities.THERAPISTID + " = ?"
                , new String[]{"" + therapist_id}, null, null, null);

        ArrayList<String> specialities= new ArrayList<>();

        while (lCursor.moveToNext()){
            specialities.add(lCursor.getString(1));
        }
        lCursor.close();
        return specialities;
    }

    public void setAssignedUsers(String therapist_id, String assignedToUser){

        SQLiteDatabase db= getWritableDatabase();

        ContentValues contentValues= new ContentValues();

        contentValues.put(DatabaseSchema.UsersAssignedToTherapist.USERID,assignedToUser);
        contentValues.put(DatabaseSchema.UsersAssignedToTherapist.THERAPISTID,therapist_id);
        db.insert(DatabaseSchema.UsersAssignedToTherapist.NAME,
                null,
                contentValues);
    }


    public final static String datePatternInMoodAdding = "dd-MM-yyyy";

    private void addTodayMood(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);

        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getWritableDatabase();

        ContentValues lValues = new ContentValues();
        lValues.put(DatabaseSchema.MoodTable.DATE, date);
        lValues.put(DatabaseSchema.MoodTable.DAY, day);
        lValues.put(DatabaseSchema.MoodTable.MOOD, value);

        lSQLiteDatabase.insert(DatabaseSchema.MoodTable.NAME, null, lValues);

    }

    private void addTodayDepression(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);

        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getWritableDatabase();

        ContentValues lValues = new ContentValues();
        lValues.put(DatabaseSchema.GraphTable.DATE, date);
        lValues.put(DatabaseSchema.GraphTable.DAY, day);
        lValues.put(DatabaseSchema.GraphTable.VALUE, value);

        lSQLiteDatabase.insert(DatabaseSchema.DepressionGraphTable.NAME,
                null, lValues);

    }

    private void addTodaySad(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);

        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getWritableDatabase();

        ContentValues lValues = new ContentValues();
        lValues.put(DatabaseSchema.GraphTable.DATE, date);
        lValues.put(DatabaseSchema.GraphTable.DAY, day);
        lValues.put(DatabaseSchema.GraphTable.VALUE, value);

        lSQLiteDatabase.insert(DatabaseSchema.SadGraphTable.NAME,
                null, lValues);
    }

    private void addTodayUpset(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);

        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getWritableDatabase();

        ContentValues lValues = new ContentValues();
        lValues.put(DatabaseSchema.GraphTable.DATE, date);
        lValues.put(DatabaseSchema.GraphTable.DAY, day);
        lValues.put(DatabaseSchema.GraphTable.VALUE, value);

        lSQLiteDatabase.insert(DatabaseSchema.UpsetGraphTable.NAME,
                null, lValues);
    }

    private void addTodayOK(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);

        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getWritableDatabase();

        ContentValues lValues = new ContentValues();
        lValues.put(DatabaseSchema.GraphTable.DATE, date);
        lValues.put(DatabaseSchema.GraphTable.DAY, day);
        lValues.put(DatabaseSchema.GraphTable.VALUE, value);

        lSQLiteDatabase.insert(DatabaseSchema.OKGraphTable.NAME,
                null, lValues);

    }

    private void addTodayFine(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);

        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getWritableDatabase();

        ContentValues lValues = new ContentValues();
        lValues.put(DatabaseSchema.GraphTable.DATE, date);
        lValues.put(DatabaseSchema.GraphTable.DAY, day);
        lValues.put(DatabaseSchema.GraphTable.VALUE, value);

        lSQLiteDatabase.insert(DatabaseSchema.FineGraphTable.NAME,
                null, lValues);

    }

    private void addTodayHappy(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);

        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getWritableDatabase();

        ContentValues lValues = new ContentValues();
        lValues.put(DatabaseSchema.GraphTable.DATE, date);
        lValues.put(DatabaseSchema.GraphTable.DAY, day);
        lValues.put(DatabaseSchema.GraphTable.VALUE, value);

        lSQLiteDatabase.insert(DatabaseSchema.HappyGraphTable.NAME,
                null, lValues);

    }


    private void addTodayExcited(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);

        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getWritableDatabase();

        ContentValues lValues = new ContentValues();
        lValues.put(DatabaseSchema.GraphTable.DATE, date);
        lValues.put(DatabaseSchema.GraphTable.DAY, day);
        lValues.put(DatabaseSchema.GraphTable.VALUE, value);

        lSQLiteDatabase.insert(DatabaseSchema.ExcitedGraphTable.NAME,
                null, lValues);

    }


    public void setTodayMood(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);

        int mood = getTodayMood();
        if (mood == -111) {
            addTodayMood(value);
        } else {
            SQLiteDatabase db = getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(DatabaseSchema.MoodTable.DATE, date);
            cv.put(DatabaseSchema.MoodTable.DAY, day);
            cv.put(DatabaseSchema.MoodTable.MOOD, value);

            db.update(DatabaseSchema.MoodTable.NAME,
                    cv, DatabaseSchema.MoodTable.DATE + " = ?",
                    new String[]{date});
        }

    }

    public void setTodayAnxiety(int value) {

        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);

        int mood = getTodayDepression();
        if (mood == -111) {
            addTodayDepression(value);
        } else {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(DatabaseSchema.GraphTable.DATE, date);
            cv.put(DatabaseSchema.GraphTable.DAY, day);
            cv.put(DatabaseSchema.GraphTable.VALUE, value);

            db.update(DatabaseSchema.DepressionGraphTable.NAME,
                    cv, DatabaseSchema.GraphTable.DATE + " = ?",
                    new String[]{date});
        }

    }

    public void setTodayEnergyLevel(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);

        int mood = getTodaySad();
        if (mood == -111) {
            addTodaySad(value);
        } else {
            SQLiteDatabase db = getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(DatabaseSchema.GraphTable.DATE, date);
            cv.put(DatabaseSchema.GraphTable.DAY, day);
            cv.put(DatabaseSchema.GraphTable.VALUE, value);

            db.update(DatabaseSchema.SadGraphTable.NAME,
                    cv, DatabaseSchema.GraphTable.DATE + " = ?",
                    new String[]{date});
        }

    }

    public void setTodaySelfConfidence(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);

        int mood = getTodayUpset();
        if (mood == -111) {
            addTodayUpset(value);
        } else {
            SQLiteDatabase db = getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(DatabaseSchema.GraphTable.DATE, date);
            cv.put(DatabaseSchema.GraphTable.DAY, day);
            cv.put(DatabaseSchema.GraphTable.VALUE, value);

            db.update(DatabaseSchema.UpsetGraphTable.NAME,
                    cv, DatabaseSchema.GraphTable.DATE + " = ?",
                    new String[]{date});
        }

    }

    public void setTodayOK(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);

        int mood = getTodayOK();
        if (mood == -111) {
            addTodayOK(value);
        } else {
            SQLiteDatabase db = getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(DatabaseSchema.GraphTable.DATE, date);
            cv.put(DatabaseSchema.GraphTable.DAY, day);
            cv.put(DatabaseSchema.GraphTable.VALUE, value);

            db.update(DatabaseSchema.OKGraphTable.NAME,
                    cv, DatabaseSchema.GraphTable.DATE + " = ?",
                    new String[]{date});
        }

    }

    public void setTodayFine(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);

        int mood = getTodayFine();
        if (mood == -111) {
            addTodayFine(value);
        } else {
            SQLiteDatabase db = getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(DatabaseSchema.GraphTable.DATE, date);
            cv.put(DatabaseSchema.GraphTable.DAY, day);
            cv.put(DatabaseSchema.GraphTable.VALUE, value);

            db.update(DatabaseSchema.FineGraphTable.NAME,
                    cv, DatabaseSchema.GraphTable.DATE + " = ?",
                    new String[]{date});
        }

    }

    public void setTodayHappy(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);

        int mood = getTodayHappy();
        if (mood == -111) {
            addTodayHappy(value);
        } else {
            SQLiteDatabase db = getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(DatabaseSchema.GraphTable.DATE, date);
            cv.put(DatabaseSchema.GraphTable.DAY, day);
            cv.put(DatabaseSchema.GraphTable.VALUE, value);

            db.update(DatabaseSchema.HappyGraphTable.NAME,
                    cv, DatabaseSchema.GraphTable.DATE + " = ?",
                    new String[]{date});
        }

    }

    public void setTodayExcited(int value) {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());
        String day = df.format(lDate);

        int mood = getTodayExcited();
        if (mood == -111) {
            addTodayExcited(value);
        } else {
            SQLiteDatabase db = getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(DatabaseSchema.GraphTable.DATE, date);
            cv.put(DatabaseSchema.GraphTable.DAY, day);
            cv.put(DatabaseSchema.GraphTable.VALUE, value);

            db.update(DatabaseSchema.ExcitedGraphTable.NAME,
                    cv, DatabaseSchema.GraphTable.DATE + " = ?",
                    new String[]{date});
        }

    }

    private int getTodayMood() {
        Date lDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);

        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.MoodTable.NAME
                , DatabaseSchema.MoodTable.COLS,
                DatabaseSchema.MoodTable.DATE + " = ?",
                new String[]{date}, null, null, null);
        int mood = -111;
        if (lCursor.getCount() != 0) {
            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        }
        lCursor.close();
        return mood;
    }

    public int getLastSelectedMood()
    {
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.MoodTable.NAME
                , DatabaseSchema.MoodTable.COLS,
                null,
                null,null, null,
                DatabaseSchema.MoodTable.DATE + "DESC");

        int mood = -111;
        if (lCursor.getCount() != 0) {
            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        }
        lCursor.close();
        return mood;
    }

    public LinkedList<Mood> getThisWeekMoods() {
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();
        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.MoodTable.NAME,
                DatabaseSchema.MoodTable.COLS, null,
                null, null, null,
                DatabaseSchema.MoodTable.DATE + " DESC");

        LinkedList<Mood> moods = new LinkedList<>();

        if (lCursor.getCount() != 0) {
            for (int i = 0; i < 7; i++) {

                moods.addFirst(new Mood(lCursor.getInt(2),
                        Mood.getNoOfDay(lCursor.getString(1))));
                if (!lCursor.moveToNext()) {
                    break;
                }
            }
        } else {
            lCursor.close();
            return null;
        }
        lCursor.close();
        return moods;
    }

    public LinkedList<Mood> getThisWeekDepression() {
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();
        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.DepressionGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

        LinkedList<Mood> moods = new LinkedList<>();

        if (lCursor.getCount() != 0) {
            if(lCursor.moveToFirst()) {
                for (int i = 0; i < 7; i++) {

                    moods.addFirst(new Mood(lCursor.getInt(2),
                            Mood.getNoOfDay(lCursor.getString(1))));
                    if (!lCursor.moveToNext()) {
                        break;
                    }
                }
            }
        } else {
            lCursor.close();
            return null;
        }
        lCursor.close();
        return moods;

    }

    public LinkedList<Mood> getThisWeekSad() {
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();
        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.SadGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

        LinkedList<Mood> moods = new LinkedList<>();

        if (lCursor.getCount() != 0) {
            if(lCursor.moveToFirst()) {
                for (int i = 0; i < 7; i++) {

                    moods.addFirst(new Mood(lCursor.getInt(2),
                            Mood.getNoOfDay(lCursor.getString(1))));
                    if (!lCursor.moveToNext()) {
                        break;
                    }
                }
            }
        } else {
            lCursor.close();
            return null;
        }
        lCursor.close();
        return moods;

    }

    public LinkedList<Mood> getThisWeekUpset() {
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();
        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.UpsetGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

        LinkedList<Mood> moods = new LinkedList<>();

        if (lCursor.getCount() != 0) {
            if(lCursor.moveToFirst()) {
                for (int i = 0; i < 7; i++) {

                    moods.addFirst(new Mood(lCursor.getInt(2),
                            Mood.getNoOfDay(lCursor.getString(1))));
                    if (!lCursor.moveToNext()) {
                        break;
                    }
                }
            }
        } else {
            lCursor.close();
            return null;
        }
        lCursor.close();
        return moods;

    }

    public LinkedList<Mood> getThisWeekOK() {
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();
        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.OKGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

        LinkedList<Mood> moods = new LinkedList<>();

        if (lCursor.getCount() != 0) {
            if(lCursor.moveToFirst()) {
                for (int i = 0; i < 7; i++) {

                    moods.addFirst(new Mood(lCursor.getInt(2),
                            Mood.getNoOfDay(lCursor.getString(1))));
                    if (!lCursor.moveToNext()) {
                        break;
                    }
                }
            }
        } else {
            lCursor.close();
            return null;
        }
        lCursor.close();
        return moods;

    }

    public LinkedList<Mood> getThisWeekFine() {
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();
        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.FineGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

        LinkedList<Mood> moods = new LinkedList<>();

        if (lCursor.getCount() != 0) {
            if(lCursor.moveToFirst()) {
                for (int i = 0; i < 7; i++) {
                    moods.addFirst(new Mood(lCursor.getInt(2),
                            Mood.getNoOfDay(lCursor.getString(1))));
                    if (!lCursor.moveToNext()) {
                        break;
                    }
                }
            }
        } else {
            lCursor.close();
            return null;
        }
        lCursor.close();
        return moods;

    }

    public LinkedList<Mood> getThisWeekHappy() {
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();
        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.HappyGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

        LinkedList<Mood> moods = new LinkedList<>();

        if (lCursor.getCount() != 0) {
            if(lCursor.moveToFirst()) {
                for (int i = 0; i < 7; i++) {

                    moods.addFirst(new Mood(lCursor.getInt(2),
                            Mood.getNoOfDay(lCursor.getString(1))));
                    if (!lCursor.moveToNext()) {
                        break;
                    }
                }
            }
        } else {
            lCursor.close();
            return null;
        }
        lCursor.close();
        return moods;

    }

    public LinkedList<Mood> getThisWeekExcited() {
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();
        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.ExcitedGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

        LinkedList<Mood> moods = new LinkedList<>();

        if (lCursor.getCount() != 0) {
            if(lCursor.moveToFirst()) {
                for (int i = 0; i < 7; i++) {

                    moods.addFirst(new Mood(lCursor.getInt(2),
                            Mood.getNoOfDay(lCursor.getString(1))));
                    if (!lCursor.moveToNext()) {
                        break;
                    }
                }
            }
        } else {
            lCursor.close();
            return null;
        }
        lCursor.close();
        return moods;

    }

    private int getTodayExcited() {

        Date lDate = Calendar.getInstance().getTime();

        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.ExcitedGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, DatabaseSchema.GraphTable.DATE + "=?",
                new String[]{date}, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

        int mood = -111;

        if (lCursor.getCount() != 0) {

            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
//			mood = new Mood(lCursor.getInt(2),
//					Mood.getNoOfDay(lCursor.getString(1)));
        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }

    public int getLastExcited() {

        SQLiteDatabase db = getReadableDatabase();
        Cursor lCursor = db.query(DatabaseSchema.ExcitedGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

        int mood = -111;

        if (lCursor.getCount() != 0) {

            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }

    private int getTodayDepression() {

        Date lDate = Calendar.getInstance().getTime();

        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.DepressionGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, DatabaseSchema.GraphTable.DATE + "=?",
                new String[]{date}, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

//		Mood mood;

        int mood = -111;

        if (lCursor.getCount() != 0) {
//
//			mood = new Mood(Cursor.getInt(2),
//					Mood.getNoOfDay(lCursor.getString(1)));
            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }

    public int getLastDepression() {

        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.DepressionGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

//		Mood mood;

        int mood = -111;

        if (lCursor.getCount() != 0) {
            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }

    private int getTodaySad() {

        Date lDate = Calendar.getInstance().getTime();

        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.SadGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, DatabaseSchema.GraphTable.DATE + "=?",
                new String[]{date}, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

        int mood = -111;

        if (lCursor.getCount() != 0) {

            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }

    public int getLastSad() {

        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.SadGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

        int mood = -111;

        if (lCursor.getCount() != 0) {
            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }

    private int getTodayUpset() {

        Date lDate = Calendar.getInstance().getTime();

        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.UpsetGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, DatabaseSchema.GraphTable.DATE + "=?",
                new String[]{date}, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");


        int mood = -111;

        if (lCursor.getCount() != 0) {

//			mood = new Mood(lCursor.getInt(2),
//					Mood.getNoOfDay(lCursor.getString(1)));
            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }

    public int getLastUpset() {

        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.UpsetGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");

        int mood = -111;

        if (lCursor.getCount() != 0) {
            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }

    private int getTodayOK() {

        Date lDate = Calendar.getInstance().getTime();

        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.OKGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, DatabaseSchema.GraphTable.DATE + "=?",
                new String[]{date}, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");


        int mood = -111;

        if (lCursor.getCount() != 0) {

//			mood = new Mood(lCursor.getInt(2),
//					Mood.getNoOfDay(lCursor.getString(1)));
            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }

    public int getLastOK() {

        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.OKGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");


        int mood = -111;

        if (lCursor.getCount() != 0) {

            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);

        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }

    private int getTodayFine() {

        Date lDate = Calendar.getInstance().getTime();

        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.FineGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, DatabaseSchema.GraphTable.DATE + "=?",
                new String[]{date}, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");


        int mood = -111;

        if (lCursor.getCount() != 0) {

            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }

    public int getLastFine() {

        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.FineGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");


        int mood = -111;

        if (lCursor.getCount() != 0) {

            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }

    private int getTodayHappy() {

        Date lDate = Calendar.getInstance().getTime();

        SimpleDateFormat sdf = new SimpleDateFormat(datePatternInMoodAdding, Locale.getDefault());
        String date = sdf.format(lDate);
        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.HappyGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, DatabaseSchema.GraphTable.DATE + "=?",
                new String[]{date}, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");


        int mood = -111;

        if (lCursor.getCount() != 0) {

//			mood = new Mood(lCursor.getInt(2),
//					Mood.getNoOfDay(lCursor.getString(1)));
            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }

    public int getLastHappy() {

        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();

        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.HappyGraphTable.NAME,
                DatabaseSchema.GraphTable.COLS, null,
                null, null, null,
                DatabaseSchema.GraphTable.DATE + " DESC");


        int mood = -111;

        if (lCursor.getCount() != 0) {
            if(lCursor.moveToFirst())
                mood = lCursor.getInt(2);
        } else {
            lCursor.close();
            return mood;
        }
        lCursor.close();
        return mood;

    }
//
//    public void addChatForum(ChatForum pChatForum) {
//        SQLiteDatabase lSQLiteDatabase = getWritableDatabase();
//
//        ContentValues lContentValues = new ContentValues();
//        lContentValues.put(QID, pChatForum.QID);
//        lContentValues.put(DatabaseSchema.ChatForumTable.QUERY, pChatForum.query);
//        lContentValues.put(DatabaseSchema.ChatForumTable.UID, pChatForum.userID);
//        lContentValues.put(DatabaseSchema.ChatForumTable.SAME_HERE, pChatForum.samehere);
//        lContentValues.put(DatabaseSchema.ChatForumTable.TIMESTAMP,
//                DateConverter.convertToString(pChatForum.timeStamp));
//
//        lSQLiteDatabase.insert(DatabaseSchema.ChatForumTable.NAME,
//                null, lContentValues);
//
//        for (Response r :
//                pChatForum.mResponses) {
//
//            ContentValues rValues = new ContentValues();
//            rValues.put(DatabaseSchema.ResponseTable.RID, r.RID);
//            rValues.put(DatabaseSchema.ResponseTable.UID, r.userID);
//            rValues.put(DatabaseSchema.ResponseTable.RESPONSE, r.text);
//            rValues.put(DatabaseSchema.ResponseTable.QUERY_ID, r.QID);
//            rValues.put(DatabaseSchema.ResponseTable.DATE_TIME,
//		            DateConverter.convertToString(r.timeStamp));
//
//            lSQLiteDatabase.insert(DatabaseSchema.ResponseTable.NAME,
//                    null, rValues);
//        }
//
//        for (String s : pChatForum.sameHereUsers) {
//            ContentValues values = new ContentValues();
//            values.put(DatabaseSchema.SameHereTable.QUERY_ID, pChatForum.QID);
////			values.put(DatabaseSchema.SameHereTable.DATE,
////					DateConverter.convertToString(pChatForum.timeStamp));
//            values.put(DatabaseSchema.SameHereTable.UID, s);
//            lSQLiteDatabase.insert(DatabaseSchema.SameHereTable.NAME,
//                    null, values);
//        }
//    }
//

    public void addResponse(Response pResponse) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseSchema.ResponseTable.RID, pResponse.RID);
        values.put(DatabaseSchema.ResponseTable.UID, pResponse.userID);
//        values.put(DatabaseSchema.ResponseTable.QUERY_ID, pResponse.QID);
        values.put(DatabaseSchema.ResponseTable.RESPONSE, pResponse.text);
        values.put(DatabaseSchema.ResponseTable.DATE_TIME,
                DateConverter.convertToString(new Date(pResponse.timeStamp)));

        db.insert(DatabaseSchema.ResponseTable.NAME, null, values);
    }

//    public ArrayList<Response> getResponses(String QID)
//    {
//        SQLiteDatabase db = getReadableDatabase();
//
//        Cursor lCursor = db.query(DatabaseSchema.ResponseTable.NAME,
//                DatabaseSchema.ResponseTable.COLS,
//                DatabaseSchema.ResponseTable.QUERY_ID + "=?",
//                new String[]{QID}, null, null, null);
//
//        ArrayList<Response> mResponses = null;
//
//        if(lCursor.getCount() > 0) {
//            if(lCursor.moveToFirst()) {
//                mResponses = new ArrayList<>();
//                for (int i = 0; i < lCursor.getCount(); i++) {
//
//                    mResponses.add(new Response(lCursor.getString(0),
//                            lCursor.getString(1),
//                            DateConverter.convertStringToUtilDate(lCursor.getString(4)),
//                            lCursor.getString(3),
//                            lCursor.getString(2)));
//
//                    if (lCursor.moveToNext()) {
//                        break;
//                    }
//                }
//            }
//        }
//        lCursor.close();
//        return mResponses;
//    }
//
//    public LinkedList<ChatForum> getChatForum()
//    {
//
//        SQLiteDatabase lSQLiteDatabase = getReadableDatabase();
//
//        Cursor lCursor = lSQLiteDatabase.query(DatabaseSchema.ChatForumTable.NAME,
//                DatabaseSchema.ChatForumTable.COLS,
//                null, null, null,null,
//		        DatabaseSchema.ChatForumTable.TIMESTAMP + " DESC");
//
//        LinkedList<ChatForum> lChatForums = new LinkedList<>();
//        if(lCursor.moveToFirst()) {
//            for (int i = 0; i < lCursor.getCount(); i++) {
//
//                String QID = lCursor.getString(0);
//                String UID = lCursor.getString(1);
//                String query = lCursor.getString(2);
//                int likes = lCursor.getInt(3);
//
//                Date time = DateConverter.convertStringToUtilDate(lCursor.getString(4));
//                ChatForum lChatForum = new ChatForum(QID, time, query,
//                        UID, likes, null, null);
//
//                lChatForums.add(lChatForum);
//
//                if (!lCursor.moveToNext()) {
//                    break;
//                }
//            }
//        }
//        lCursor.close();
//
//        for(ChatForum cf : lChatForums) {
//
//            lCursor = lSQLiteDatabase.query(DatabaseSchema.SameHereTable.NAME,
//                    DatabaseSchema.SameHereTable.COLS,
//                    DatabaseSchema.SameHereTable.QUERY_ID + " =? ",
//                    new String[]{""+cf.QID}, null, null, null);
//
//            if(lCursor.moveToFirst()) {
//                for (int i = 0; i < lCursor.getCount(); i++) {
//                    String UID = lCursor.getString(1);
//                    cf.sameHereUsers.add(UID);
//                    if (!lCursor.moveToNext()) {
//                        break;
//                    }
//                }
//            }
//
//            lCursor.close();
//
//            lCursor = lSQLiteDatabase.query(DatabaseSchema.ResponseTable.NAME,
//                    DatabaseSchema.ResponseTable.COLS,
//                    DatabaseSchema.ResponseTable.QUERY_ID + " = ? ",
//                    new String[]{""+cf.QID}, null, null,
//		            DatabaseSchema.ResponseTable.DATE_TIME + " DESC");
//
//            if(lCursor.moveToFirst()) {
//                for (int i = 0; i < lCursor.getCount(); i++) {
//                    String RID = lCursor.getString(0);
//                    String UID = lCursor.getString(1);
//                    String QID = lCursor.getString(2);
//                    String response = lCursor.getString(3);
//                    Date lDate = DateConverter.convertStringToUtilDate(
//                            lCursor.getString(4)
//                    );
//                    Response lResponse = new Response(RID, UID, lDate, response, QID);
//                    cf.mResponses.add(lResponse);
//                    if (!lCursor.moveToNext()) {
//                        break;
//                    }
//                }
//            }
//            lCursor.close();
//        }
//
//        return lChatForums;
//    }

    public void addLike(String QID, String UID)
    {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(DatabaseSchema.SameHereTable.QUERY_ID, QID);
        cv.put(DatabaseSchema.SameHereTable.UID, UID);

        db.insert(DatabaseSchema.SameHereTable.NAME,
                null,
                cv);
        SQLiteDatabase db1 = getReadableDatabase();
        Cursor lCursor = db1.query(DatabaseSchema.ChatForumTable.NAME,
                new String[]{DatabaseSchema.ChatForumTable.SAME_HERE},
                DatabaseSchema.SameHereTable.QUERY_ID + " =?",
                new String[]{QID}, null, null, null);

        if(lCursor.moveToFirst()) {
            ContentValues lValues = new ContentValues();
            lValues.put(DatabaseSchema.ChatForumTable.SAME_HERE,
                    lCursor.getInt(0) + 1);

            db.update(DatabaseSchema.ChatForumTable.NAME,
                    lValues, DatabaseSchema.SameHereTable.QUERY_ID + " =?",
                    new String[]{QID});
        }
        lCursor.close();
    }

    public void setLikes(String QID, int likes)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DatabaseSchema.ChatForumTable.SAME_HERE, likes);
        db.update(DatabaseSchema.ChatForumTable.NAME, cv,
                DatabaseSchema.ChatForumTable.SAME_HERE + " =?",
                new String[]{"" + likes});

    }

    public int getLikes(String QID)
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor lCursor = db.query(DatabaseSchema.ChatForumTable.NAME,
                new String[]{DatabaseSchema.ChatForumTable.SAME_HERE},
                DatabaseSchema.SameHereTable.QUERY_ID + " =?",
                new String[]{QID}, null, null, null);
        int likes = 0;
        if(lCursor.moveToFirst())
            likes = lCursor.getInt(0);
        lCursor.close();
        return likes;
    }

//    public CompleteMood getLastCompleteMood()
//    {
//    	SQLiteDatabase db = getReadableDatabase();
//    	Cursor lCursor = db.query(DatabaseSchema.CompleteMoodTable.NAME,
//			    DatabaseSchema.CompleteMoodTable.COL,
//			    null, null, null, null,
//			    DatabaseSchema.CompleteMoodTable.DATE + " DESC");
//
//    	if(lCursor.getCount() == 0)
//	    {
//	    	lCursor.close();
//	    	return null;
//	    }
//	    else {
//	    	lCursor.moveToFirst();
//
//	    	String lDate = lCursor.getString(0);
//	    	String day = lCursor.getString(1);
//	    	int mood = lCursor.getInt(2);
//		    int anxiety = lCursor.getInt(3);
//	    	int energyLevel = lCursor.getInt(4);
//	    	int selfConfidence = lCursor.getInt(5);
//	    	int medTaken = lCursor.getInt(6);
//	    	String note = lCursor.getString(7);
//	    	lCursor.close();
//
//		    return new CompleteMood(lDate, day, mood, medTaken, anxiety, selfConfidence, energyLevel, note);
//	    }
//
//    }
//
//    public ArrayList<CompleteMood> getAllCompleteMoods()
//    {
//    	SQLiteDatabase db = getReadableDatabase();
//
//    	Cursor lCursor = db.query(DatabaseSchema.CompleteMoodTable.NAME,
//			    DatabaseSchema.CompleteMoodTable.COL,
//			    null, null, null, null,
//			    DatabaseSchema.CompleteMoodTable.DATE + " DESC");
//
//    	if(lCursor.getCount() == 0)
//	    {
//	    	lCursor.close();
//	    	return new ArrayList<>();
//	    }
//	    else
//	    {
//	    	ArrayList<CompleteMood> moods = new ArrayList<>();
//
//	    	while(lCursor.moveToNext())
//		    {
//			    String lDate = lCursor.getString(0);
//			    String day = lCursor.getString(1);
//			    int mood = lCursor.getInt(2);
//			    int anxiety = lCursor.getInt(3);
//			    int energyLevel = lCursor.getInt(4);
//			    int selfConfidence = lCursor.getInt(5);
//			    int medTaken = lCursor.getInt(6);
//			    String note = lCursor.getString(7);
//
//			    moods.add(new CompleteMood(lDate, day, mood, medTaken, anxiety,
//					    selfConfidence, energyLevel, note));
//		    }
//		    lCursor.close();
//	    	return moods;
//	    }
//    }
//
//
//	public ArrayList<CompleteMood> getThisWeeksCompleteMood()
//	{
//		SQLiteDatabase db = getReadableDatabase();
//
//		Cursor lCursor = db.query(DatabaseSchema.CompleteMoodTable.NAME,
//				DatabaseSchema.CompleteMoodTable.COL,
//				null, null, null, null,
//				DatabaseSchema.CompleteMoodTable.DATE + " ASC");
//
//		if(lCursor.getCount() == 0)
//		{
//			lCursor.close();
//			return null;
//		}
//		else
//		{
//			ArrayList<CompleteMood> moods = new ArrayList<>();
//
//			int i = 0;
//			while(lCursor.moveToNext() && i < 7)
//			{
//				String lDate = lCursor.getString(0);
//				String day = lCursor.getString(1);
//				int mood = lCursor.getInt(2);
//				int anxiety = lCursor.getInt(3);
//				int energyLevel = lCursor.getInt(4);
//				int selfConfidence = lCursor.getInt(5);
//				int medTaken = lCursor.getInt(6);
//				String note = lCursor.getString(7);
//
//				moods.add(new CompleteMood(lDate, day, mood, medTaken, anxiety,
//						selfConfidence, energyLevel, note));
//				i++;
//			}
//			lCursor.close();
//			return moods;
//		}
//	}
//
//
//	public void addTodaysCompleteMood(CompleteMood cm)
//    {
//    	SQLiteDatabase db = getReadableDatabase();
//
//    	Cursor lCursor = db.query(DatabaseSchema.CompleteMoodTable.NAME,
//			    new String[]{DatabaseSchema.CompleteMoodTable.DATE},
//			    DatabaseSchema.CompleteMoodTable.DATE + "=?",
//			    new String[]{cm.mDate}, null, null, null);
//
//    	db = getWritableDatabase();
//
//	    ContentValues cv = new ContentValues();
//
//	    cv.put(DatabaseSchema.CompleteMoodTable.DATE, cm.mDate);
//	    cv.put(DatabaseSchema.CompleteMoodTable.DAY, cm.day);
//	    cv.put(DatabaseSchema.CompleteMoodTable.ANXIETY, cm.anxiety);
//	    cv.put(DatabaseSchema.CompleteMoodTable.ENERGY_LEVEL, cm.energyLevel);
//	    cv.put(DatabaseSchema.CompleteMoodTable.SELF_CONFIDENCE, cm.selfConfidence);
//	    cv.put(DatabaseSchema.CompleteMoodTable.MOOD, cm.mood);
//	    cv.put(DatabaseSchema.CompleteMoodTable.MED_TAKEN, cm.medicationTaken);
//	    cv.put(DatabaseSchema.CompleteMoodTable.NOTE, cm.mNote);
//
//	    if(lCursor.getCount() == 0)
//	    {
//	    	db.insert(DatabaseSchema.CompleteMoodTable.NAME,
//				    null,
//				    cv);
//	    }
//	    else
//	    {
//			db.update(DatabaseSchema.CompleteMoodTable.NAME,
//					cv, DatabaseSchema.CompleteMoodTable.DATE + " =?",
//					new String[]{cm.mDate});
//	    }
//	    lCursor.close();
//    }

    @Override
    public void onUpgrade(SQLiteDatabase pSQLiteDatabase, int pI, int pI1) {

        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.DepressionGraphTable.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.SadGraphTable.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.UpsetGraphTable.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.OKGraphTable.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.FineGraphTable.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.HappyGraphTable.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.ExcitedGraphTable.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.SameHereTable.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.TherapistInfoTable.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.MoodTable.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.UsersAssignedToTherapist.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.ResponseTable.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.ChatTable.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.ChatForumTable.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.Specialities.NAME);
        pSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.CompleteMoodTable.NAME);
        createTables(pSQLiteDatabase);
    }
}
