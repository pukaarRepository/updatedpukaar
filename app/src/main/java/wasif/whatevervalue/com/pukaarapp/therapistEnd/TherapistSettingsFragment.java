package wasif.whatevervalue.com.pukaarapp.therapistEnd;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.admin.NotificationsList;
import wasif.whatevervalue.com.pukaarapp.therapistEnd.DiaryList;
import wasif.whatevervalue.com.pukaarapp.therapistEnd.SelectMoodList;
import wasif.whatevervalue.com.pukaarapp.usersQuestioningPortal.UsersPortal;

public class TherapistSettingsFragment extends Fragment {

    public static final String TAG = "TherapistSettingsFragment";

    Button newUsers;
    Button assignedUsers;
    final int PERMISSION_REQUEST_CODE=1;


    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;

    TextView profiles;
    TextView chats;
    private View mView;

    TextView noti;
    TextView selectMood;
    TextView diary;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View lView = inflater.inflate(R.layout.fragment_therapist_settings, null);
        mView = lView;

        noti= lView.findViewById(R.id.noti);
        selectMood=lView.findViewById(R.id.selectMood);
        diary=lView.findViewById(R.id.diary);
        lView.findViewById(R.id.pukaar_portal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View pView) {
                Intent lIntent = new Intent(getContext(), UsersPortal.class);
                startActivity(lIntent);
            }
        });

        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNotificationsList();
            }
        });

        selectMood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSelectMoodList();
            }
        });

        diary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToDiaryList();
            }
        });




//		FacebookSdk.sdkInitialize(getApplicationContext());
//		Log.d("AppLog", "key:" + FacebookSdk.getApplicationSignature(getActivity()));


        return lView;
    }

    public void goToNotificationsList(){
        Intent intent= new Intent(getActivity(), NotificationsList.class);
        startActivity(intent);
    }

    public void goToDiaryList(){
        Intent intent= new Intent(getActivity(), DiaryList.class);
        startActivity(intent);
    }

    public void goToSelectMoodList(){
        Intent intent= new Intent(getActivity(), SelectMoodList.class);
        startActivity(intent);
    }


}
