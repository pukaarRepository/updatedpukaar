package wasif.whatevervalue.com.pukaarapp.models;

/**
 * Created by Taha Malik on 01/02/2019.
 **/
public class Mood {
	public int value;
	public int day;

	public Mood(int pValue, int pDay)
	{
		value = pValue;
		day = pDay;
	}

	public static int getNoOfDay(String day)
	{
		if(day.toLowerCase().equals("mon"))
		{
			return 0;
		} else if(day.toLowerCase().equals("tue"))
		{
			return 1;
		} else if(day.toLowerCase().equals("wed"))
		{
			return 2;
		} else if(day.toLowerCase().equals("thr"))
		{
			return 3;
		} else if(day.toLowerCase().equals("fri"))
		{
			return 4;
		} else if(day.toLowerCase().equals("sat"))
		{
			return 5;
		} else if(day.toLowerCase().equals("sun"))
		{
			return 6;
		} else
		{
			return -1;
		}
	}

}
