package wasif.whatevervalue.com.pukaarapp.admin;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.models.User;

public class UserProfile extends AppCompatActivity {

    private static final String TAG = "UserProfile";


    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;


    private FirebaseAuth mAuth;

    CircleImageView userProfilePicture;


    TextView question1,question2,question3,question4,question5,question6,userName,userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        Log.d(TAG, "onCreate: started.");
        question1=findViewById(R.id.question1);
        question2=findViewById(R.id.question2);
        question3=findViewById(R.id.question3);
        question4=findViewById(R.id.question4);
        question5=findViewById(R.id.question5);
        question6=findViewById(R.id.question6);
        userName=findViewById(R.id.userName);
        userEmail=findViewById(R.id.userEmail);
        userProfilePicture=findViewById(R.id.userProfilePicture);

        String userID= getIntent().getStringExtra("userID");
        getQuestions(userID);

    }

    public void getQuestions(String userID){
        mFirebaseDatabase=FirebaseDatabase.getInstance();
        myRef=mFirebaseDatabase.getReference("users").child(userID);

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String name=dataSnapshot.child("name").getValue().toString();
                String email=dataSnapshot.child("email").getValue().toString();
                String profileDisplay=dataSnapshot.child("imageURL").getValue().toString();
                String answer4=dataSnapshot.child("questions").child("Are you currently taking any medication?").getValue().toString();
                String answer2=dataSnapshot.child("questions").child("Do you consider yourself to be spiritual or religious?").getValue().toString();
                String answer5=dataSnapshot.child("questions").child("How would you rate your current sleeping habits?").getValue().toString();
                String answer1=dataSnapshot.child("questions").child("What is your orientation?").getValue().toString();
                String answer3=dataSnapshot.child("questions").child("Which religion do you identify with?").getValue().toString();
                String answer6="";
                for(DataSnapshot dataSnapshot1 : dataSnapshot.child("questions").child("What do you think you're suffering from?").getChildren()){
                    answer6= answer6.concat(dataSnapshot1.getValue().toString());
                    answer6= answer6.concat(",");
                }

                question1.setText(answer1);
                question2.setText(answer2);
                question3.setText(answer3);
                question4.setText(answer4);
                question5.setText(answer5);
                question6.setText(answer6);
                userName.setText(name);
                userEmail.setText(email);
                Picasso.get().load(profileDisplay).into(userProfilePicture);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
