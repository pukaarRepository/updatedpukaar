package wasif.whatevervalue.com.pukaarapp.session;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.dashboard.DashboardActivity;
import wasif.whatevervalue.com.pukaarapp.dashboard.DiaryFragment;
import wasif.whatevervalue.com.pukaarapp.dashboard.MoodDiaryFragment;
import wasif.whatevervalue.com.pukaarapp.database.MyDatabase;
import wasif.whatevervalue.com.pukaarapp.intefaces.GetTAG;
import wasif.whatevervalue.com.pukaarapp.models.User;
import wasif.whatevervalue.com.pukaarapp.usersQuestioningPortal.UsersPortal;
import wasif.whatevervalue.com.pukaarapp.utils.FirebaseMethods;

public class SettingsFragment extends Fragment implements GetTAG {

    private static final String TAG = "SettingsFragment";


    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private FirebaseMethods mFirebaseMethods;
    private Context mContext=getContext();


    //firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mUserRef;
    CircleImageView userProfilePicture;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        Log.d(TAG, "onCreateView: started.");

        LinearLayout lTextView = view.findViewById(R.id.select_mood);
        userProfilePicture=view.findViewById(R.id.userProfilePicture);

        lTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View pView) {
                ((DashboardActivity)Objects.requireNonNull(getActivity())).changeFragment(
                        new MoodDiaryFragment(), MoodDiaryFragment.TAG);
            }
        });

        LinearLayout moodDiary = view.findViewById(R.id.diary);
        moodDiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View pView) {
                ((DashboardActivity)Objects.requireNonNull(getActivity())).changeFragment(
                        DiaryFragment.getInstance(FirebaseAuth.getInstance().getUid(), false, ""), DiaryFragment.TAG);
            }
        });

        LinearLayout chatForum = view.findViewById(R.id.pukaar_portal);
        chatForum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View pView) {
                Intent lIntent = new Intent(getContext(), UsersPortal.class);
                startActivity(lIntent);
            }
        });

        LinearLayout editSettings = view.findViewById(R.id.settings);
        editSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View pView) {
                ((DashboardActivity) Objects.requireNonNull(getActivity()))
                        .changeFragment(new EditSettingsFragment()
                                , EditSettingsFragment.TAG);
            }
        });

        //getCurrentUser();

        return view;
    }

    public void getCurrentUser(){
        MyDatabase lMyDatabase = MyDatabase.getInstance(getContext());
        mAuth = FirebaseAuth.getInstance();
        //User user=lMyDatabase.getUser(mAuth.getCurrentUser().getUid());
        //Picasso.get().load(user.getImageURL()).placeholder(R.drawable.ariana).into(userProfilePicture);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}
