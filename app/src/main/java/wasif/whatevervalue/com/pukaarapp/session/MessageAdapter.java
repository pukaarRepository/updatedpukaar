package wasif.whatevervalue.com.pukaarapp.session;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import java.util.List;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.dashboard.DashboardActivity;
import wasif.whatevervalue.com.pukaarapp.models.Message;

/**
 * Created by AkshayeJH on 24/07/17.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {

    private List<Message> mMessageList;
    private DatabaseReference mUserDatabase;
    private DatabaseReference mUsersSettingsDatabase;

	private String mTherapistName;
    private String mUserName;
    private boolean isTherapist;

    private static final String TAG = "MessageAdapter";

    public MessageAdapter(List<Message> mMessageList, String pTname, String pUsername, boolean pB) {

        this.mMessageList = mMessageList;
		mTherapistName = pTname;
		mUserName = pUsername;
		isTherapist = pB;

    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_item ,parent, false);
        return new MessageViewHolder(v);

    }

    @SuppressWarnings("WeakerAccess")
    public class MessageViewHolder extends RecyclerView.ViewHolder {

        public TextView messageText;
//        public TextView displayName;
//        public RoundedImageView messageImage;
		public LinearLayout container;
//		public View topMessage;
		public TextView timeStamp;
//		public View messageGravity;

        public MessageViewHolder(View view) {
            super(view);

            messageText = view.findViewById(R.id.message);
            container = view.findViewById(R.id.msg_container);
            timeStamp = view.findViewById(R.id.msg_timestamp);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull final MessageViewHolder viewHolder, int i) {

//	    if(i == 0)
//	    {
//		    viewHolder.topMessage.setVisibility(VISIBLE);
//		    viewHolder.main.setVisibility(GONE);
//
//	    }
//	    else {
//		    viewHolder.topMessage.setVisibility(GONE);

	    final Message c = mMessageList.get(i);
	    FirebaseAuth lAuth = FirebaseAuth.getInstance();

	    String current_user_id = lAuth.getUid();

	    String from_user = c.getFrom();
	    String message_type = c.getType();


//			    viewHolder.setIsRecyclable(false);
	    if (from_user.equals(current_user_id)) {
            Log.d(TAG, "onBindViewHolder: "+ current_user_id);
		    viewHolder.messageText.setTextColor(Color.BLACK);
//			    viewHolder.main.setGravity(Gravity.END);

//			    if(isTherapist)
//			    {
//			    	viewHolder.displayName.setText(mTherapistName);
//			    }
//			    else viewHolder.displayName.setText(mUserName);


			    if (message_type.equals("text")) {

			        viewHolder.messageText.setBackgroundResource(R.drawable.message_item_background);
//				    setValue(viewHolder.messageText, c.getMessage()
//						    , viewHolder.messageImage);

				    viewHolder.messageText.setText(c.getMessage());

			    } else {
				    viewHolder.messageText.setBackground(null);
//				    setValue(viewHolder.messageImage, c.getMessage()
//						    , viewHolder.messageText);

//                    viewHolder.messageImage.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//
//                            DashboardActivity.zoomImage(c.getMessage());
//                        }
//                    });
			    }

		    viewHolder.container.setGravity(Gravity.END);
		    if(i == 0)
		    {
		        viewHolder.timeStamp.setText(c.getTime());
		    }
		    else
		    {
		        long prevTime = mMessageList.get(i-1).getTImeinMillis();
		        long time = c.getTImeinMillis();

		        if(time - prevTime > 300000)
			    {
			        viewHolder.timeStamp.setText(c.getTime());
			    }
			    else viewHolder.timeStamp.setText("");

		    }
	    }
	    else {

//			    if(isTherapist)
//			    {
//				    viewHolder.displayName.setText(mUserName);
//			    }
//			    else viewHolder.displayName.setText(mTherapistName);


		    viewHolder.messageText.setTextColor(Color.BLACK);

//			    ((LinearLayout.LayoutParams)viewHolder.messageGravity.getLayoutParams())
//					    .gravity = Gravity.START;
//			    ((LinearLayout)viewHolder.messageGravity).setGravity(Gravity.START);
//			    ((LinearLayout.LayoutParams)viewHolder.main.getLayoutParams())
//					    .gravity = Gravity.START;

		    viewHolder.container.setGravity(Gravity.START);
		    if (message_type.equals("text")) {
			    viewHolder.messageText.setText(c.getMessage());
			    viewHolder.messageText
				    .setBackgroundResource
						    (R.drawable.message_item_other_person_background);

//				    setValue(viewHolder.messageText, c.getMessage()
//						    , viewHolder.messageImage);


		    } else {
			    viewHolder.messageText.setBackground(null);


//				    setValue(viewHolder.messageImage, c.getMessage()
//						    , viewHolder.messageText);

//				    viewHolder.messageImage.setOnClickListener(new View.OnClickListener() {
//						@Override
//						public void onClick(View v) {
//
//							DashboardActivity.zoomImage(c.getMessage());
//						}
//					});

		    }


	    }

//		    if (message_type.equals("text")) {
//
////			    viewHolder.messageText.setVisibility(VISIBLE);
////			    viewHolder.messageText.setText(c.getMessage());
//			    Log.d(TAG, "onBindViewHolder: " + c.getMessage());
////			    viewHolder.messageImage.setVisibility(View.GONE);
//
//			    setValue(viewHolder.messageText, c.getMessage()
//					    , viewHolder.messageImage);
//
//
//		    } else {
//
//				setValue(viewHolder.messageImage, c.getMessage()
//						, viewHolder.messageText);
////			    viewHolder.messageText.setVisibility(View.GONE);
////			    viewHolder.messageImage.setVisibility(VISIBLE);
//
////			    if(c.mBitmap != null)
////			    	Picasso.get().load(c.getMessage())
////						    .placeholder(R.drawable.ic_menu_gallery)
////						    .fit().centerCrop().into(viewHolder.messageImage);
////				    viewHolder.messageImage.setImageBitmap(
////						    ImageHelper.getRoundedCornerBitmap(c.mBitmap));
////			    else viewHolder.messageImage.setImageBitmap(c.placeHolder);
//
//		    }
//	    }

    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }


//    private void setValue(View view, String text, View view2)
//    {
//		showView(view);
//		hideView(view2);
//
//		if(view instanceof TextView)
//		{
//			((TextView) view).setText(text);
//		}
//		else
//		{
//			Picasso.get().load(text).fit().centerCrop()
//					.placeholder(R.drawable.ic_menu_gallery)
//					.error(R.drawable.ic_menu_gallery)
//					.into((RoundedImageView)view);
//		}
//    }

//    private void showView(View pTextView)
//    {
//    	pTextView.setVisibility(VISIBLE);
//    }
//    private void hideView(View view)
//    {
//    	view.setVisibility(GONE);
//    }
//
}
