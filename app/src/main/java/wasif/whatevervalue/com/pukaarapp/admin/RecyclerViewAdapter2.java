package wasif.whatevervalue.com.pukaarapp.admin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.database.DatabaseSchema;
import wasif.whatevervalue.com.pukaarapp.database.MyDatabase;
import wasif.whatevervalue.com.pukaarapp.models.Therapist;
import wasif.whatevervalue.com.pukaarapp.utils.FirebaseMethods;

/**
 * Created by User on 1/1/2018.
 */

@SuppressWarnings({"WeakerAccess", "unused"})
public class RecyclerViewAdapter2 extends RecyclerView.Adapter<RecyclerViewAdapter2.ViewHolder> implements View.OnClickListener {

    private static final String TAG = "RecyclerViewAdapter2";

    private ArrayList<String> mImageNames;
    private ArrayList<String> mImages;
    private ArrayList<String> mUserIds;
    private Context mContext;
    String phoneNo;
    String message;
    final int MY_PERMISSIONS_REQUEST_SEND_SMS = 1000;
    final int PERMISSION_REQUEST_CODE=1;


    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    @SuppressWarnings("FieldCanBeLocal")
    private FirebaseAuth mAuth;
    private FirebaseMethods mFirebaseMethods;


    public RecyclerViewAdapter2(Context context, ArrayList<String> imageNames, ArrayList<String> images, ArrayList<String> ids) {
        Log.d(TAG, "RecyclerViewAdapter: I am hereee");
        mImageNames = imageNames;
        mImages = images;
        mUserIds = ids;
        mContext = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: I am inside");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem2, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Glide.with(mContext)
                .asBitmap()
                .load(mImages.get(position))
                .into(holder.image);

        holder.imageName.setText(mImageNames.get(position));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on: " + mImageNames.get(position));

                Toast.makeText(mContext, mImageNames.get(position), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(mContext, GalleryActivity2.class);
                intent.putExtra("image_url", mImages.get(position));
                intent.putExtra("image_name", mImageNames.get(position));
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImageNames.size();
    }

    @Override
    public void onClick(View v) {

        if (v.equals(R.id.btn_cancel)) {

        }
    }


    @SuppressWarnings("WeakerAccess")
    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView image;
        TextView imageName;
        RelativeLayout parentLayout;
        Button assignUser;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            imageName = itemView.findViewById(R.id.image_name);
            parentLayout = itemView.findViewById(R.id.parent_layout);
            assignUser= itemView.findViewById(R.id.assign_user);

            assignUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //removeAt(getAdapterPosition());
                    Log.d(TAG, "onClick: "+ mUserIds);
                    //goToAllTherapistsList(getAdapterPosition());


                }
            });
        }



        public void removeAt(int position) {
            mImageNames.remove(position);
            mUserIds.remove(position);
            mImages.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, mImages.size());
        }
    }
}














