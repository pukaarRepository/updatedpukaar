package wasif.whatevervalue.com.pukaarapp.login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.utils.FirebaseMethods;

public class QuestionActivity5 extends AppCompatActivity implements View.OnClickListener {

    //firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private FirebaseMethods mFirebaseMethods;

    private int questionCount = 0;

    private static final String TAG = "questionActivity5";

    Button answer1,answer2,answer3,answer4,answer5;
    FirebaseMethods firebaseMethods;
    TextView question5;


    String question_1,answer_1;
    String question_2,answer_2;
    String question_3,answer_3;
    String question_4,answer_4;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question5);
        answer1=findViewById(R.id.answer1);
        answer2=findViewById(R.id.answer2);
        answer3=findViewById(R.id.answer3);
        answer4=findViewById(R.id.answer4);
        question5=findViewById(R.id.question5);
        mFirebaseMethods= new FirebaseMethods(QuestionActivity5.this);

        setupFirebaseAuth();

        findViewById(R.id.answer1).setOnClickListener(this);
        findViewById(R.id.answer2).setOnClickListener(this);
        findViewById(R.id.answer3).setOnClickListener(this);


        question_1= getIntent().getStringExtra("question1");
        answer_1= getIntent().getStringExtra("answer1");
        question_2= getIntent().getStringExtra("question2");
        answer_2= getIntent().getStringExtra("answer2");
        question_3= getIntent().getStringExtra("question3");
        answer_3= getIntent().getStringExtra("answer3");
        question_4= getIntent().getStringExtra("question4");
        answer_4= getIntent().getStringExtra("answer4");





    }

    /**
     * Setup the firebase auth object
     */
    private void setupFirebaseAuth(){
        Log.d(TAG, "setupFirebaseAuth: setting up firebase auth.");
        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        Log.d(TAG, "onDataChange: image count: " + questionCount);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();


                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };



        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                questionCount = mFirebaseMethods.getQuestionsCount(dataSnapshot);
                Log.d(TAG, "onDataChange: question count: " + questionCount);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    public void goToQuestionActivity6(TextView question,TextView answer){
        Intent intent= new Intent(this,QuestionActivity6.class);
        intent.putExtra("question1",question_1);
        intent.putExtra("answer1",answer_1);
        intent.putExtra("question2",question_2);
        intent.putExtra("answer2",answer_2);
        intent.putExtra("question3",question_3);
        intent.putExtra("answer3",answer_3);
        intent.putExtra("question4",question_4);
        intent.putExtra("answer4",answer_4);
        intent.putExtra("question5",question.getText().toString());
        intent.putExtra("answer5",answer.getText().toString());
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.answer1:
                goToQuestionActivity6(question5,answer1);
                break;
            case R.id.answer2:
                goToQuestionActivity6(question5,answer2);
                break;

            case R.id.answer3:
                goToQuestionActivity6(question5,answer3);
                break;
            case R.id.answer4:
                goToQuestionActivity6(question5,answer4);
                break;

        }
    }
}
