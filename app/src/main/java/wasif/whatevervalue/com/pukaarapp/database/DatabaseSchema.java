package wasif.whatevervalue.com.pukaarapp.database;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Taha Malik on 01/02/2019.
 **/
@SuppressWarnings({"unused","WeakerAccess"})
public class DatabaseSchema {

	@SuppressWarnings("WeakerAccess")
	public static class GraphTable{
		public final static String DATE = "DATE";
		public final static String DAY = "DAY";
		public final static String VALUE = "VALUE";
//		public final static String QID = "QID";
		public final static String[] COLS = {
				DATE, DAY, VALUE
		};
	}

	public static class ExcitedGraphTable extends GraphTable
	{
		public final static String NAME = "ExcitedGraph";
	}
	public static class HappyGraphTable extends GraphTable
	{
		public final static String NAME = "HappyGraph";

	}

	public static class FineGraphTable extends GraphTable
	{
		public final static String NAME = "FineGraph";
	}

	public static class DepressionGraphTable extends GraphTable
	{
		public final static String NAME = "DepressionGraph";

	}
	public static class SadGraphTable extends GraphTable
	{
		public final static String NAME = "SadGraph";
	}

	public static class UpsetGraphTable extends GraphTable
	{
		public final static String NAME = "UpsetGraph";
	}

	public static class OKGraphTable extends GraphTable
	{
		public final static String NAME = "OKGraph";
	}



	public static class ChatForumTable
	{
		public final static String NAME = "ChatForum";
		public final static String QID = "QID";
		public final static String TIMESTAMP = "TIMESTAMP";
		public final static String UID = "Username";
		public final static String QUERY = "QueryCol";
		public final static String SAME_HERE = "SameHere";
		public final static String[] COLS = {QID, UID, QUERY, SAME_HERE, TIMESTAMP};

	}
	public static class ResponseTable
	{
		public final static String NAME = "Responses";
		public final static String RID = "RID";
		public final static String UID = "userID";
		public final static String QUERY_ID = "QueryID";
		public final static String RESPONSE = "Response";
		public final static String DATE_TIME = "DATETIME";
		public final static String[] COLS = {RID, UID, QUERY_ID, RESPONSE, DATE_TIME};
	}

	public static class MoodTable
	{
		public final static String NAME = "Moods";
		public final static String DATE = "DATE";
		public final static String DAY = "DAY";
		public final static String MOOD = "MOOD";
		public final static String[] COLS = {DATE, DAY, MOOD};

	}

	public static class ChatTable {
		public final static String NAME= "Chatting";
		public final static String FROM= "FromID";
		public final static String TO= "ToID";
		public final static String MessageID= "MessageID";
		public final static String Message= "Message";
		public final static String TYPE= "Type";
		public final static String DATE = "Date";
		public final static String[] COLS = { FROM,TO,MessageID,Message,TYPE,DATE};
	}

	public static class TherapistInfoTable{
		public final static String NAMEOFTABLE="TherapistProfileTable";
		public final static String THERAPISTID="TherapistID";
        public final static String NAME="TherapistName";
		public final static String MONDAY="Monday";
		public final static String TUESDAY="Tuesday";
		public final static String WEDNESDAY="Wednesday";
		public final static String THURSDAY= "Thursday";
		public final static String FRIDAY="Friday";
		public final static String SATURDAY= "Saturday";
		public final static String SUNDAY="Sunday";
		public final static String DESCRIPTION="Description";
		public final static String LICENSE="License";
		public final static String LICENSEDES="LicenseDescription";
		public final static String IMAGEURL="imageURL";
		public final static String SPECIALITIES="Specialities";
		public final static String DEGREE="Degree";
		public final static String JOINEDTIME="JoinedTime";
		public final static String TIMEPRACTICEMONTHS="TimePracticeMonths";
		public final static String TIMEZONE="TimeZone";
		public final static String TREATMENTAPPROACH="TreatmentApproach";
		public final static String USERSHELPED="UsersHelped";
		public final static String[] COLS={THERAPISTID,NAME,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY,IMAGEURL,
		DESCRIPTION,LICENSE,LICENSEDES,SPECIALITIES,DEGREE,JOINEDTIME,TIMEPRACTICEMONTHS,TIMEZONE,TREATMENTAPPROACH,USERSHELPED};
	}

	public static class UsersAssignedToTherapist{
		public final static String NAME="UsersAssignedToTherapist";
		public final static String THERAPISTID="TherapistID";
		public final static String USERID="UserID";
		public final static String[] COLS={THERAPISTID,USERID};
	}


	public static class TherapistsAssignedToUser{
		public final static String NAME="TherapistsAssignedToUser";
		public final static String USERID="USERID";
		public final static String THERAPISTID="TherapistID";
		public final static String STATUS="Status";
		public final static String[] COLS={USERID,THERAPISTID,STATUS};
	}

	public static class Specialities{
		public final static String NAME="Specialities";
		public final static String THERAPISTID="TherapistID";
		public final static String SPECIALITY="Speciality";
		public final static String STATUS="Status";
		public final static String[] COLS={THERAPISTID,SPECIALITY,STATUS};
	}


	public static class SameHereTable
	{
		public final static String NAME = "SameHere";
		public final static String QUERY_ID = "QID";
		public final static String UID = "userID";
		public final static String DATE = "DATE";
		public final static String[] COLS = { QUERY_ID, UID};
	}

	public static class UsersTable{
		public final static String NAME="Users";
		public final static String USERID="UserID";
		public final static String PHONENO="PhoneNo";
		public final static String EMAIL="Email";
		public final static String PASSWORD="Password";
		public final static String USERNAME="Username";
		public final static String PROFILE_PHOTO="ProfilePhoto";
		public final static String[] COLS ={USERID,PHONENO,EMAIL,USERNAME,PROFILE_PHOTO,PASSWORD};
	}

	public static class CompleteMoodTable
	{

		public final static String NAME = "CompleteMoodTable";
		public final static String DATE = "Date";
		public final static String MOOD = "Mood";
		public final static String ANXIETY = "Anxiety";
		public final static String ENERGY_LEVEL = "EnergyLevel";
		public final static String SELF_CONFIDENCE = "SelfConfidence";
		public final static String MED_TAKEN = "MedTaken";
		public final static String DAY = "DAY";
		public final static String NOTE = "NOTE";

		public final static String[] COL = {DATE, DAY, MOOD, ANXIETY, ENERGY_LEVEL, SELF_CONFIDENCE, MED_TAKEN, NOTE};


	}



}
