package wasif.whatevervalue.com.pukaarapp.models;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Taha Malik on 01/02/2019.
 **/
public class ChatForum {

	public int commentCount = 0;
	public String QID;
	public long timeStamp;
	public String query;
	public String userID;
	public ArrayList<Response> comments;
	public String username;
	public String userPic;

	public ChatForum(String pQID, long pDateTime, String pQuery, String pUserID, int pCommentCount,ArrayList<Response> pResponses, String pUsername, String pUserPic) {
		commentCount = pCommentCount;
		QID = pQID;
		timeStamp = pDateTime;
		query = pQuery;
		userID = pUserID;
		username = pUsername;
		userPic = pUserPic;
		if(pResponses != null)
			comments = pResponses;
		else comments = new ArrayList<>();

	}

}
