package wasif.whatevervalue.com.pukaarapp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Taha Malik on 01/02/2019.
 **/
public class DateConverter {

	public static java.sql.Timestamp convertUtilToSQLTimeStamp(Date pDate)
	{
		return new java.sql.Timestamp(pDate.getTime());
	}

	public static java.sql.Date convertUtilToSQLDate(Date pDate)
	{
		return new java.sql.Date(pDate.getTime());
	}

	public static Date convertSQLToUtilDate(java.sql.Date pDate)
	{
		return new Date(pDate.getTime());
	}
	public static Date convertSQLToUtilDate(java.sql.Timestamp pTimestamp)
	{
		return new Date(pTimestamp.getTime());
	}

	public static String convertToString(java.sql.Date pDate)
	{
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z", Locale.getDefault());
		return df.format(pDate);
	}
	public static String convertToString(java.sql.Timestamp pTimestamp)
	{
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z", Locale.getDefault());
		return df.format(pTimestamp);
	}
	public static String convertToString(Date pDate)
	{
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z", Locale.getDefault());
		return df.format(pDate);
	}

	public static Date convertStringToUtilDate(String date)
	{
		try {
			return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z", Locale.getDefault()).parse(date);
		} catch (ParseException pE) {
			pE.printStackTrace();
		}
		return null;
	}

	public static java.sql.Date convertStringToSQLDate(String date)
	{
		try {
			Date lDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z", Locale.getDefault()).parse(date);
			return convertUtilToSQLDate(lDate);
		} catch (ParseException pE) {
			pE.printStackTrace();
		}
		return null;
	}
	public static java.sql.Timestamp convertStringToSQLTimeStamp(String date)
	{
		try {
			Date lDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z", Locale.getDefault()).parse(date);
			return convertUtilToSQLTimeStamp(lDate);
		} catch (ParseException pE) {
			pE.printStackTrace();
		}
		return null;
	}


	public static String convertToNewLineString(Date pDate) {
		SimpleDateFormat df = new SimpleDateFormat("dd-MM'\n'HH:mm a", Locale.getDefault());
		return df.format(pDate);
	}

	public static String getDateStringFromInt(int num)
	{
		if(num == 0)
		{
			return "Sunday";
		}
		else if(num == 1)
		{
			return "Monday";
		}
		else if(num == 2)
		{
			return "Tuesday";
		}
		else if(num == 3)
		{
			return "Wednesday";
		}
		else if(num == 4)
		{
			return "Thursday";
		}
		else if(num == 5)
		{
			return "Friday";
		}
		else if(num == 6)
		{
			return "Saturday";
		}
		else return "INVALID";

	}
	public static String getThreeDigitDateStringFromInt(int num)
	{
		if(num == 0)
		{
			return "Sun";
		}
		else if(num == 1)
		{
			return "Mon";
		}
		else if(num == 2)
		{
			return "Tue";
		}
		else if(num == 3)
		{
			return "Wed";
		}
		else if(num == 4)
		{
			return "Thu";
		}
		else if(num == 5)
		{
			return "Fri";
		}
		else if(num == 6)
		{
			return "Sat";
		}
		else return "N/A";

	}

}
