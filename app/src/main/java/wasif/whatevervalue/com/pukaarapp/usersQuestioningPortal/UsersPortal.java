package wasif.whatevervalue.com.pukaarapp.usersQuestioningPortal;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.database.MyDatabase;
import wasif.whatevervalue.com.pukaarapp.models.ChatForum;
import wasif.whatevervalue.com.pukaarapp.models.Response;
import wasif.whatevervalue.com.pukaarapp.utils.DateConverter;

public class UsersPortal extends AppCompatActivity {

	LinkedList<ChatForum> mChatForums = new LinkedList<>();
	RecyclerView mRecyclerView;
	public final static String Tag = "UsersPortal";
	public static final String IS_ADMIN = "ISADMIN";
	private boolean isAdmin;
	AppCompatActivity mActivity = this;


	@Override
	public void onBackPressed() {

		List<Fragment> list = getSupportFragmentManager().getFragments();
		if(list.size() > 0)
		{
			for(Fragment f : list)
			{
				getSupportFragmentManager().beginTransaction()
						.remove(f)
						.commit();
			}
		}
		else super.onBackPressed();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_users_portal);
		isAdmin = getIntent().getBooleanExtra( IS_ADMIN, false);
		findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				onBackPressed();
			}
		});

		ImageView addPost = findViewById(R.id.add_post);
		addPost.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				getSupportFragmentManager().beginTransaction()
						.add(R.id.container,
								new AddPostFragment(), AddPostFragment.TAG)
						.commit();
			}
		});

		mRecyclerView = findViewById(R.id.queries);
//		MyDatabase lMyDatabase = MyDatabase.getInstance(getApplicationContext());
//
//		mChatForums = lMyDatabase.getChatForum();

//		if(mChatForums.size() == 0)
//		{
//			loadFromFirebase(mChatForums);
//		}

		mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
		mRecyclerView.setAdapter(new QueryAdapter(mChatForums));
		loadFromFirebase(mChatForums);


	}

	private void loadFromFirebase(@NonNull final LinkedList<ChatForum> pChatForums) {


//		String UID = FirebaseAuth.getInstance().getUid();

		DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

		String QID = pChatForums.size() > 0 ? pChatForums.getLast().QID
				: "";
		ref = ref.child("posts");

		ref.orderByChild("timestamp")
				.limitToLast(15)
				.endAt(QID)
				.addListenerForSingleValueEvent(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {
						Iterable<DataSnapshot> ldsShots = pDataSnapshot.getChildren();
						for(DataSnapshot dss
								: ldsShots)
						{
							ChatForum cf;
							String qid = dss.getKey();
							String text = (String) dss.child("text").getValue();
							long timeStamp = (long) dss.child("timestamp").getValue();
							String uid = (String) dss.child("userID").getValue();

							DataSnapshot comments = dss.child("comments");
							int commentCount = Long.valueOf(comments.getChildrenCount()).intValue();

//							ArrayList<Response> cmnts = new ArrayList<>();
//							Iterable<DataSnapshot> cs = comments.getChildren();
//							for(DataSnapshot c : cs)
//							{
//								String rid = c.getKey();
//								String cText = (String) c.child("text").getValue();
//								String cUserID = (String) c.child("userID").getValue();
//
//								Date cTimeStamp = c.child("timeStamp").getValue(Date.class);
//
//								String uname = (String) c.child("username").getValue();
//								String pic = (String) c.child("userPic").getValue();
//								Response r = new Response(rid, cUserID, cTimeStamp, cText, uname, pic);
//								cmnts.add(r);
//							}

							String username = (String) dss.child("userName").getValue();
							String userPic = (String) dss.child("userImg").getValue();

							cf = new ChatForum(qid, timeStamp, text, uid, commentCount,null, username, userPic);
//							ChatForum cf = new ChatForum(
//									DateConverter.convertStringToUtilDate((String) dss.child("Date").getValue()),
//									(String) dss.child("Query").getValue(),
//									(String) dss.child("userID").getValue()
//									, ((Long) Objects.requireNonNull(dss.child("SameHere").getValue())).intValue()
//									, null, null);
////
////							cf.mResponses = new ArrayList<>();
////
////
////
////							DataSnapshot res = dss.child("Responses");
////							Iterable<DataSnapshot> lDataSnapshots = res.getChildren();
////							for (DataSnapshot dshot :
////									lDataSnapshots) {
////								Response lResponse = new Response(dshot.getKey()
////										, Objects.requireNonNull(dshot.child("userID").getValue()).toString()
////										, DateConverter.convertStringToUtilDate(dshot.child("Date").toString())
////										, "Response"
////										, "QID");
////								cf.mResponses.add(0, lResponse);
////
////							}
							pChatForums.addFirst(cf);
						}

						Objects.requireNonNull(mRecyclerView.getAdapter()).notifyDataSetChanged();
					}

					@Override
					public void onCancelled(@NonNull DatabaseError pDatabaseError) {

					}
				});
	}

	private class QueryAdapter extends RecyclerView.Adapter<QueryHolder>
	{
		LinkedList<ChatForum> mChatForums;

		QueryAdapter(LinkedList<ChatForum> pChatForums) {
			mChatForums = pChatForums;
		}

		@NonNull
		@Override
		public QueryHolder onCreateViewHolder(@NonNull ViewGroup pViewGroup, int pI) {
			View view = LayoutInflater.from(getApplicationContext())
					.inflate(R.layout.user_portal_item, pViewGroup, false);
			return new QueryHolder(view);
		}

		@SuppressLint("SetTextI18n")
		@Override
		public void onBindViewHolder(@NonNull final QueryHolder pQueryHolder, final int pI) {

//			if(pI == 0)
//			{
//
//				DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
//						.child(getString(R.string.users_table_firebase))
//						.child(Objects.requireNonNull(FirebaseAuth.getInstance().getUid()));
//
//				ref.addListenerForSingleValueEvent(new ValueEventListener() {
//					@Override
//					public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {
//						String username = (String) pDataSnapshot.child(getString(R.string.nickname_firebase))
//								.getValue();
//						pQueryHolder.userNameTop.setText(username);
//						String picUrl = (String) pDataSnapshot.
//								child(getString(R.string.profile_photo_col_firebase)).getValue();
//						Picasso.get().load(picUrl).placeholder(R.drawable.ic_menu_gallery)
//								.into(pQueryHolder.userPicTop);
//					}
//
//					@Override
//					public void onCancelled(@NonNull DatabaseError pDatabaseError) {
//
//					}
//				});
//
//
//				pQueryHolder.allQueries.setVisibility(View.GONE);
//				pQueryHolder.addQuery.setVisibility(View.VISIBLE);
//
//				pQueryHolder.post.setOnClickListener(new View.OnClickListener()
//				{
//					@Override
//					public void onClick(View pView) {
//
//						if(!pQueryHolder.writeQuery.getText().toString().trim().equals(""))
//						{
//							pQueryHolder.post.setEnabled(false);
//							final FirebaseAuth mAuth = FirebaseAuth.getInstance();
//							final HashMap<String, Object> lHashMap = new HashMap<>();
//							final Date lDate = Calendar.getInstance().getTime();
//							lHashMap.put(getString(R.string.UID), mAuth.getUid());
//							lHashMap.put(getString(R.string.date),
//									DateConverter.convertToString(lDate));
//							lHashMap.put(getString(R.string.query),
//									pQueryHolder.writeQuery.getText().toString().trim());
//							lHashMap.put(getString(R.string.same_here), 0);
//
//							DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
//							ref = ref.child(getString(R.string.user_portal))
//									.child(getString(R.string.queries_firebase)).push();
//							final String QID = ref.getKey();
//							final DatabaseReference finalRef = ref;
//							ref.setValue(lHashMap).addOnCompleteListener(
//									new OnCompleteListener<Void>() {
//										@Override
//										public void onComplete(@NonNull Task<Void> pTask) {
//											if(pTask.isSuccessful())
//											{
////												MyDatabase db = MyDatabase.getInstance(getApplicationContext());
//												ChatForum lChatForum = new ChatForum(QID, lDate,
//														pQueryHolder.writeQuery.getText().toString().trim(),
//														mAuth.getUid(), 0, null, null);
////												db.addChatForum(lChatForum);
//												mChatForums.addFirst(lChatForum);
//												notifyDataSetChanged();
//												mRecyclerView.invalidate();
//												pQueryHolder.writeQuery.setText("");
//												pQueryHolder.post.setEnabled(true);
//												Toast.makeText(getApplicationContext(), "Query Posted Successfully", Toast.LENGTH_SHORT).show();
//
//											}
//											else if(!pTask.isSuccessful())
//											{
//												finalRef.child(getString(R.string.user_portal))
//														.child(getString(R.string.queries_firebase)).push().
//														setValue(lHashMap).addOnCompleteListener(this);
//											}
//											else if(pTask.isCanceled())
//											{
//												pQueryHolder.post.setEnabled(false);
//												Toast.makeText(getApplicationContext(), "Failed To Post Query. Check your Network Connection",
//														Toast.LENGTH_SHORT).show();
//											}
//											else
//											{
//
//												pQueryHolder.post.setEnabled(false);
//												Toast.makeText(getApplicationContext(), "Unknown Error Occurred." +
//																" Check your Network Connection",
//														Toast.LENGTH_SHORT).show();
//											}
//
//										}
//									});
//						}
//						else
//						{
//							pQueryHolder.post.setEnabled(true);
//							Toast.makeText(getApplicationContext(), "Write Something", Toast.LENGTH_SHORT).show();
//						}
//					}
//				});
//
//			}
//			else
//			{
			final ChatForum lChatForum = getItem(pI);
//				pQueryHolder.addQuery.setVisibility(View.GONE);
//			pQueryHolder.allQueries.setVisibility(View.VISIBLE);
//			pQueryHolder.sendResponse.setOnClickListener(new View.OnClickListener() {
//				@Override
//				public void onClick(View pView) {
//					if(!pQueryHolder.enterResponse.getText().toString().trim().equals("")) {
//						final String QID = lChatForum.QID;
//						final HashMap<String, String> lHashMap = new HashMap<>();
//						DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
//
//						final DatabaseReference response =
//								ref.child(getString(R.string.firebase_table_responses))
//								.child(QID)
//								.push();
//
////							final DatabaseReference text = ref.child(getString(R.string.user_portal))
////									.child(getString(R.string.queries_firebase))
////									.child(QID).child(getString(R.string.responses)).push();
//						final String RID = response.getKey();
//						final Date lDate = Calendar.getInstance().getTime();
//						lHashMap.put(getString(R.string.qid), QID);
//						lHashMap.put(getString(R.string.date),
//								DateConverter.convertToString(lDate));
//						lHashMap.put(getString(R.string.response),
//								pQueryHolder.enterResponse.getText().toString().trim());
//						lHashMap.put(getString(R.string.UID), FirebaseAuth.getInstance().getUid());
//
//						response.setValue(lHashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
//							@Override
//							public void onComplete(@NonNull Task<Void> pTask) {
//								if(pTask.isSuccessful())
//								{
//									MyDatabase db = MyDatabase.getInstance(getApplicationContext());
//									Response lResponse = new Response(RID, FirebaseAuth.getInstance().getUid(),
//											lDate, pQueryHolder.enterResponse.getText().toString().trim(),
//											QID);
//									db.addResponse(lResponse);
//									pQueryHolder.enterResponse.setText("");
//									pQueryHolder.sendResponse.setEnabled(true);
//									((ResponseAdapter) Objects.requireNonNull(lmRecyclerView.getAdapter())).mResponses.add(0,lResponse);
//									notifyDataSetChanged();
//									lmRecyclerView.invalidate();
//
//								}
//								else
//								{
//									response.setValue(lHashMap).addOnCompleteListener(this);
//								}
//							}
//						});
//
//					}
//					else
//					{
//						Toast.makeText(getApplicationContext(),"Write Something First", Toast.LENGTH_SHORT).show();
//					}
//				}
//			});

//				MyDatabase db = MyDatabase.getInstance(getApplicationContext());
//				if(!isAdmin && !db.shouldSameHereEnabled(lChatForum.QID, lChatForum.userID)) {
////					pQueryHolder.like.setEnabled(false);
//
////					pQueryHolder.like.setOnClickListener(new View.OnClickListener() {
////						@Override
////						public void onClick(View pView) {
////							final DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
////									.child(getString(R.string.user_portal))
////									.child(lChatForum.QID).child(getString(R.string.same_here));
////							final int[] likes = new int[1];
////							ref.addListenerForSingleValueEvent(new ValueEventListener() {
////								@Override
////								public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {
////									likes[0] = Long.valueOf((Long) pDataSnapshot.child("SameHere").getValue()).intValue();
////								}
////
////								@Override
////								public void onCancelled(@NonNull DatabaseError pDatabaseError) {
////
////								}
////							});
////							ref.setValue(likes[0] + 1).addOnCompleteListener(new OnCompleteListener<Void>() {
////								@Override
////								public void onComplete(@NonNull Task<Void> pTask) {
////									if (pTask.isSuccessful()) {
////										MyDatabase.getInstance(getApplicationContext()).setLikes(lChatForum.QID,
////												likes[0] + 1);
////									} else {
////										ref.setValue(likes[0] + 1).addOnCompleteListener(this);
////									}
////								}
////							});
////						}
////					});
//				}
			pQueryHolder.query.setText(lChatForum.query);
//			pQueryHolder.mCardView.setRadius(10);
			pQueryHolder.comment.setText(lChatForum.commentCount + " Comments");
			pQueryHolder.userName.setText(lChatForum.username);
			Picasso.get().load(lChatForum.userPic).fit().centerInside()
					.placeholder(R.drawable.default_avatar).into(pQueryHolder.userPic);
			pQueryHolder.comment.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View pView) {
//					if(lmRecyclerView.getVisibility() == View.GONE) {
//
//						ArrayList<Response> lResponses = ((ResponseAdapter)lmRecyclerView.getAdapter()).mResponses;
//						if(lResponses.size() == 0)
//						{
//							loadResponsesIn(lmRecyclerView, lChatForum.QID);
//						}
//
//						lmRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//							@Override
//							public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//								super.onScrollStateChanged(recyclerView, newState);
//							}
//
//							@Override
//							public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//
//								if(((LinearLayoutManager) Objects.requireNonNull(lmRecyclerView.getLayoutManager()))
//										.findLastVisibleItemPosition() == ((ResponseAdapter)lmRecyclerView
//										.getAdapter()).mResponses.size()-1)
//								{
//									loadResponsesIn(lmRecyclerView, lChatForum.QID);
//								}
//								super.onScrolled(recyclerView, dx, dy);
//
//							}
//						});
//
//						lmRecyclerView.setVisibility(View.VISIBLE);
//
//
//
//					}
//					else
//						lmRecyclerView.setVisibility(View.GONE);
					mActivity.getSupportFragmentManager()
							.beginTransaction()
							.add(R.id.container,  PostDetailFragment.newInstance(lChatForum.username,
									lChatForum.userPic, lChatForum.query,
									DateConverter.convertToString(new Date(lChatForum.timeStamp)),
									lChatForum.commentCount, lChatForum.QID), PostDetailFragment.TAG)
							.commit();
				}
			});


//			lmRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
//			lmRecyclerView.setAdapter(new ResponseAdapter(lChatForum.comments));

//			DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
//					.child(getString(R.string.users_table_firebase))
//					.child(lChatForum.userID);
//
//			ref.addListenerForSingleValueEvent(new ValueEventListener() {
//				@Override
//				public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {
//					String username = (String) pDataSnapshot.child(getString(R.string.nickname_firebase))
//							.getValue();
//					pQueryHolder.userName.setText(username);
////					pQueryHolder.userName.requestLayout();
////					pQueryHolder.userName.invalidate();
//					String picUrl = (String) pDataSnapshot.child("imageURL").getValue();
//					Picasso.get().load(picUrl).placeholder(R.drawable.ic_menu_gallery)
//							.into(pQueryHolder.userPic);
//				}
//
//				@Override
//				public void onCancelled(@NonNull DatabaseError pDatabaseError) {
//
//				}
//			});
////				pQueryHolder.likes.setText("likes: " + lChatForum.samehere);
////			}
		}

		ChatForum getItem(int position)
		{
			return mChatForums.get(position);

		}

		@Override
		public int getItemCount() {
			return mChatForums.size();
		}
	}

	private class QueryHolder extends RecyclerView.ViewHolder{


		View allQueries;

		TextView query;
		CircleImageView userPic;
		TextView userName;
		TextView comment;

		CardView mCardView;

		QueryHolder(@NonNull View itemView) {
			super(itemView);
			mCardView = itemView.findViewById(R.id.user_query);
//			sendResponse = itemView.findViewById(R.id.send_response);
			allQueries = itemView.findViewById(R.id.all_queries);
//			addQuery = itemView.findViewById(R.id.top_view);
//			writeQuery = itemView.findViewById(R.id.write_query);
//			like = itemView.findViewById(R.id.like);
			comment = itemView.findViewById(R.id.comment);
			query = itemView.findViewById(R.id.query);
//			userPicTop = itemView.findViewById(R.id.user_pic_top);
			userPic = itemView.findViewById(R.id.user_pic);
//			userNameTop = itemView.findViewById(R.id.username_top);
			userName = itemView.findViewById(R.id.username);
//			enterResponse = itemView.findViewById(R.id.write_response);
//			responses = itemView.findViewById(R.id.responses);
//			post = itemView.findViewById(R.id.post);
//			likes = itemView.findViewById(R.id.likes);
		}
	}
}
