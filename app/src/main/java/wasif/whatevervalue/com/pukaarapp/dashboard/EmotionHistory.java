package wasif.whatevervalue.com.pukaarapp.dashboard;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;
import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.intefaces.GetTAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmotionHistory extends Fragment implements GetTAG {


	public static final String TAG = "EmotionHistoryFragment";

	public EmotionHistory() {
		// Required empty public constructor
	}

	private static boolean mTherapist = false;
	private static String UID = "";

	@Override
	public void onDestroy() {
		mTherapist = false;
		UID = "";
		super.onDestroy();
	}

	public static Fragment getInstance(boolean pTherapist, String pUID)
	{
		mTherapist = pTherapist;
		UID = pUID;
		return new EmotionHistory();

	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		final View lView = inflater.inflate(R.layout.fragment_emotion_history, null);

		ImageView backBtn = lView.findViewById(R.id.back);
		final Fragment lFragment = this;
		backBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View pView) {
				Objects.requireNonNull(getActivity()).onBackPressed();


			}
		});


		final PieChartView pieChartView = lView.findViewById(R.id.chart);
		final List<SliceValue> pieData = new ArrayList<>();
		pieData.add(new SliceValue(0,
				ContextCompat.getColor(getContext(), R.color.anxiety)).
				setLabel("0.00"));
		pieData.add(new SliceValue(0,
				ContextCompat.getColor(getContext(), R.color.energy)).
				setLabel("0.00"));
		pieData.add(new SliceValue(0,
				ContextCompat.getColor(getContext(), R.color.confidence)).
				setLabel("0.00"));

		PieChartData pieChartData = new PieChartData(pieData);
		pieChartData.setHasLabels(true);
		pieChartData.setHasLabels(true).setValueLabelTextSize(16);
		pieChartData.setHasCenterCircle(true);
		pieChartView.setPieChartData(pieChartData);

		String moodNode = "notes";

		if(mTherapist)
			moodNode = "therapistNotes";

		String uid = FirebaseAuth.getInstance().getUid();

		if(!mTherapist && !UID.equals(""))
			uid = UID;

		assert uid != null;
		DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
				.child(moodNode).child(uid);
		if(mTherapist)
			ref = ref.child(UID);

		ref.addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot pDataSnapshot) {
				if(pDataSnapshot.getChildrenCount() > 0) {
					int n = (int) pDataSnapshot.getChildrenCount();

					float anxietySum = 0;
					float energySum = 0;
					float confidenceSum = 0;

					for(DataSnapshot d : pDataSnapshot.getChildren()){
						anxietySum += ((Long) Objects.requireNonNull(d.child("anxiety").getValue())).intValue();
						energySum += ((Long) Objects.requireNonNull(d.child("energyLevel").getValue())).intValue();
						confidenceSum += ((Long) Objects.requireNonNull(d.child("selfConfidence").getValue())).intValue();
					}

//					int anxietySum = ((Long) Objects.requireNonNull(pDataSnapshot.child("anxiety-sum").getValue())).intValue();
//					int energySum = ((Long) Objects.requireNonNull(pDataSnapshot.child("energy-sum").getValue())).intValue();
//					int confidenceSum = ((Long) Objects.requireNonNull(pDataSnapshot.child("confidence-sum").getValue())).intValue();
//					int n = ((Long) Objects.requireNonNull(pDataSnapshot.child("n").getValue())).intValue();
//
//					long anxietyAvg = Integer.valueOf(anxietySum).longValue();
//					long energyAvg = Integer.valueOf(energySum).longValue();
//					long confidenceAvg = Integer.valueOf(confidenceSum).longValue();

					double anxietyAvg = anxietySum/n;
					double energyAvg = energySum/n;
					double confidenceAvg =confidenceSum/n;

					DecimalFormat lDecimalFormat = new DecimalFormat("0.00");

					List<SliceValue> pieData = new ArrayList<>();
					pieData.add(new SliceValue(Math.abs(anxietySum),
							ContextCompat.getColor(getContext(), R.color.anxiety)).
							setLabel(lDecimalFormat.format(Math.abs(anxietyAvg))));
					pieData.add(new SliceValue(Math.abs(energySum),
							ContextCompat.getColor(getContext(), R.color.energy)).
							setLabel(lDecimalFormat.format(Math.abs(energyAvg))));
					pieData.add(new SliceValue(Math.abs(confidenceSum),
							ContextCompat.getColor(getContext(), R.color.confidence)).
							setLabel(lDecimalFormat.format(Math.abs(confidenceAvg))));
					PieChartData pieChartData = new PieChartData(pieData);
					pieChartData.setHasLabels(true);
					pieChartData.setHasLabels(true).setValueLabelTextSize(16);
					pieChartData.setHasCenterCircle(true);
					pieChartView.setPieChartData(pieChartData);
				}
			}

			@Override
			public void onCancelled(@NonNull DatabaseError pDatabaseError) {

			}
		});



		return lView;
	}

	@Override
	public String getTAG() {
		return TAG;
	}



}
