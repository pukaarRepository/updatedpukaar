package wasif.whatevervalue.com.pukaarapp.therapistEnd;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import wasif.whatevervalue.com.pukaarapp.R;
import wasif.whatevervalue.com.pukaarapp.therapistEnd.ChatsList;
import wasif.whatevervalue.com.pukaarapp.therapistEnd.NotesList;
import wasif.whatevervalue.com.pukaarapp.therapistEnd.ProfilesList;

public class TherapistPanelFragment extends Fragment {

    public static final String TAG = "TherapistPanelFragment";

    Button newUsers;
    Button assignedUsers;
    final int PERMISSION_REQUEST_CODE=1;


    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;

    TextView profiles;
    TextView chats;
    private View mView;
    TextView notes;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View lView = inflater.inflate(R.layout.activity_therapist_menu, null);
        mView = lView;
        profiles=mView.findViewById(R.id.profiles);
        chats=mView.findViewById(R.id.chats);
        notes=mView.findViewById(R.id.notes);


        profiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToProfilesList();
            }
        });

        chats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToChatsList();
            }
        });

        notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNotesList();
            }
        });



//		FacebookSdk.sdkInitialize(getApplicationContext());
//		Log.d("AppLog", "key:" + FacebookSdk.getApplicationSignature(getActivity()));


        return lView;
    }


    public void goToChatsList(){
        Intent intent= new Intent(getActivity(), ChatsList.class);
        startActivity(intent);
    }

    public void goToNotesList(){
        Intent intent= new Intent(getActivity(), NotesList.class);
        startActivity(intent);
    }


    public void goToProfilesList(){
        Intent intent= new Intent(getActivity(), ProfilesList.class);
        startActivity(intent);
    }



}
