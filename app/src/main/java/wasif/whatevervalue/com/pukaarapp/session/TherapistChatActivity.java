package wasif.whatevervalue.com.pukaarapp.session;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;

import wasif.whatevervalue.com.pukaarapp.R;

public class TherapistChatActivity extends AppCompatActivity {

	public static final String TherapistID = "TherapistID";
	public static final String UserID = "userID";
	public static final String UserName = "UserName";
	public static final String TherapistName = "TherapistName";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_therapist_chat);

		getSupportFragmentManager().beginTransaction()
				.add(R.id.container, SessionFragment.getInstance(getIntent().getExtras().getString(UserID),
						FirebaseAuth.getInstance().getUid())
						, SessionFragment.TAG).commit();


	}
}
