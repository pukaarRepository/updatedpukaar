package wasif.whatevervalue.com.pukaarapp.models;

import java.util.Date;

/**
 * Created by Taha Malik on 03/02/2019.
 **/
public class CompleteMood {

	public long timestamp;
	public String mood;
	public int anxiety;
	public int selfConfidence;
	public int energyLevel;
//	public String day;
	public String note;

	public CompleteMood(long pDate, String pMood,
	                    int pAnxiety,
	                    int pSelfConfidence, int pEnergyLevel, String note) {

		mood = pMood;
		anxiety = pAnxiety;
		selfConfidence = pSelfConfidence;
		energyLevel = pEnergyLevel;
		timestamp = pDate;
		this.note = note;
	}


	public static int getNoOfDay(String day) {
		if (day.toLowerCase().equals("mon")) {
			return 0;
		} else if (day.toLowerCase().equals("tue")) {
			return 1;
		} else if (day.toLowerCase().equals("wed")) {
			return 2;
		} else if (day.toLowerCase().equals("thr") || day.toLowerCase().equals("thu")) {
			return 3;
		} else if (day.toLowerCase().equals("fri")) {
			return 4;
		} else if (day.toLowerCase().equals("sat")) {
			return 5;
		} else if (day.toLowerCase().equals("sun")) {
			return 6;
		} else {
			return -1;
		}
	}
}